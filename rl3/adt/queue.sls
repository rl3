;;;
;; "Efficient" queue implemented with set-cdr!
;; Essentially a port of Larceny's lib/Standard/queue.scm to R6RS
;; Consider rewriting to Okasaki's applicative queue
;;;

(library
  (rl3 adt queue)
  
  (export
   make-queue queue? queue-empty? queue-put! queue-get! queue-remove!)
  
  (import
   (primitives call-without-interrupts
               unspecified)
   (rnrs base)
   (only (rnrs mutable-pairs)
         set-cdr!)
   (err5rs records syntactic))
  
  (define-record-type queue ctor-queue #t
    (head)(tail))
  
  (define make-queue 
    (lambda ()
      (ctor-queue '() '())))

  (define queue-empty? 
    (lambda (x)
      (null? (queue-head x))))
    
  (define queue-put!
    (lambda (queue obj)
      (call-without-interrupts
       (lambda ()
         (let ((item (cons obj '())))
           (if (null? (queue-head queue))
               (begin (queue-head-set! queue item)
                      (queue-tail-set! queue item))
               (begin (set-cdr! (queue-tail queue) item)
                      (queue-tail-set! queue item)))
           (unspecified))))))
    
  (define queue-get!
    (lambda (queue)
      (call-without-interrupts 
       (lambda ()
         (let ((item (queue-head queue)))
           (assert (not (null? item)))
           (queue-head-set! queue (cdr item))
           (if (null? (cdr item))
               (queue-tail-set! queue '()))
           (car item))))))
  
  (define queue-remove!
    (lambda (queue obj)
      (call-without-interrupts
       (lambda ()
         (let loop ((p (queue-head queue)) (pp '()))
           (cond ((null? p))
                 ((eq? obj (car p))
                  (if (null? pp)
                      (queue-head-set! queue (cdr p))
                      (set-cdr! pp (cdr p))))
                 (else
                  (loop (cdr p) p))))))))

  'queue
  )