(library
 (rl3 env debug)

 (export debug-enable)

 (import (rnrs base)
         (only (rnrs io simple)
               display)
         (only (rnrs control)
               unless)                     
         (primitives current-output-port decode-error error-handler reset))
 
 (define simple-error-handler
   (lambda e
     (decode-error e)
     (reset)))
 
 (define default-error-handler #f)
 
 (define debug-enable
   (lambda (state)
     (if state
         (error-handler default-error-handler)
         (error-handler simple-error-handler))
     state))
 
 (unless default-error-handler
   (set! default-error-handler (error-handler))))

;;  ;; (require 'inspect-cont)

;;  ;; The code was parameterized over the puts function to interface to some
;;  ;; highly-customized logging functionality. (lambda (s) (display s) (newline))
;;  ;; would be one standard Scheme implementation...

;;  (define (batch/last-chance-handler puts)
;;    (lambda e
;;      (define (display-line s)
;;        (puts (with-output-to-string
;;                (lambda () (display s)))))

;;      ;;(display-line `(lastchance error handler ,e))

;;      (let* ((error-text
;;              (call-with-output-string (lambda (p) (decode-error e p))))
;;             (stacktrace (current-continuation-structure))
;;             (inspector (make-continuation-inspector stacktrace))

;;             (summarize-frame
;;              (lambda (count inspector . prefix)
;;                (let* ((frame (inspector 'get))
;;                       (code  (frame 'code))
;;                       (class (code 'class))
;;                       (expr  (code 'expression))
;;                       (proc  (code 'procedure)))
;;                  (display-line
;;                   `(frame ,@prefix ,class
;;                           ,@(case class
;;                               ((system-procedure) '())
;;                               ((interpreted-primitive) (procedure-name proc))
;;                               ((interpreted-expression) expr)
;;                               ((compiled-procedure) (procedure-name proc))
;;                               (else '())))
;;                   ))))

;;             (backtrace
;;              (lambda (count inspector)
;;                (let loop ((c (inspector 'clone)))
;;                  (let ((f (c 'get)))
;;                    (if (f 'same? (inspector 'get))
;;                        (summarize-frame 0 c "=> ")
;;                        (summarize-frame 0 c "   ")))
;;                  (if (c 'down)
;;                      (loop c))
;;                  )))
;;             )
;;        ;;(display-line `(decoded error ,error-text))
;;        ;;(backtrace 0 inspector)
;;        (display error-text)
;;        (display "Resetting REPL")
;;        (reset)
       
;;        )))

;;  (define (install-lastchance puts)
;;    (error-handler (batch/last-chance-handler puts)))

;;  (require 'inspect-cont) 
;; ;; (install-lastchance (lambda (s) (display s)(newline)))


