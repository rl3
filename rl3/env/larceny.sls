;; Library to hold basis environment/intialization stuff.

(library
 (rl3 env larceny)

 (export break error-handler getenv reset-handler os-type system)

 (import
  (rnrs base)
  (only (rnrs lists)
        assq member)
  (only (rnrs io simple)
        display newline write)
  (only (larceny load)
        require)
  (primitives procedure-name error-handler reset getenv larceny-break
              make-continuation-inspector
              call-with-output-string
              with-output-to-string
              decode-error
              current-continuation-structure
              reset-handler system-features system))

 (define break larceny-break)
 
 (define os-type
   (let ((os-name (assq 'os-name (system-features))))
     (cond
      ((member os-name '((os-name . "Linux")
                         (os-name . "MaxOS X")
                         (os-name . "SunOS")))
       'unix)
      ((member os-name '((os-name . "Win32")))
       'windows)
      (else
       (error 'os-type ": add case for os " os-name)))))

 
 )

