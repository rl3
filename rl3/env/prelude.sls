(library
 
 (rl3 env prelude)
 
 (export
  begin0
  unspecified unspecified?
  add1 sub1
  inc! dec!
  fx1+ fx1-
  id)
 
 (import
  (rnrs base)
  (only (rnrs arithmetic fixnums)
        fx+ fx-))

 (define unspecified
   (let ((void (if #f #t)))
     (lambda () void)))

 (define unspecified?
   (let ((v (unspecified)))
     (lambda (void)
       (eq? void v))))

  ;; ;;;;;;;;;;;;;;
 ;; Basic  Macros
 ;; ;;;;;;;;;;;;;;

 (define-syntax fx1-
   (syntax-rules ()
     ((fx1- x)
      (fx- x 1))))
   
 (define-syntax fx1+
   (syntax-rules ()
     ((fx1+ x)
      (fx+ 1 x))))
 
 (define-syntax add1
   (syntax-rules ()
     ((add1 x)
      (+ 1 x))))
 
 (define-syntax sub1
   (syntax-rules ()
     ((sub1 x)
      (- x 1))))
 
 (define-syntax inc!
   (syntax-rules ()
     ((inc! x)
      (set! x (+ 1 x)))))

 (define-syntax dec!
   (syntax-rules ()
     ((dec! x)
      (set! x (- x 1)))))

 
 (define-syntax begin0
   (syntax-rules ()
     ((begin0 e1 es ...)
      (let ((result e1))
        es ...
        result))))

 ;; misc functions
 (define id
   (lambda (x) x))
 
 )

