(library
  (rl3 web http)

  ;;http-request
  (export  http-invoke
          ;;get-chunk         
          http-ascii-port-from-binary-port
          ;; open-http-binary-input-port
          make-http-binary-input-port
          get-header header-value)
  
  (import
   (rnrs base)
   (only (rnrs lists)
         assoc)
   (only (rnrs unicode)
         char-numeric? string-ci=?)
   (only (rnrs arithmetic fixnums)
         fx=? fx+ fx- fx<? fx<=? fxzero?)
   (only (rnrs mutable-strings)
         string-set!)
   (only (rnrs io simple)
         display write newline)
   (only (rnrs bytevectors)
         string->utf8
         bytevector-length)
   (only (rnrs io simple)
         display newline)
   (only (rnrs io ports)
         eof-object? eof-object put-char
         get-char get-string-n get-bytevector-n
         get-bytevector-n! put-bytevector
         textual-port? binary-port? close-port
         get-u8 lookahead-u8 lookahead-char
         flush-output-port
         make-custom-textual-input-port
         make-custom-binary-input-port
         make-custom-binary-output-port)
   (only (rl3 io net sockets)
         client-socket
         socket-output-port socket-input-port
         socket-input-port-bytes-read)
   (only (rl3 web uri)
         parse-authority authority-host authority-port
         parse-uri uri-authority)
   (only (rnrs control)
         when do)
   (only (err5rs records syntactic)
         define-record-type)
   (only (rl3 env prelude)
         fx1+ fx1-)
   ;; FIXME RPR
   (primitives get-output-string open-output-string io/make-port))

  ;; Do a substring, trimming spaces
  (define substring-trim
    (lambda (src-str start end)
      (let ((s-pos (do ((s-pos start (fx1+ s-pos)))
                       ((or (fx=? s-pos end)
                            (not (eqv? (string-ref src-str s-pos) #\space)))
                        s-pos)))
            (e-pos (do ((e-pos end (fx1- e-pos)))
                       ((or (fx=? e-pos start)
                            (not (eqv? (string-ref src-str e-pos) #\space)))
                        (if (fx<? e-pos end)
                            (fx1+ e-pos)
                            e-pos)))))
        (substring src-str s-pos e-pos))))
  
  (define get-chunk-length   
    (lambda (ip)
      
      (define char-hex?
        (lambda (ch)
          (or (char-numeric? ch)
              (case ch
                ((#\a #\b #\c #\d #\e #\f) #t)
                (else #f)))))
      
      (define get-char-binary-port
        (lambda (ip)
          (let ((u8 (get-u8 ip)))
            (if (eof-object? u8)
                u8
                (integer->char u8)))))
      
      (define lookahead-char-binary-port
        (lambda (ip)
          (let ((u8 (lookahead-u8 ip)))
            (if (eof-object? u8)
                u8
                (integer->char u8)))))
      
      (define read-chunk-length
        (lambda (ip next peek)
          (let ((osp (open-output-string)))
            (let ((ch (peek ip)))
              (if (eof-object? ch)
                  0
                  (when (eqv? ch #\return)
                    (next ip)     ;; return
                    (next ip))))  ;; linefeed
            (let loop ((ch (next ip)))
              (cond 
               ((eof-object? ch)         
                0)
               ((char-hex? ch)
                (begin
                  (put-char osp ch)
                  (loop (next ip))))
               ((eqv? #\return ch)
                (let ((ch (peek ip)))
                  (if (eof-object? ch)
                      0
                      (if (eqv? ch #\linefeed)
                          (begin
                            (next ip)
                            (string->number (string-append "#x" (get-output-string osp))))
                          0))))
               (else 0))))))

      (cond
       ((textual-port? ip)
        (read-chunk-length ip get-char lookahead-char))
       ((binary-port? ip)
        (read-chunk-length ip get-char-binary-port lookahead-char-binary-port)))))

  ;; Assumes the port http headers have been read.
  ;; Extra buffer copy here as I'm wrapping a port within a port
  ;; Create a binary input port which understands the http protocol.
  ;; e.g., seamless handling of chunked encoding.
  ;; Ideally I'd like to avoid double buffer filling
  ;; but the R6RS machinary and maybe Larceny's don't make this
  ;; readily achievable. Really need a Streams/Port approach such
  ;; as proposed in SRFI-81,82,83.

  ;; If content-length is false then assume chunked encoding.
  ;; input-port? * number? -> binary-input-port?

  ;; content-length/chunked?
  ;;   #f - header has not been read
  ;;   'chunked - chunked
  ;;   number? - content-length
  ;; id - name of the port??
  ;; ip - socket input port
  (define make-http-binary-input-port
    (lambda (id ip content-length/chunked?)

      (define name (string-copy id))

      (define chunked?
        (eq? 'chunked content-length/chunked?))
      
      (define left-to-read 
        (cond
         ((number? content-length/chunked?)
          content-length/chunked?)
         (chunked? (get-chunk-length ip))
         (error 'make-http-binary-input-port "HTTP Protocol must have content-length or chunked encoding.")))

      (define read-proc
        (lambda (iodata buffer)
          (if (fxzero? left-to-read)
              'eof
              (let ((buffsz (bytevector-length buffer)))
                (let ((cnt (get-bytevector-n! ip buffer 0                 
                                              (if (fx<? left-to-read buffsz)
                                                  left-to-read
                                                  buffsz))))
                  (set! left-to-read (fx- left-to-read cnt))
                  (if (fxzero? cnt)                    ;; socket peer reset
                      'eof
                      (when (and chunked?
                                 (fxzero? left-to-read))
                        (set! left-to-read (get-chunk-length ip))))
                  cnt)))))
      
      (define iproc
        (lambda (op)
          (case op
            ((read) read-proc)
            ((close) (lambda (io-data) (close-port ip)))
            ((name)  (lambda (io-data) name))
            (else (assertion-violation 'make-http-binary-input-port "unsupported IO operation" id op)))))

      (io/make-port iproc name 'input 'binary)))
  
  ;; Temporary simple ascii textual port implementation that is a work around
  ;; for transcoded-port bug in Larceny.
  ;;
  ;; binary-input-port? -> textual-input-port?
  (define http-ascii-port-from-binary-port
    (lambda (ip)
      (let ((read! (lambda (str start count)
                     (let loop ((idx start) (cnt 0))
                       (if (fx<? cnt count)
                           (let ((u8 (get-u8 ip)))
                             (if (eof-object? u8)
                                 cnt
                                 (begin
                                   (string-set! str idx (integer->char u8))
                                   (loop (fx1+ idx) (fx1+ cnt)))))
                           cnt))))
            (close (lambda ()
                     (close-port ip))))
        (make-custom-textual-input-port "ascii port" read! #f #f close))))

  (define get-header
    (lambda (attr headers)
      (assoc attr headers)))

  (define header-value
    (lambda (hdr)
      (if (pair? hdr)
          (cdr hdr)
          #f)))
  
  (define chunked-encoding?
    (lambda (headers)
      (let ((hdr (get-header "Transfer-Encoding" headers)))
        (if hdr
            (string-ci=? "chunked" (cdr hdr))
            #f))))

  ;; extract the http request Content-Length
  ;; return #f if Content-Length is not present
  (define content-length
    (lambda (headers)
      (let ((len (header-value (get-header "Content-Length" headers))))
        (if (string? len)
            (string->number len)
            #f))))
  
  ;; returns:
  ;;   'chunked
  ;;   number? is content-length
  ;;   #f is bad and an error
  (define content-length-or-chunked?
    (lambda (headers)
      (let ((len (content-length headers)))
        (if len
            len
            (if (chunked-encoding? headers)
                'chunked
                #f)))))
  
  ;; Read the http header by reading the given port until \r\n\r\n is found.
  ;; port? -> (cons start-line headers)
  (define http-header-from-socket-input-port
    (lambda (inp)
      (let ((MAX-REQUEST 1024))
        (let ((req (make-string MAX-REQUEST)))
          (let loop ((state 0) (cnt 0) (byte (get-u8 inp)) (caret 0) (colon 0) (headers '()))
            (if (fx=? cnt MAX-REQUEST)
                #f                                                           ;; FIXME return 4XX
                (if (eof-object? byte)
                    (reverse headers)
                    (let ((state (case state
                                   ((0) (case byte
                                          ((#x0D) 1)
                                          (else  0)))
                                   ((1) (case byte
                                          ((#x0A) 2)
                                          (else  0)))
                                   ((2) (case byte
                                          ((#x0D) 3)
                                          (else 0)))
                                   ((3) (case byte
                                          ((#x0A) 4)
                                          (else 0))))))
                      (case state
                        ((2) (let ((ch (integer->char byte)))
                               (string-set! req cnt ch)
                               (loop state
                                     (fx1+ cnt)
                                     (get-u8 inp)
                                     (fx1+ cnt)
                                     -1                      ;; colon <> -1 mean we found the first one already.  ':' is a legitimate header value, only first ':" is a delim.
                                     (if (zero? colon)       ;; HTTP line as no colon was found
                                         (cons (substring req caret (fx1- cnt)) headers)
                                         (cons (cons (substring req caret colon) (substring-trim req (fx1+ colon) (fx1- cnt))) ;; header line (attr . value)
                                               headers)))))
                        ((4) (reverse headers))
                        (else
                         (let ((ch (integer->char byte)))
                           (string-set! req cnt ch)
                           (loop state
                                 (fx1+ cnt)
                                 (get-u8 inp)
                                 caret
                                 (if (and (fx=? colon -1)
                                          (eqv? ch #\:))
                                     cnt                ;; found a colon at position cnt
                                     colon)
                                 headers))))))))))))

  (define http-invoke
    (lambda (action url headers)
      (let ((authority (parse-authority (uri-authority (parse-uri url)))))
        (let ((host (authority-host authority))
              (port (authority-port authority))
              (verb (lambda (action)
                      (case action
                        ((GET)  (string->utf8 "GET"))
                        ;;  ((POST) (string->utf8 "POST"))
                        ;;  ((HEAD) (string->utf8 "HEAD")))))
                        (else (error 'http-invoke "HTTP method not support." action)))))
              (space (string->utf8 " "))
              (version (string->utf8 "HTTP/1.1"))
              (terminate (string->utf8 "\r\n")))
          (let ((s (client-socket  host  (if port port 80))))
            (let ((op (socket-output-port s))
                  (ip (socket-input-port s)))
              (let ((send (lambda (s)
                            (put-bytevector op s))))
                ;; header line
                (send (verb action))      (send space)
                (send (string->utf8 url)) (send space)
                (send version)            (send terminate)
                ;; headers
                (for-each (lambda (h)
                            (send (string->utf8 h))
                            (send terminate))
                          headers)
                (send terminate)
                (flush-output-port op)             
                (close-port op) ;; fix for keep-alive
                (let ((headers (http-header-from-socket-input-port ip)))
                  (values headers
                          (make-http-binary-input-port "http port" ip (content-length-or-chunked? (cdr headers))))))))))))

  )

