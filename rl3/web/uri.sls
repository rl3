;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Bravais' Edito Princeps: EBook Tool Suite	    
;; Copyright (C) 2007  Raymond Paul Racine
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(library
 (rl3 web uri char-sets)

 (export
  encode-char
  digit-char? hex-char? pchar? pct-encoded-char?
  scheme-start-ch? scheme-tail-ch? sub-delim-char? unreserved-char?
  unsafe-char?)

 (import
  (rnrs base)
  (only (rnrs unicode)
        char-ci>=? char-ci<=? char-downcase))

 (define encode-char
   (lambda (ch)
     (string-append "%" (number->string (char->integer ch) 16))))

 (define encode-char?
   (lambda (ch)
     (or (unsafe-char? ch))))

 (define digit-char?
   (lambda (ch)
     (and
      (char>=? ch #\0)
      (char<=? ch #\9))))
 
 (define alphabet-char?
   (lambda (ch)
     (and
      (char-ci>=? ch #\a)
      (char-ci<=? ch #\z))))
 
 (define hex-char?
   (lambda (ch)
     (or
      (digit-char? ch)
      (case (char-downcase ch)
        ((#\a #\b #\c #\d #\e #\f) #t)
        (else #f)))))
 
 (define unreserved-char?
   (lambda (ch)
     (or
      (alphabet-char? ch)
      (digit-char? ch)
      (case ch
        ((#\. #\_ #\~ #\\ #\-) #t)
        (else #f)))))

 (define reserved
   (lambda (ch)
     (or
      (general-delim-char? ch)
      (sub-delim-char? ch))))

 ;; rtf1138
 (define unsafe-char?
   (lambda (ch)
     (case ch
       ((#\{ #\} #\| #\\ #\^ #\~ #\[ #\] #\`)
        #t)
       (else #f))))

 (define general-delim-char?
   (lambda (ch)
     (case ch
       ((#\: #\/ #\? #\# #\[ #\] #\@) #t)
       (else #f))))
 
 (define sub-delim-char?
   (lambda (ch)
     (case ch
       ((#\! #\$ #\& #\' #\( #\) #\* #\+ #\, #\; #\=) #t)
       (else #f))))
 
 (define pct-encoded-char?
   (lambda (ch)
     (or
      (eq? ch #\%)
      (hex-char? ch))))
 
 (define pchar?
   (lambda (ch)
     (or
      (unreserved-char? ch)
      (pct-encoded-char? ch)
      (sub-delim-char? ch)
      (case ch
        ((#\: #\@) #t)
        (else #f)))))
 
 (define scheme-start-ch? alphabet-char?)
 
 (define scheme-tail-ch?
   (lambda (ch)
     (or
      (scheme-start-ch? ch)
      (digit-char? ch)
      (case ch
        ((#\+ #\- #\.) #t)
        (else #f)))))
 
 )

(library
 
 (rl3 web uri)
 
 (export
  url-decode-string
  parse-uri parse-authority uri->string
  ;; Authority rtd
  make-authority authority? authority->string authority-equal?
  authority-username authority-host authority-port
  ;; URI rtd
  make-uri uri?
  uri-scheme uri-authority uri-path uri-query uri-fragment)
 
 (import
  (rnrs base)
  (only (rnrs control)
        unless)
  (only (rnrs io simple)
        display newline 
        eof-object? peek-char read-char write-char)
  (only (rnrs io ports)
        get-char put-char
        open-string-input-port)
  (primitives open-output-string get-output-string)
  (err5rs records syntactic)
  (only (larceny records printer)
        rtd-printer-set!)
  (rl3 env prelude)
  (only (rl3 web uri char-sets)
        hex-char? digit-char?
        pchar?
        scheme-start-ch?
        scheme-tail-ch?
        pct-encoded-char?
        sub-delim-char?
        unreserved-char?))

 (define url-decode-string
   (lambda (str)
     (let ((op (open-output-string))
           (ip (open-string-input-port str)))
       (let loop ((ch (get-char ip)))
         (if (eof-object? ch)
             (get-output-string op)
             (if (char=? #\% ch)
                 (let ((ch1 (get-char ip)))
                   (if (and (not (eof-object? ch1))
                            (hex-char? ch1))
                       (let ((ch2 (get-char ip)))
                         (if (and (not (eof-object? ch2))
                                  (hex-char? ch2))
                             (begin
                               (put-char op (integer->char (string->number (list->string (list ch1 ch2)) 16))) ;; use (let ((buff (make-string 2))) ???
                               (loop (get-char ip)))
                             (begin              ;; got %d? so write '%' and digit and carryon
                               (put-char op #\%)
                               (put-char op ch1)
                               (unless (eof-object? ch2)
                                 (put-char op ch2))
                               (loop (get-char ip)))))
                       (begin                   ;; got %? so write them and carryon
                         (put-char op #\%)
                         (unless (eof-object? ch1)
                           (put-char op ch1))
                         (loop (get-char ip)))))
                 (begin
                   (unless (eof-object? ch)
                     (put-char op ch))
                   (loop (get-char ip)))))))))
 
 ;; all strings or #f
 (define-record-type authority #t #t username host port)
 
 ;;all strings or #f, each string is a major piece of the uri.
 (define-record-type uri ctor-uri #t scheme authority path query fragment)
 
 (define null-string?
   (lambda (s)
     (if (string? s)
         (zero? (string-length s))
         #f)))
 
 (define make-uri
   (lambda (scheme user host port path query fragment)
     (assert (and
              (string? scheme)
              (or (string? user)
                  (and (boolean? user)
                       (not user)))
              (string? host)
              (or (number? port)
                  (and (boolean? port)
                       (not port)))
              (string? path)
              (string? query)
              (string? fragment)))
     (let ((sport (if (number? port)
                      (number->string port)
                      port)))                               
       (let ((authority (authority->string (make-authority (if (null-string? user)
                                                               #f user)
                                                           host (if (null-string? sport) #f sport)))))
         (if (null-string? scheme)
             #f
             (ctor-uri scheme authority
                       (if (null-string? path)
                           "/"
                           path)
                       (if (null-string? query)
                           #f
                           query)
                       (if (null-string? fragment)
                           #f
                           fragment)))))))

 (define maybe
   (lambda (field prefix)
     (if field (string-append prefix field) "")))
 
 (define uri->string
   (lambda (uri)
     (string-append
      (uri-scheme uri)
      ":"
      (let ((auth (uri-authority uri)))
        (if auth
            (string-append "//" auth)
            ""))
      (uri-path uri)
      (maybe (uri-query uri) "?")
      (maybe (uri-fragment uri) "#"))))

 (define authority->string
   (lambda (authority)
     (string-append
      (let ((user (authority-username authority)))
        (if user
            (string-append user "@")
            ""))
      (authority-host authority)
      (let ((port (authority-port authority)))
        (if port
            (string-append ":" (number->string port))
            "")))))

 ;; Two authororities are equal if they're record values are equal.
 (define authority-equal?
   (lambda (auth1 auth2)
     (and (equal? (authority-username auth1)
                  (authority-username auth2))
          (equal? (authority-host auth1)
                  (authority-host auth2))
          (eqv? (authority-port auth1)
                (authority-port auth2)))))
 
 ;; Read chars while valid or eof-object?
 ;; Place valid chars in output string port
 ;; First invalid char is left on the input port
 ;; returns: number of valid chars read from input port.

 (define read-valid
   (lambda (ip valid? op)
     (let loop ((ch (peek-char ip)) (cnt 0))
       (if (or (eof-object? ch)
               (not (valid? ch)))
           cnt
           (begin
             (write-char (read-char ip) op)
             (loop (peek-char ip) (fx1+ cnt)))))))
 
 ;; (input-port?  output-port?) -> boolean?)
 ;; parse the "tail" of a scheme
 ;; i.e., the rest of the scheme string given that
 ;; the start char of the scheme was valid.
 ;; returns: # of chars read
 (define parse-scheme-tail
   (lambda (ip op)
     (read-valid ip scheme-tail-ch? op)))
 
 (define parse-scheme
   (lambda (ip)
     (let ((op (open-output-string)))
       (let ((ch (peek-char ip)))
         (if (not (scheme-start-ch? ch))
             #f
             (begin
               (write-char (read-char ip) op)
               (parse-scheme-tail ip op)
               (get-output-string op)))))))
 
 
 ;; lex a character of value chtok
 ;; returns: #f if the next character is not a chtok
 (define (parse-char ip chtok)
   (let ((ch (peek-char ip)))
     (if (eof-object? ch)
         #f
         (if (eq? ch chtok)
             (begin
               (read-char ip)
               #t)
             #f))))
 
 (define parse-authority-opaque
   (lambda (ip)
     (let ((op (open-output-string)))
       (read-valid ip (lambda (ch)
                        (case ch
                          ((#\/ #\? #\#) #f)
                          (else #t)))
                   op)
       (get-output-string op))))
 
 (define parse-path-abempty
   (lambda (ip)
     (let ((op (open-output-string)))
       (let ((ch (peek-char ip)))
         (if (or (eof-object? ch)
                 (eq? ch #\?)
                 (eq? ch #\#))
             ""
             (if (not (eq? ch #\/))
                 (error "A URI with an authority can only have an absolute path.")
                 (begin
                   (write-char (read-char ip) op)
                   (let ((ch (peek-char ip)))
                     (if (eq? ch #\/)
                         (error "Absolute path must have a none empty segment.  i.e., // is illegal")
                         (read-valid ip
                                     (lambda (ch)
                                       (or
                                        (pchar? ch)
                                        (eq? ch #\/)))
                                     op))))))
         (get-output-string op)))))

 (define parse-path-absolute
   (lambda (ip)
     (let ((op  (open-output-string)))
       (write-char #\/ op)
       ;; first segment must not have a ':'
       (read-valid ip
                   (lambda (ch)
                     (and
                      (not (eq? ch #\:))
                      (pchar? ch)))
                   op)
       (read-valid ip
                   (lambda (ch)
                     (or
                      (pchar? ch)
                      (eq? ch #\/)))
                   op)
       (get-output-string op))))

 (define parse-path-rootless
   (lambda (ip)
     (let ((op (open-output-string)))
       (read-valid ip
                   (lambda (ch)
                     (or
                      (pchar? ch)
                      (eq? ch #\/)))
                   op)
       (get-output-string op))))

 ;; returns 2 values
 ;;  1) authority or #f
 ;;  2) path
 (define parse-hier
   (lambda (ip)
     (let ((ch (peek-char ip)))
       (if (eof-object? ch)
           (values #f "")
           (if (eq? ch #\/)
               (begin
                 (read-char ip)
                 (if (eq? (peek-char ip) #\/)
                     (begin
                       (read-char ip)
                       (let ((authority (parse-authority-opaque ip)))
                         (let ((path-abempty (parse-path-abempty ip)))
                           (values authority path-abempty))))
                     (values #f (parse-path-absolute ip))))
               (values #f (parse-path-rootless ip)))))))

 (define parse-query-or-fragment
   (lambda (ip signal-char)
     (let ((ch (peek-char ip)))
       (if (eof-object? ch)
           #f
           (if (eq? ch signal-char)
               (let ((op (open-output-string)))
                 (read-char ip) ;; consume signal char
                 (read-valid ip
                             (lambda (ch)
                               (or
                                (pchar? ch)
                                (eq? ch #\?)
                                (eq? ch #\/)))
                             op)
                 (get-output-string op))
               #f)))))
 
 (define parse-uri
   (lambda (uri-str)
     (let ((ip (open-string-input-port uri-str)))
       (let ((scheme (parse-scheme ip)))
         (if (not scheme)
             (error "Invalid URI.  manditory scheme is missing.")
             (if (not (parse-char ip #\:))
                 (error "Invalid URI.  scheme must be delimited by a ':'.")
                 (let-values (((authority path) (parse-hier ip)))
                   (let ((query (parse-query-or-fragment ip #\?)))
                     (let ((fragment (parse-query-or-fragment ip #\#)))
                       (ctor-uri scheme authority path query fragment))))))))))

 ;; Parse out the port string.
 ;; Assumes leading ':' has been consumed.
 (define parse-port
   (lambda (ip)
     (let ((op  (open-output-string)))
       (if (or (eof-object? (peek-char ip))
               (not (digit-char? (peek-char ip))))
           (error "Missing port number or extraneous characters where port number was expected.")
           (let ((port (begin
                         (read-valid ip
                                     (lambda (ch)
                                       (digit-char? ch))
                                     op)
                         (get-output-string op))))
             (string->number port))))))

;;; Parse the host and optional port from a given string
;;; returns: (values host port)
 (define parse-host-port
   (lambda (ip)
     (let ((op  (open-output-string)))
       (if (eof-object? (peek-char ip))
           (error "URI missing required host.")
           (let ((host (begin
                         (read-valid ip
                                     (lambda (ch)
                                       (or
                                        (unreserved-char? ch)
                                        (pct-encoded-char? ch)
                                        (sub-delim-char? ch)))
                                     op)
                         (get-output-string op))))
             (if (> (string-length host) 0)
                 (let ((ch (read-char ip)))
                   (if (eof-object? ch)  ;; no port
                       (values host #f)
                       (if (not (eq? ch #\:))
                           (error "Host must be optionally followed by a port.  Something else found.")
                           (let ((port (parse-port ip)))
                             (values host port)))))
                 (error "Manditory host is missing.")))))))

 (define authority-with-username?
   (lambda (auth)
     (let ((ip (open-string-input-port auth)))
       (let loop ((ch (read-char ip)))
         (cond
          ((eof-object? ch) #f)
          ((eq? ch #\@) #t)
          (else
           (loop (read-char ip))))))))

 (define parse-authority
   (lambda (auth-str)
     (if (not (string? auth-str))
         #f
         (let ((ip (open-string-input-port auth-str)))
           (if (authority-with-username? auth-str)
               (let ((op  (open-output-string)))
                 (read-valid ip
                             (lambda (ch)
                               (or
                                (unreserved-char? ch)
                                (pct-encoded-char? ch)
                                (sub-delim-char? ch)
                                (eq? ch #\:)))
                             op)
                 (if (not (eq? (read-char ip) #\@))
                     (error "Invalid username.")
                     (let ((username (get-output-string op)))
                       (let-values (((host port) (parse-host-port ip)))
                         (make-authority username host port)))))
               (let-values (((host port) (parse-host-port ip)))
                 (make-authority #f host port)))))))


 (rtd-printer-set! uri (lambda (uri outp)
                         (display "#<uri \"" outp)
                         (display (uri->string uri) outp)
                         (display "\">" outp)))

 (rtd-printer-set! authority (lambda (auth outp)
                               (display "#<authority \"" outp)
                               (display (authority->string auth) outp)
                               (display "\">" outp)))
 )

(library
 (rl3 web uri url parms)
 
 (export parse-parms encode-parm
         parms->query)
 
 (import
  (rnrs base)
  (only (rnrs io simple)
        eof-object?)
  (only (rnrs io ports)
        open-string-input-port
        get-char put-char put-string)
  (only (rl3 web uri char-sets)
        encode-char unsafe-char?)
  (only (rl3 types chars)
        string->char-set
        char-set-complement)
  (only (rl3 types strings)
        string-tokenize)
  (only (rl3 text text)
        separate)
  (primitives get-output-string open-output-string))
 
 (define parm-reserved-char?
   (lambda (ch)
     (case ch
       ((#\& #\=) #t)
       (else #f))))
 
 (define alist? list?)
 (define query alist?)

 (define encode-parm-string
   (lambda (str)
     (let ((op (open-output-string))
           (ip (open-string-input-port str)))
       (let loop ((ch (get-char ip)))
         (if (eof-object? ch)
             (get-output-string op)
             (begin
               (if (or (unsafe-char? ch)
                       (parm-reserved-char? ch))
                   (put-string op (encode-char ch))
                   (put-char op ch))
               (loop (get-char ip))))))))
 
 (define encode-parm
   (lambda (parm)
     (let ((key   (car parm))
           (value (cdr parm)))
       (cons (encode-parm-string key)
             (encode-parm-string value)))))

 (define parms->query
   (lambda (parms)
     (separate "&" (map (lambda (kv)
                          (string-append (car kv) "=" (cdr kv)))
                        parms))))
 
 (define parm-delim-char-set
   (char-set-complement (string->char-set "=&")))
 
 (define parse-parms
   (lambda (parm-str)
     (let ((kvs (string-tokenize parm-str parm-delim-char-set)))
       (let loop ((kvs kvs) (parms '()))
         (if (null? kvs)
             parms
             (let ((key (car kvs)))
               (if (null? (cdr kvs))
                   parms ;; odd number of KVs which is wrong.  Return what we got.
                   (loop (cddr kvs) (cons (cons key (cadr kvs)) parms)))))))))
 
 )

;;          (define test-auth
;;            (list
;;             "www.amazon.com"
;;             "ray@www.amazon.com"
;;             "www.amazon.com:80"
;;             "ray@www.amazon.com:80"))

;;          (define test-auth-bad
;;            (list
;;             "ray@"
;;             "ray@80"
;;             ":90"
;;             "ray@:80"))

;;          (for-each (lambda (auth-str)
;;                      (let ((auth (parse-authority auth-str)))
;;                        (display "Username: ")
;;                        (display (authority-username auth))
;;                        (display " Host: ")
;;                        (display (authority-host auth))
;;                        (display " Port: ")
;;                        (display (authority-port auth))
;;                        (newline)))
;;                    test-auth)


;; http://ecs.amazonaws.com/onca/xml?
;; Service=AWSECommerceService&
;; Operation=ItemSearch&
;; AWSAccessKeyId=[Access Key ID]&
;; AssociateTag=[ID]&
;; SearchIndex=Apparel&
;; Keywords=Shirt

;; (uri->string (parse-uri (uri->string
;;                          (make-uri "http"
;;                                    "" "ecs.amazonaws.com" #f
;;                                    "/onca/xml"          
;;                                    "Service=AWSECommerceService&Operation=ItemSearch&AWSAccessKeyId=[Access Key ID]&AssociateTag=[ID]&SearchIndex=Apparel&Keywords=Shirt"
;;                                    ""))))


;; (uri-authority (make-uri "http"
;;                          "" "ecs.amazonaws.com" 8080
;;                          "/onca/xml"
;;                          "Service=AWSECommerceService&Operation=ItemSearch&AWSAccessKeyId=[Access Key ID]&AssociateTag=[ID]&SearchIndex=Apparel&Keywords=Shirt"
;;                          "")) )
