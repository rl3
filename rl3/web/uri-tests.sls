
(import
 (rnrs base)
 (only (rnrs control)
       when)
 (rl3 env debug)
 (rl3 test unit-tests)
 (rl3 web uri))

(debug-enable #f)

(begin 

  (when (test-runner-current)
    (test-runner-reset (test-runner-current)))
  
  (test-begin "URI Parsing - Test Suite")
  
  (test-begin "1. Authority Parsing")
  
  (test-begin "1.1 Simple Authority")
  
  (define auth (parse-authority "www.amazon.com"))
  
  (test-equal? "Simple authority"
               (authority-host auth)
               "www.amazon.com")
  
  (test-assert "No port" (not (authority-port auth)))
  
  (test-assert "No user" (not (authority-username auth)))

  (test-equal? "To String"
               (authority->string auth)
               "www.amazon.com")
  
  (test-end "1.1 Simple Authority")
  
  (test-begin "1.2 Host and Port")
  
  (define auth (parse-authority "www.bravais.org:8080"))
  
  (test-equal? "1.2 Host"
               (authority-host auth)
               "www.bravais.org")
  
  (test-equal? "1.2 Port"
               (authority-port auth)
               8080)
  
  (test-assert "1.2 No user" (not (authority-username auth)))
  
  (test-equal? "1.2 To string"
               (authority->string auth)
               "www.bravais.org:8080")
  
  (test-end "1.2 Host and Port")
  
  (test-begin "1.3 Full Authority")
  
  (define auth (parse-authority "ray@www.bravais.org:8080"))
  
  (test-equal? "1.3 Host"
               (authority-host auth)
               "www.bravais.org")
  
  (test-equal? "1.3 Port"
               (authority-port auth)
               8080)

  (test-equal? "1.3 User"
               (authority-username auth)
               "ray")
  
  (test-equal? "1.3 To string"
               (authority->string auth)
               "ray@www.bravais.org:8080")
  
  (test-end "1.3 Full Authority")  
  
  (test-end "1. Authority Parsing")

;;;

  (test-begin "2. URI Parsing")

   (define test-uris
     '("http://www.yahoo.com/a/b/c/d.txt"
       "http://www.yahoo.com"
       "http://www.yahoo.com/"
       "http://www.yahoo.com/a"
       "http://www.yahoo.com/a/b"
       "http://www.yahoo.com/a/b?sku=315515"
       "http://www.yahoo.com/a/b?sku=315515&quantity=10"
       "http://www.yahoo.com?sky=315515"
       "http://www.yahoo.com/a/b?sku=315515&quantity=10#ray"
       "http://www.yahoo.com/a/b#ray"
       "http://www.yahoo.com#ray"
       "file:"
       "file:///"
       "file:/a/b/c/d.txt"
       "file:a/b/c/d.txt"))
   
   (test-assert "2.1 Round Trip string -> T -> string"
                (equal? test-uris
                        (map (lambda (u)
                               (uri->string (parse-uri u)))
                             test-uris)))

   (test-begin "2.2 Parse Full URI")
   
   (define test-uri-str "http://ray@www.bravais.org:8080/a/b/c?d=1&e=2#f")
   
   (define test-uri (parse-uri test-uri-str))
   
   (test-assert "2.2.1 Authority"
                (equal? (uri-authority test-uri)
                        (authority->string (make-authority "ray" "www.bravais.org" 8080))))

   (test-equal? "2.2.2 Authority"
                (uri-authority test-uri)
                "ray@www.bravais.org:8080")

   (test-equal? "2.2.3. Authority"
                (uri-scheme test-uri)
                "http")                

   (test-equal? "2.2.4 Path"
                (uri-path test-uri)
                "/a/b/c")

   (test-equal? "2.2.5 Query"
                (uri-query test-uri)
                "d=1&e=2")

   (test-equal? "2.2.6 Frag"
                (uri-fragment test-uri)
                "f")
   
   (test-end "2.2 Parse Full URI")

   (test-begin "2.3 Path Parsing")

   (test-equal? "2.3.1 Simple Abs Path"
                (uri-path (parse-uri "file:/"))
                "/")

   ;; THIS IS PARSING WHEN IT IS FULL OF ERRORS ??? WHY????
   (test-error "2.3.2 URI with Abs Path MUST have a host"
               'error
               (parse-uri "file://bravais.org:a80a/b/c"))

   
   (test-end "2.3 Path Parsing")

   
   (test-end  "2. URI Parsing")
   

  (test-end "URI Parsing - Test Suite")

  )
