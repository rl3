(library  
 (rl3 system unix)
 
 (export
  F_GETFL F_SETFL O_NONBLOCK
  EAGAIN EBADF EIO EINTR
  get-errno errno->symbol
  unix-descriptor-closed? unix-descriptor-set-nonblock
  unix-execl unix-read unix-write unix-close unix-dup2 unix-fcntl
  unix-fork unix-perror unix-pipe unix-wait unix-waitpid
  open-input-port-from-descriptor open-output-port-from-descriptor)
 
 (import
  (rnrs base)
  (only (rnrs bytevectors)
        make-bytevector)
  (only (rnrs arithmetic fixnums)
        fixnum? fx+ fx>? fx=? fxior)
  (only (rnrs control)
        do)
  (only (rnrs lists)
        memv)
  (only (rnrs mutable-pairs)
        set-cdr!)
  (only (rnrs bytevectors)
        bytevector-u8-ref
        bytevector-u8-set!)
  (only (rnrs io simple)
        display newline)
  (only (rnrs io ports)
        make-custom-binary-input-port make-custom-binary-output-port)
  (only (rl3 ffi ffi-std)
        %peek-pointer %get-int
        sizeof:int
        foreign-procedure)
  (primitives append!) ;; FIXME RPR
  (only (rl3 io print)
        format)
  (for (rl3 ffi foreign-ctools)
       run expand))

 (define-c-info
   (include<> "fcntl.h")
   (include<> "errno.h")

   (const F_GETFL    int "F_GETFL")
   (const F_SETFL    int "F_SETFL")
   (const O_NONBLOCK int "O_NONBLOCK")

   (const EAGAIN     int  "EAGAIN")
   (const EBADF      int  "EBADF")
   (const EINTR      int  "EINTR")
   (const EIO        int  "EIO")
   )

 (define errno->symbol
   (lambda (eno)
     (cond
      ((eq? EAGAIN eno)
       'EAGAIN)
      ((eq? EBADF eno)
       'EBADF)
      ((eq? EIO eno)
       'EIO)
      ((eq? EINTR eno)
       'EINTR)
      (else 'UNKNOWN))))

;;; Error
 
 (define (c-variable-getter varname locname)    
   (let ((get-loc (foreign-procedure locname '() 'uint)))
     (lambda ()
       (%peek-pointer (get-loc)))))
 
 ;; int get_errno( void )
 ;; Returns errno.
 (define get-errno
   (c-variable-getter "errno" "__errno_location"))
 
 ;; int get_h_errno( void )
 ;; Returns h_errno.
 (define get-h-errno
   (c-variable-getter "h_errno" "__h_errno_location"))

;;; IO  
 
 (define unix-fcntl (foreign-procedure "fcntl" '(int int long) 'int))


 (define unix-descriptor-closed?
   (lambda (fd)
     (if (fixnum? fd)
         (let ((status (unix-fcntl fd F_GETFL 0)))
           (display "fd-closed? ")(display status)(newline)
           (fx=? status EBADF))
         (begin
           (display "fd-closed? #t")(newline) #t))))
 
 ;; Sets the socket file descriptor to non-blocking mode.
 ;; returns: boolean true on success
 ;; FIXME does not belong here RPR.
 (define unix-descriptor-set-nonblock
   (lambda (fd)
     (let ((flgs (unix-fcntl fd F_GETFL 0)))
       (if (fx>? flgs 0)
           (fx>? (unix-fcntl fd F_SETFL
                             (fxior flgs O_NONBLOCK)) 0)
           #f))))
 
 ;; read(2)
 ;; int read( int fd, void *buf, int n )
 (define unix-read (foreign-procedure "read" '(int boxed int) 'int))

 ;; write(2)
 ;; int write( int fd, void *buf, int n )
 (define unix-write (foreign-procedure "write" '(int boxed int) 'int))
 
 (define unix-close (foreign-procedure "close" '(int) 'int))
 
 (define unix-perror (foreign-procedure "perror" '(string) 'void))

 ;; pipe(2)
 ;; int pipe( int fildes[2] )
 (define unix-pipe 
   (let ((pipe (foreign-procedure "pipe" '(boxed) 'int)))
     (lambda ()
       (let ((array (make-bytevector (* sizeof:int 2))))
         (let ((r (pipe array)))
           (if (= r -1)
               (values -1 -1 -1)
               (values r (%get-int array 0) (%get-int array sizeof:int))))))))
 
 ;; wait(2)
 ;; pid_t wait( int *stat_loc )
 ;; Returns { return value, *stat_loc }
 (define unix-wait
   (let ((wait (foreign-procedure "wait" '(boxed) 'int)))
     (lambda ()
       (let ((buffer (make-bytevector sizeof:int)))
         (let ((r (wait buffer)))
           (values r (%get-int buffer 0)))))))
 
 ;; waitpid(2)
 ;; pid_t waitpid(pid_t pid, int *stat_loc, int options)
 ;;
 ;; Options defaults to 0.
 ;; Returns { return value, *stat_loc }
 (define unix-waitpid
   (let ((waitpid (foreign-procedure "waitpid" '(int boxed int) 'int)))
     (lambda (pid . rest)
       (let ((options (if (null? rest) 0 (car rest))))
         (let ((buffer (make-bytevector sizeof:int)))
           (let ((r (waitpid pid buffer options)))
             (values r (%get-int buffer 0))))))))


 
 ;; dup2(3C)
 ;; int dup2( int fildes, inf fildes2 )
 (define unix-dup2 (foreign-procedure "dup2" '(int int) 'int))

;;; Process


 (define make-list
   (lambda (n filler)
     (let loop ((n n) (lst '()))
       (if (zero? n)
           lst
           (loop (- n 1) (cons filler lst))))))
 
 ;; execl(2)
 ;;
 ;; execl( char *path, char *arg0, ..., char *argn, char * /* NULL */ )
 ;;
 ;; Example: (unix/execl "/usr/bin/ksh" "ksh" "-c" "echo Hello, sailor!")
 ;;
 ;; The cache is a hack that is necessary only because foreign functions
 ;; are not (yet) weakly held, thus repeated use of vfork(2) will cause 
 ;; foreign functions to be accumulated in the parent's address space.
 
 (define unix-execl
   (let ((cache '#()))
     (lambda (path . args)
       (let ((l (length args)))
         (if (>= l (vector-length cache))
             (let ((new-cache (make-vector (+ l 1) #f)))
               (do ((i 0 (+ i 1)))
                   ((= i (vector-length cache)))
                 (vector-set! new-cache i (vector-ref cache i)))
               (set! cache new-cache)))
         (if (not (vector-ref cache l))
             (vector-set! cache l
                          (foreign-procedure
                           "execl" (make-list (+ l 2) 'string) 'int)))
         (apply (vector-ref cache l) path (append! args '(#f)))))))
 
 ;; fork(2)
 ;; pid_t fork( void )
 (define unix-fork (foreign-procedure "fork" '() 'int))

 ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
 ;; This is getting messy.  FIXME RPR
 ;; This code was expanded to accomodate sockets, however, by only
 ;; including the fd in the closure socket/port code became awkward.
 ;; The below code was forked into the sockets library, but I still
 ;; need for the process code below.  There should be a common core
 ;; here I can fix up later.
 ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

 ;; Sockets will open the same descriptor multiple times.  We do not
 ;; want to actually close the "file" until the last one.  Solving
 ;; this problem by tracking open file descriptors in a bag.
 ;; FIXME O(n) yucko
 ;; FIXME RPR - Ref counting on fd in a hashtable??? 
 (define bag '(sentinel)) ;; sentinel simplifies set! logic below
 
 (define (add-fd-ref! fd)
   (set-cdr! bag (cons fd (cdr bag))))
 
 (define (rem-fd-ref! fd) ;; returns #t if fd not in result bag.  O/w #f.
   (let loop ((l bag))
     (cond ((null? (cdr l)) 
            (error 'rem-ref! ": no references left!"))
           ((eqv? fd (cadr l))
            (set-cdr! l (cddr l)))
           (else 
            (loop (cdr l)))))
   (not (memv fd (cdr bag))))
 
 (define open-input-port-from-descriptor
   (lambda (fd)
     
     (define id (string-append "<input port: unix descriptor " (number->string fd) ">"))
     
     ;; buffer? -> fixnum? -> fixnum? -> fixnum?
     (define (read! buf start count)
       (assert (fixnum? count))
       (format #t "Socket read! at ~s for ~s~%" start count)
       (cond ((zero? start) ;; fast path
              (let ((cnt (unix-read fd buf count)))
                (format #t
                        "Fast Read ~s bytes.~%" cnt)
                (if (fx=? cnt -1)
                    (let* ((errno  (get-errno))
                           (err (errno->symbol errno)))
                      (display "R1-ERRNO: ") (display errno)(newline)
                      (case err
                        ((EAGAIN) (display "Read-1 EAGAIN") (newline) 0)
                        (else (unix-perror (string-append "read!-2(" (symbol->string err) ")"))))
                      0)
                    cnt))) ;; FIXME For now assume -1 is from EAGAIN and return 0.
             (else 
              ;; FIXME wouldn't it be nice to avoid copying; look into
              ;; revising unix.sch accordingly
              (let* ((bv (make-bytevector count))
                     (cnt (unix-read fd bv count)))
                (do ((i 0 (fx+ i 1))
                     (j start (fx+ j 1)))
                    ((fx=? i cnt))
                  (bytevector-u8-set! buf j (bytevector-u8-ref bv i)))
                (format #t
                        "Read ~s bytes.~%" cnt)
                (if (fx=? cnt -1)                   
                    (let* ((errno  (get-errno))
                           (err (errno->symbol errno)))
                      (display "R2-ERRNO: ") (display errno)(newline)
                      (case err
                        ((EAGAIN) (display "Read-2 EAGAIN") (newline) 0)
                        (else (unix-perror (string-append "read!-2(" (symbol->string err) ")"))))
                      0)                   
                    cnt))))) ;; FIXME For now assume -1 is from EAGAIN and return 0.
     
     (define (close)
       (cond ((rem-fd-ref! fd)
              (let ((res (unix-close fd)))
                (cond ((< res 0)
                       (error 'close ": error " res 
                              " closing input descriptor port " fd)))))))
     
     (add-fd-ref! fd)
     (unix-descriptor-set-nonblock fd)
     (make-custom-binary-input-port id read! #f #f close)))

 (define open-output-port-from-descriptor
   (lambda (fd)

     (define id (string-append "<output port: unix descriptor " (number->string fd) ">"))
     
     (define (subbytevector bv start-incl end-excl)
       (let ((ret (make-bytevector (- end-excl start-incl))))
         (do ((i start-incl (+ i 1))
              (j 0 (+ j 1)))
             ((= i end-excl) ret)
           (bytevector-u8-set! ret j (bytevector-u8-ref bv i)))))
     
     ;; FIXME wouldn't it be nice to avoid copying here; look into
     ;; revising unix.sch accordingly
     ;; FIXME R6RS says count of 0 should have effect of passing EOF to
     ;; byte sink.  What does that mean in this context?
     ;; He's right.  Need to use pointer offset into the bytevector. RPR
     ;; Since I'm now the fd is in NON-BLOCK mode returning zero, means EOF
     (define (write! buf idx count) 
       (let ((cnt (unix-write fd (subbytevector buf idx (+ idx count)) count)))
         (if (fx=? cnt -1)
             (let* ((errno  (get-errno))
                    (err (errno->symbol errno)))
               (display "W1-ERRNO: ") (display errno)(newline)
               (case err
                 ((EAGAIN) 0)
                 (else (unix-perror (string-append "write!(" (symbol->string err) ")"))))
               0)                   
             cnt)))
     
     (define (close) 
       (cond ((rem-fd-ref! fd)
              (let ((res (unix-close fd)))
                (cond ((not (zero? res))
                       (error 'close ": error " res " closing custom output "
                              "descriptor port " fd)))))))
     
     (add-fd-ref! fd)
     ;;(descriptor-nonblock fd)
     (make-custom-binary-output-port id write! #f #f close)))

 )

(library

 (rl3 system unix process)

 (export process)

 (import
  (rnrs base)
  (only (rl3 system unix)
        unix-execl unix-pipe unix-fork unix-close unix-dup2
        open-input-port-from-descriptor
        open-output-port-from-descriptor))
 
 
 ;; PROCESS: Create a subprocess and set up communication with it.
 ;;
 ;; Returns a list (input-port output-port process-id), where data written
 ;; to output-port are available on standard input in the subprocess and
 ;; data written on standard output in the subprocess are available on
 ;; input-port.
 ;;
 ;; Compatible with Chez Scheme.  A modern version might return multiple values.
 
 (define (process command)
   (let-values (((r1 stdin-in stdin-out) (unix-pipe))
                ((r2 stdout-in stdout-out) (unix-pipe)))
     (let ((pid (unix-fork)))
       (case pid
         ((-1)
          (error "Failed to fork a subprocess."))
         ((0)
          (unix-close stdout-in)
          (unix-close stdin-out)
          (unix-dup2 stdin-in 0)
          (unix-dup2 stdout-out 1)
          (unix-execl "/bin/sh" "sh" "-c" command))
                                        ; FIXME: signal an error here, but can't use ERROR, I think.
         ;;(unix-exit 1))
         (else 
          (unix-close stdin-in)
          (unix-close stdout-out)
          (list (open-input-port-from-descriptor stdout-in)
                (open-output-port-from-descriptor stdin-out)
                pid))))))
 )


;; print system error 
;;(define perror (foreign-procedure "perror" '(string) 'void))
;;)