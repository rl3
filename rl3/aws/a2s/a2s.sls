(library
  (rl3 aws a2s a2s)

  (export keyword-search
          item-lookup)
  
  (import
   (rnrs base)
   (only (rnrs io simple)
         display newline)
   (only (rl3 aws configuration)
         aws-configuration)
   (only (rnrs io ports)
         close-port)
   (only (rl3 aws awscredentials)
         aws-credentials-associate-tag
         aws-credentials-secret-key
         aws-credentials-access-key)
   (only (rl3 web uri)
         make-uri uri->string)
   (only (rl3 web http)
         http-invoke http-ascii-port-from-binary-port)
   (only (rl3 web pipes htmlprag)
         html->sxml)
   (only (rl3 types dates)
         current-time-rfc2822)
   (only (rl3 aws awsauth)
         aws-s3-auth-str
         aws-s3-auth-mac)
   (only (rl3 aws s3 s3headers)
         host-header date-header)
   (only (rl3 web uri url parms)
         parms->query)
   (only (rl3 aws s3 s3headers)
         host-header))

  (define ecs-host (aws-configuration 'host))
  
  (define ecshost-header (host-header ecs-host))
  
  (define search-parms
    '( ("Operation"   . "ItemSearch")
       ("SearchIndex" . "KindleStore")))

  (define itemlookup-parms
    '(("Operation"   . "ItemLookup")))
  
  (define ecs-parms
    (lambda (creds)
      `(("Service"        . "AWSECommerceService")
        ("Version"        . "2008-03-03")         
        ("Associate-Tag"  . ,(aws-credentials-associate-tag creds))
        ("AWSAccessKeyId" . ,(aws-credentials-access-key creds)))))
    
  (define keyword-search
    (lambda (creds words)
      (let ((parms (append search-parms (ecs-parms creds)
                           `(("Keywords" . ,words)))))
        (let ((uri (make-uri "http" #f ecs-host #f "/onca/xml" (parms->query parms) "")))
          (let-values (((hdrs hip) (http-invoke 'GET (uri->string uri)
                                                `(,ecshost-header))))
            (let ((tip (http-ascii-port-from-binary-port hip)))
              (let ((results (html->sxml tip)))
                (close-port tip)
                results)))))))
  
  (define item-lookup
    (lambda (creds asin)
      (let ((parms (append itemlookup-parms (ecs-parms creds)
                           `(("IdType" . "ASIN")
                             ("ItemId" . ,asin)
                             ("ResponseGroup" . "SalesRank,Small,EditorialReview,Reviews")))))
        (let ((uri (make-uri "http" #f ecs-host #f "/onca/xml" (parms->query parms) "")))
          (let-values (((hdrs hip) (http-invoke 'GET (uri->string uri)
                                                `(,ecshost-header))))
            (let ((tip (http-ascii-port-from-binary-port hip)))
              (let ((results (html->sxml tip)))
                (close-port tip)
                results)))))))
  )


  