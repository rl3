;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Bravais' Edito Princeps: EBook Tool Suite	    
;; Copyright (C) 2007  Raymond Paul Racine
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(library
 (rl3 aws awsauth)

 (export
  md5-file
  aws-s3-auth-str
  aws-s3-auth-mac
  aws-s3-auth-mac-encode)
 
 (import
  (rnrs base)
  (only (rnrs bytevectors)
        utf8->string string->utf8)
  (only (rnrs io ports)
        open-file-input-port
        close-port)
  (rl3 crypto hash md5)
  (rl3 crypto hash sha1)
  (rl3 crypto base64)
  (only (rl3 text text)
        separate)
  (only (rl3 web uri url parms)
        encode-parm)
  (only (rl3 types strings)
        string-trim-both))
 
 ;; (require "awsmisc.scm"
 ;;           (lib "md5.ss")
 ;;           ;; (lib "head.ss" "net")
 ;;           (only (lib "13.ss" "srfi") string-trim-both)
 ;;           (lib "base64.ss" "net")
 ;;           (lib "uri-codec.ss" "net")
 ;;           (planet "hmac-sha1.ss" ("jaymccarthy" "hmac-sha1.plt" 1 1)))
 
 
 (define (md5-file fname)
   (let ((inf (open-file-input-port fname 'binary)))
     (let ((hash (md5 inf)))
       (close-port inf)
       hash)))
 
 (define (aws-s3-auth-str verb md5 mime expiration amz-headers resource)
   (let ((sep "\n"))
     (if (null? amz-headers)
         (separate sep (list verb md5 mime expiration resource))
         (separate sep (list verb md5 mime expiration (separate sep amz-headers) resource)))))
 
 (define (aws-s3-auth-mac key str)
   (string-trim-both (base64-encode (hmac-sha1 (string->utf8 key)
                                               (string->utf8 str)))))
 
 (define (aws-s3-auth-mac-encode key str)
   (encode-parm (aws-s3-auth-mac key str)))
 
 )
