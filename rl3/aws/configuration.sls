;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Simple storage of configuration values.
;; we could have multiple configuration maps 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(library
  (rl3 aws configuration)

  (export s3-configuration
          aws-configuration)

  (import
   (rnrs base)
   (only (rnrs lists)
         assoc))

  (define aws-configuration-map
    `("AWS Configuration" . ,(quasiquote  (( host . "ecs.amazonaws.com")
                                           (credentials-path . "/home/ray/awsaccount.txt")))))
                                                                                                                   
  (define s3-configuration-map
    `("S3 Configuration" . ,(quasiquote ((host . "s3.amazonaws.com")
                                        ; Path to file containing AWS id and keys
                                         (credentials-path . "/home/ray/awsaccount.txt")
                                        ; S3 bucket for storage of books
                                         (bucket . "bravais")
                                        ; Common library for uploaded ebooks
                                         (library . "library")))))
  
  (define (configuration-value configuration property)
    (let ((prop (assoc property (cdr configuration))))
      (if (pair? prop)
          (cdr prop)
          (error 'get-property-value "Configuration: '~a' does not have property '~a'." (car configuration) property))))
  
  (define (s3-configuration prop)
    (configuration-value s3-configuration-map prop))

  (define (aws-configuration prop)
    (configuration-value aws-configuration-map prop))
  
  )
