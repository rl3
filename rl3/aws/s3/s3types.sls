;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Bravais' Edito Princeps: EBook Tool Suite	    
;; Copyright (C) 2007  Raymond Paul Racine
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(library
 (rl3 aws s3 s3types)

 (export
  s3-bucket? s3-key? s3-key->string
  make-s3-resource s3-resource? s3-resource->string)

(import
 (rnrs base)
 (only (rl3 types lists)
       every?)
 (only (rl3 text text)
       weave))
 
  ;;;;;;;;;;;;;
;; S3 Bucket
  ;;;;;;;;;;;;;  
(define (s3-bucket? bucket)
  (string? bucket))

  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; S3 key
;; listof string?
;; list of path segments in a uri
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define (s3-key? key)
  (and (list? key)
       (every? string? key)))

(define (s3-key->string key)
  (weave key "/"))

  ;;;;;;;;;;;;;;;
;; S3 Resource
  ;;;;;;;;;;;;;;;

(define (make-s3-resource bucket key)    
  (cons bucket key))

(define (s3-resource? res)
  (and (pair? res)
       (s3-bucket? (car res))
       (s3-key? (cdr res))))    

(define (s3-resource->string res)
  (if (null? (cdr res))
      (string-append "/" (car res))
      (string-append "/" (car res) "/" (s3-key->string (cdr res)))))

)
