;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Bravais' Edito Princeps: EBook Tool Suite	    
;; Copyright (C) 2007  Raymond Paul Racine
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(library
 (rl3 aws s3 s3-response)
 
 (export
  s3-response? 
  s3-response-close
  s3-response-from-port  
  s3-response-debug-dump
  s3-http-header
  s3-http-code
  s3-http-version
  s3-http-message
  s3-http-response-fields
  s3-http-response-field)
 
 (import
  (rnrs base)
  (only (rnrs io ports)
        close-port)
  (only (err5rs records syntactic)
        define-record-type)
  (only (rnrs io simple)
        display newline)
  (only (rl3 text regexp)
        regexp-compile
        regexp-match))
 
 
 (define-record-type s3-response #t #t http port)
 
 (define s3-response-from-port
   (lambda (ip)
     (make-s3-response (purify-port ip) ip)))
 
 (define s3-response-close
   (lambda (s3-response)
     (close-port (s3-response-port s3-response))))
 
 (define s3-http-response-fields
   (lambda (s3-response)
     (extract-all-fields (s3-response-http s3-response))))
 
 (define s3-http-response-field
   (lambda (field-name-str s3-response)
     (extract-field field-name-str (s3-response-http s3-response))))
 
 (define (header-extract s3-response regexp)
   (cadr (regexp-match regexp (s3-response-http s3-response))))
 
 ;;(define response-test "HTTP/1.1 403 Forbidden\r\n\r\n")

 (define http-response-header-regex
   (regexp-compile "(HTTP)/([0-9][.][0-9]) ([0-9]+) (.+$)"))
 
 (define (s3-response-headers response)
   (s3-response-http response))

 (define match-http-header-regexp
   (regexp-match "^HTTP/.*?)\r\n\r\n|\n\n|\r\r"))
 
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
 ;; s3-response -> string
 ;; response -> "HTTP/1.1 403 Forbidden"
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
 (define (s3-http-header s3-response)
   (header-extract s3-response match-http-header-regexp))

 (define match-http-code-regexp
   (regexp-match "HTTP/[0-9][.][0-9] ([0-9]+) "))
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
 ;; Give a standard HTTP response string extract the code
 ;; "HTTP/1.1 403 Forbidden" -> 403
 ;; string -> number
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
 (define (s3-http-code s3-response)
   (string->number (header-extract s3-response match-http-code-regexp)))

 (define match-http-version-regexp
   (regexp-compile "HTTP/([0-9][.][0-9]) "))
 
 ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
 ;; "HTTP/1.1 403 Forbidden" -> "1.1"
 ;; s3-response? -> string
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
 (define (s3-http-version s3-response)
   (header-extract s3-response match-http-version-regexp))

 (define match-403-regexp
   (regexp-compile "HTTP/[0-9][.][0-9] [0-9]+ (.+$)"))
 
 ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
 ;; "HTTP/1.1 403 Forbidden" -> "Forbidden"
 ;; s3-response? -> string
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
 (define (s3-http-message s3-response)
   (header-extract s3-response match-403-regexp))
 
 ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
 ;; s3-response -> void
 ;; Dump out a response to the console
 ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
 (define (s3-response-debug-dump s3-response)
   (display "---------S3 Response---------")(newline)
   (display (s3-response-http s3-response))
   (display "---------Payload-------------")(newline))
  ;;  (let ([inport (s3-response-port s3-response)])
;;      (if (equal? (s3-http-response-field "Content-Type" s3-response) "application/xml")
;;          (display (read-xml inport))
;;          #f)
;;      ;;(display-pure-port inport))
;;      (close-port inport)
;;      (newline)(display "---------End Payload---------")(newline)))
 )
