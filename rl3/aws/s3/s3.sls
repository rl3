;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Bravais' Edito Princeps: EBook Tool Suite	    
;; Copyright (C) 2007  Raymond Paul Racine
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(library
 (rl3 aws s3 s3)

 (export
  list-buckets         ;; (-> aws-credentials? s3-response?))
  ;;   create-bucket        ;; (-> aws-credentials? s3-bucket? s3-response?))
  ;;   delete-bucket        ;; (-> aws-credentials? s3-bucket? s3-response?))
  ;;   put-object           ;; (-> aws-credentials? bytes? s3-resource? s3-response?))
  ;;   put-file-object      ;; (-> aws-credentials? path? s3-resource? s3-response?))
  ;;   head-object          ;; (-> aws-credentials? s3-resource? s3-response?))
  ;;   get-object           ;; (-> aws-credentials? s3-resource? s3-response?))
  ;;   delete-object        ;; (-> aws-credentials? s3-resource? s3-response?))
  ;;   list-bucket-objects) ;; (-> aws-credentials? s3-bucket? s3-key? string? number? s3-response?)))    
  )
 
 (import
  (rnrs base)
  (rl3 aws configuration)
  (only (rnrs io ports)
        close-port)
  (only (rl3 aws awscredentials)
        aws-credentials-secret-key
        aws-credentials-access-key)
  (only (rl3 web uri)
        make-uri uri->string)
  (only (rl3 web http)
        http-invoke http-ascii-port-from-binary-port)
  (only (rl3 web pipes htmlprag)
        html->sxml)
  (only (rl3 types dates)
        current-time-rfc2822)
  (only (rl3 aws awsauth)
        aws-s3-auth-str
        aws-s3-auth-mac)
  (only (rl3 aws s3 s3headers)
        host-header date-header))
 
 ;; Read from configuration 
 (define s3-host (s3-configuration 'host))
 (define s3host-header (host-header s3-host))
 
;;  (define aws-error
;;    (lambda (s)
;;      (display s) (newline)))
 
 (define authorization-header
   (lambda (credentials auth-str)
     (string-append "Authorization: AWS " (aws-credentials-access-key credentials) 
                    ":" (aws-s3-auth-mac (aws-credentials-secret-key credentials) auth-str))))


 (define build-rest-uri
   (lambda (path)
     (make-uri "http" #f s3-host #f path "" "")))
 
;;  (define make-bucket-uri
;;    (lambda (bucket)
;;      (let ((uri (make-base-uri)))
;;        (uri-path-set! uri (list bucket))
;;        uri)))
 
 (define list-buckets
   (lambda (credentials)        
     (let ((url (build-rest-uri ""))
           (datetime (current-time-rfc2822)))
       (let ((http-headers (list s3host-header
                                 (date-header (current-time-rfc2822))
                                 (authorization-header credentials
                                                       (aws-s3-auth-str "GET" "" "" datetime '() "/")))))
         (let-values (((hdrs hip) (http-invoke 'GET
                                               (uri->string url)
                                               http-headers)))
           (let ((tip (http-ascii-port-from-binary-port hip)))
             (let ((buckets (html->sxml tip)))
               (close-port tip)
               buckets)))))))
       
       ;;(s3-response-from-port (s3-get (list-url) http-headers)))))
 
;;  (define create-bucket
;;    (lambda (credentials bucket)
;;      (let* ((datetime (rfc2822-date))
;;             (bucket-resource (make-s3-resource bucket (make-s3-key '())))
;;             (http-headers (list (date-header datetime) 
;;                                 (authorization-header credentials (aws-s3-auth-str "PUT" "" "" 
;;                                                                                    datetime '() 
;;                                                                                    (s3-resource->string bucket-resource))))))
;;        (s3-response-from-port (s3-put (make-bucket-url bucket) "" http-headers)))))
 
;;  (define delete-bucket
;;    (lambda (credentials bucket)
;;      (let* ((datetime (rfc2822-date))
;;             (bucket-resource (make-s3-resource bucket (make-s3-key '())))
;;             (http-headers (list (date-header datetime) 
;;                                 (authorization-header credentials (aws-s3-auth-str "DELETE" "" "" datetime '() 
;;                                                                                    (s3-resource->string bucket-resource))))))
;;        (s3-response-from-port (s3-delete (make-bucket-url bucket) http-headers)))))
 
;;  (define list-bucket-objects
;;    (lambda (credentials bucket prefix-key marker max)
;;      (define (make-list-url path)
;;        (let ((url (make-base-url)))
;;          (set-url-path! url path)
;;          (set-url-query! url `((prefix . ,(s3-key->string prefix-key))
;;                                (marker . ,marker)
;;                                (max-keys . ,(number->string max))))
;;          (display (url->string url))(newline)
;;          url))      
;;      (let* ((datetime (rfc2822-date))
;;             (s3-resource (make-s3-resource bucket (make-s3-key '())))
;;             (http-headers (list (date-header datetime)
;;                                 (authorization-header credentials (aws-s3-auth-str "GET" "" "" datetime '() (s3-resource->string s3-resource))))))
;;        (s3-response-from-port (s3-get (make-list-url s3-resource) http-headers)))))
 
;;  (define make-object-url
;;    (lambda (resource)
;;      (let ((url (make-base-url)))
;;        (set-url-path! url resource)
;;        url)))
 
;;  (define put-file-object
;;    (lambda (credentials in-file-path s3-resource)
;;      (when (file-exists? in-file-path)
;;        (let* ((size (file-size in-file-path))
;;               (ip (open-input-file in-file-path))
;;               (buff (read-bytes size ip))
;;               (close-input-port ip))
;;          (put-object credentials buff s3-resource)))))
 
;;  (define put-object
;;    (lambda (credentials bytes s3-resource)
;;      (let* ((size (bytes-length bytes))
;;             (hash64 (bytes->string/utf-8 (let ((enc (base64-encode (md5 bytes #f)))) 
;;                                            (subbytes enc 0 (- (bytes-length enc) 2))))) ;; base64-encode adds a bogus \r\n 
;;             (mime "binary/octet-stream")
;;             (datetime (rfc2822-date))
;;             (url (make-object-url s3-resource))
;;             (http-headers (list (date-header datetime)
;;                                 (content-type mime)                         
;;                                 (content-md5 hash64)
;;                                 (authorization-header credentials 
;;                                                       (aws-s3-auth-str "PUT" hash64 mime datetime '() 
;;                                                                        (s3-resource->string s3-resource))))))   
;;        (s3-response-from-port (s3-put url bytes http-headers)))))
 
;;  (define get-object
;;    (lambda (credentials s3-resource)
;;      (let* ((datetime (rfc2822-date))
;;             (http-headers (list (date-header datetime)
;;                                 (authorization-header credentials 
;;                                                       (aws-s3-auth-str "GET" "" "" datetime '() 
;;                                                                        (s3-resource->string s3-resource))))))
;;        (s3-response-from-port (s3-get (make-object-url s3-resource) http-headers)))))
 
;;  (define head-object
;;    (lambda (credentials s3-resource)
;;      (let* ((datetime (rfc2822-date))           
;;             (http-headers (list (date-header datetime)
;;                                 (authorization-header credentials 
;;                                                       (aws-s3-auth-str "HEAD" "" "" datetime '() 
;;                                                                        (s3-resource->string s3-resource))))))
;;        (s3-response-from-port (s3-head (make-object-url s3-resource) http-headers)))))
 
;;  (define delete-object
;;    (lambda (credentials s3-resource)
;;      (let* ((datetime (rfc2822-date))
;;             (http-headers (list (date-header datetime) 
;;                                 (authorization-header credentials 
;;                                                       (aws-s3-auth-str "DELETE" "" "" datetime '() 
;;                                                                        (s3-resource->string s3-resource))))))
;;        (s3-response-from-port (s3-delete (make-object-url s3-resource) http-headers)))))
 
 )
