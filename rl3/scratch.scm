(import (rnrs base)
        (rnrs io simple)
        (rl3 io print)
        (rl3 aws a2s a2s)
        (rl3 aws awscredentials)
        (rl3 concurrency tasks-with-io))

(define creds (load-credentials "/home/ray/awsaccount.txt"))

(with-tasking-io
 (lambda ()
   (display (pretty-print (keyword-search creds "sml%20language")))))

(with-tasking-io
 (lambda ()
   (display (pretty-print (item-lookup creds "B000V78UWM")))))

;;==================================================================


(import (rnrs base)
        (rnrs io simple)
        (rl3 io print)
        (rl3 aws s3 s3)
        (rl3 aws awscredentials)
        (rl3 concurrency tasks-with-io))

(define creds (load-credentials "/home/ray/awsaccount.txt"))

(with-tasking-io
 (lambda ()
   (display (pretty-print (list-buckets creds)))))
   

;;; S3

(import (rnrs base)
        (rnrs bytevectors)
        (rl3 env debug)
        (rl3 types dates)
        (rl3 web uri)
        (rl3 web http)
        (rl3 aws awscredentials)
        (rl3 aws configuration)
        (rl3 crypto hash sha1)
        (only (rl3 aws s3 s3headers)
              date-header host-header)
        (rl3 aws awsauth))

;;;

(import (rnrs base)
        ;;(rnrs r5rs)
        ;;(rnrs lists)
        (rnrs bytevectors)
        (rnrs io simple)
        (rnrs io ports)
        (rnrs unicode)
        (rnrs mutable-pairs)
        ;;(rnrs mutable-strings)
        (rl3 concurrency tasks-with-io)
        (rl3 env debug)
        (rl3 io net sockets)
        (rl3 io print)
        (rl3 web http)
        (rl3 web pipes htmlprag)
        (primitives time))

(define creds  (load-credentials "/home/ray/awsaccount.txt"))

(define base-uri (make-uri "http" #f (s3-configuration 'host) #f "" "" ""))

(define build-rest-uri
  (lambda (path)
    (make-uri "http" #f (s3-configuration 'host) #f path "" "")))

(define bucket-root-uri (build-rest-uri ""))

(define tstamp (current-time-rfc2822))
  
(define dheader (date-header tstamp))

(define auth-header
  (lambda (creds auth-str)
    (string-append "Authorization: AWS " (aws-credentials-access-key credentials) 
                   ":" (aws-s3-auth-mac (aws-credentials-secret-key credentials) auth-str))))

(define auth-str (aws-s3-auth-str "GET" "" "" tstamp '() "/"))

(define authorization-header
  (lambda (credentials auth-str)
    (string-append "Authorization: AWS " (aws-credentials-access-key credentials) 
                   ":" (aws-s3-auth-mac (aws-credentials-secret-key credentials) auth-str))))

(define list-headers (list (host-header (s3-configuration 'host))
                           dheader (authorization-header creds auth-str)))

(with-tasking-io
 (lambda ()
   (let-values (((hdrs hip) (http-invoke 'GET
                                         (uri->string bucket-root-uri)
                                         list-headers)))
     (display hdrs)
     (newline)
     (let ((tip (http-ascii-port-from-binary-port hip)))
       (display (html->sxml tip))
       (close-port tip)))))

;;;
;;;
;;;

(import (rnrs base)
        ;;(rnrs r5rs)
        ;;(rnrs lists)
        (rnrs bytevectors)
        (rnrs io simple)
        (rnrs io ports)
        (rnrs unicode)
        (rnrs mutable-pairs)
        ;;(rnrs mutable-strings)
        (rl3 concurrency tasks-with-io)
        (rl3 env debug)
        (rl3 io net sockets)
        (rl3 io print)
        (rl3 web http)
        (rl3 web pipes htmlprag)
        (primitives time))

(let ()
  (debug-enable #t)
  (with-tasking-io
   (lambda ()
     (let ((surl "GET /home/will/Larceny/ HTTP/1.1\r\nHost: www.ccs.neu.edu\r\nUser-Agent: Mozilla/5.0 (X11; U; Linux x86_64; en-US; rv:1.8.1.5) Gecko/20070718 Fedora/2.0.0.5-1.fc7 Firefox/2.0.0.5\r\nAccept: text/xml,application/xml,application/xhtml+xml,text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5\r\nAccept-Language: en-us,en;q=0.5\r\Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7\r\n\r\n")
           (s (client-socket "www.ccs.neu.edu" 80)))
       (let ((op (socket-output-port s))
             (ip (socket-input-port s)))
         (put-bytevector op (string->utf8 surl))
         (flush-output-port op)
         (let ((req (http-request ip)))
           (let ((hip (http-req-input-port req)))
             (let ((tip (http-ascii-port-from-binary-port hip)))
               (format #t "Header: ~a Len: ~a~%" (http-req-start-line req) (http-req-size req))
               (display (html->sxml tip))))))))))
                              
;;                (let loop ((ch (get-char tip)) (cnt 0))
;;                  (if (eof-object? ch)
;;                      (begin
;;                        (display "Total Response Bytes: ")
;;                        (display cnt)
;;                        (newline)
;;                        (socket-close s))
;;                      (begin
;;                        (display ch)
;;                        (loop (get-char tip) (+ cnt 1))))))))))))
  
  ;; (require 'tasking-with-io)

;; (define (do-n thunk n)
;;   (let loop ((n n))
;;     (if (> n 0)
;;         (begin
;;           (thunk)
;;           (loop (sub1 n))))))

;; (define (hi)
;;   (display 'hi)
;;   (newline))

;; (define (hello)
;;   (display 'hello)
;;   (newline))

;; (define (done)
;;   (display 'done)
;;   (newline))

;; (define (main)
;;   (with-tasking (lambda ()
;;                   (let ((t1 (spawn (lambda () (do-n hi 100))))
;;                         (t2 (spawn (lambda () (do-n hello 100)))))
;;                     (let loop ((ts (all-tasks)))
;;                       (if (> (length ts) 1)
;;                           (begin
;;                             (yield)
;;                             (loop (all-tasks)))
;;                           (end-tasking
;;                            (begin
;;                              (done)
;;                              'goodbye))))))))
                            
;; (main)                    

;; (do-n hi 10)

;; (display "Go\n")

;; (require 'srfi-0)
;; (require 'Experimental/webserver/web-server.sch)

(library
 (scratch)

 (export EPOLLIN);; EPOLLIN EPOLLOUT)
 
 (import 
  (rnrs base)
  (rnrs io simple)
  (ffi foreign-ctools))
 
 (c-info (include<> "sys/epoll.h")
   ;; EPoll Events
   (const EPOLLIN int "EPOLLIN")
   (const EPOLLPRI int "EPOLLPRI")
   (const EPOLLOUT int "EPOLLOUT")
   (const EPOLLRDNORM int "EPOLLRDNORM")
   (const EPOLLRDBAND int "EPOLLRDBAND")
   (const EPOLLWRNORM int "EPOLLWRNORM")
   (const EPOLLWRBAND int "EPOLLWRBAND")  
   (const EPOLLMSG int "EPOLLMSG")
   (const EPOLLERR int "EPOLLERR")
   (const EPOLLONESHOT int "EPOLLONESHOT")   ;; no more fixnum?
   (const EPOLLET uint "EPOLLET")

   ;; EPoll Ops
   (const EPOLL-CTL-ADD int "EPOLL_CTL_ADD")
   (const EPOLL-CTL-DEL int "EPOLL_CTL_DEL")
   (const EPOLL-CTL-MOD int "EPOLL_CTL_MOD")
   
   ;; EPoll Structures
   (sizeof EPOLL-SIZEOF-STRUCT-EPOLL-EVENT "struct epoll_event")
   (sizeof EPOLL-SIZEOF-EPOLL-DATA "union epoll_data")
   
   (struct "epoll_event"
           (EPOLL-EVENT-EVENTS "events")
           (EPOLL-EVENT-DATA   "data")))

 
 )
 
  (define run
    (lambda (t1 t2)
      (lambda ()
        (let ((t1 (make-thread t1 "t1"))
              (t2 (make-thread t2 "t2")))
          (thread-start! t1)
          (thread-start! t2)
          (thread-join! t1)
          (thread-join! t2)
          (let loop ((n 10))
            (if (zero? n)
                'done))))))

 (define athread
     (lambda ()
       (let loop ((n 100))
         (if (<= n 0)
             (begin
               (newline)
               (display "********DONE**********")
               (newline)
               (flush-output-port)
               'done)
             (begin
               (display n)
               (display ", ")
               (flush-output-port)
               ;;(thread-yield)
               (loop
                (- n 1)))))))

  ;; (define t1 t2)

(import (rnrs))
(import (rnrs io simple))
(import (rnrs io ports))
(import (rl3 io net sockets))
(import (rl3 io net ipaddress))
(import (rl3 concurrency tasks))
(import (rl3 concurrency tasks-with-io))

(define gurl "GET / HTTP/1.1\r\nHost: www.google.com\r\nUser-Agent: Mozilla/5.0 (X11; U; Linux x86_64; en-US; rv:1.8.1.5) Gecko/20070718 Fedora/2.0.0.5-1.fc7 Firefox/2.0.0.5\r\nAccept: text/xml,application/xml,application/xhtml+xml,text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5\r\nAccept-Language: en-us,en;q=0.5\r\Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7\r\nKeep-Alive: 300\r\nConnection: keep-alive\r\n\r\n")

(with-tasking-io
 (lambda ()
   (let ((t (make-task
             (lambda ()
               (let ()
                 (define s (client-socket "www.google.com" 80))
                 
                 (define op (socket-output-port s))
                 (define ip (socket-input-port s))
                 
                 (put-bytevector op (string->utf8 gurl))
                 (flush-output-port op)
                 
                 (let loop ((b (get-bytevector-n ip 1)))
                   (if (or (eof-object? b)
                           (zero? (bytevector-length b)))
                       (socket-close s)
                       (begin
                         (display (utf8->string b))
                         (newline)
                         (flush-output-port (current-output-port))
                         (loop (get-bytevector-n ip 1)))))))
             "worker")))
     (thread-start! t)
     (thread-join! t))))



(let ()
  (define s (client-socket "www.google.com" 80))
  
  (define op (socket-output-port s #t))
  (define ip (socket-input-port s #t))
  
  (put-bytevector op (string->utf8 gurl))
  (flush-output-port op)
  
  (let loop ((b (get-bytevector-n ip 1)))
    (if (or (eof-object? b)
            (zero? (bytevector-length b)))
        (socket-close s)
        (begin
          (display (utf8->string b))
          (newline)
          (flush-output-port (current-output-port))
          (loop (get-bytevector-n ip 1))))))


(socket-error s)
(socket-close s)
(socket-descriptor s)
(define s #f)

(library
 (test)
 
 (export doit)
 
 (import
  (rnrs)
  (rnrs lists)
  (rnrs io simple)
  (sys system unix)
  (sys system process))
 
(define reduce-r (lambda (pred l)
                   (let ((l1 (cdr l)))
                     (if (null? l1)
                         (car l)
                         (pred (car l) (reduce-r pred l1))))))

(define (call-with-input-pipe command pred)
  (let* ((results (process (if (string? command)
                               command
                               (reduce-r (lambda (arg result)
                                           (string-append arg " " result))
                                         command))))
         (result (pred (car results))))
    (close-input-port (car results))
    (close-output-port (cadr results))
    (unix-waitpid (caddr results)) ; important in order to remove process
    result))

(define doit
  (lambda ()
    (call-with-input-pipe '("date" "-I")
                          (lambda (ip)
                            (utf8->string (get-bytevector-n ip 512))))))
)

;; START HERE - OK must track length of HTTP header. Content-Length + Header = Total to read.

(let ((buffsz 32))
  (get-bytevector-n! (open-bytevector-input-port (make-bytevector 0))
                     (make-bytevector 32 0)
                     0 16))

(get-bytevector-n (open-bytevector-input-port (make-bytevector 0)) 16)





           
           (haip (http-ascii-port-from-binary-port hip))
           (html (html->sxml haip)))
      (display html)
      'done)))
               
          
;;       (if (chunked-encoding? header)
;;           (let loop ((chunk (get-chunk ip)))
;;             (if (eof-object? chunk)
;;                 (socket-close s)
;;                 (begin
;;                   (display "CHUNK -------------")
;;                   (newline)
;;                   ;;(display chunk)
;;                   ;;(newline)
;;                   (loop (get-chunk ip)))))))))
                

;;      (newline)
;;      (time (display (html->sxml ip)))
;;      (socket-close s))))

(import (rnrs))

(let ((bip (open-bytevector-input-port (string->utf8 "This is how the world ends, not with a bang, but with a whimper."))))
  (display (get-u8 bip))
  (let ((tip (html-port bip)))
    (display (read-char tip))
    (close-input-port tip)))

(define x 0)
(define l '#(1 2 3 4 5 6 7 8))

(define get
  (let ((idx 0))
    (lambda ()
      (let ((n (vector-ref l idx)))
        (set! idx (+ idx 1))
        n))))

(do ((i 4 (- i 1))
     (x (get) (get)))
    ((<= i 0))
  (display x))
