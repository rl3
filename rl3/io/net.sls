(library
 (rl3 io net ipaddress)
 
 (export
  make-ipaddress
  ipaddress?
  ipaddress->string
  ipaddress->bytevector-u8
  number->ipaddress ipaddress->number
  bytevector-u8->ipaddress)
 
 (import
  (rnrs base)
  (only (rnrs r5rs)
        quotient remainder)
  (only (rnrs bytevectors)
        u8-list->bytevector
        bytevector-u8-ref bytevector-u8-set!
        make-bytevector bytevector?)
  (only (rl3 io print)
        format open-output-string get-output-string))
 
 (define uh 16777216)
 (define ul 65536)
 (define lh 256)
 ;;(define ll 1) ;;not needed
 
 ;; (define make-ipv6address
 ;;   (lambda (b0 b1 b2 b3 b4 b5)
 ;;     (bytevector-u8->ipaddress `#(,b0 ,b1 ,b2 ,b3 ,b4 ,b5))))
 
 (define make-ipaddress
   (lambda (b0 b1 b2 b3)
     (let ((bv (make-bytevector 4)))
       (bytevector-u8-set! bv 0 b0)
       (bytevector-u8-set! bv 1 b1)
       (bytevector-u8-set! bv 2 b2)
       (bytevector-u8-set! bv 3 b3)
       bv)))

 (define ipaddress? bytevector?)
 
 (define bytevector-u8->ipaddress
   (lambda (bv) bv))

 (define ipaddress->bytevector-u8
   (lambda (ip) ip))
 
 (define number->ipaddress
   (lambda (n)
     (u8-list->bytevector (list (quotient n uh)
                                (remainder (quotient n ul) lh)
                                (remainder (quotient n lh) lh)
                                (remainder n lh)))))

 (define ipaddress->number
   (lambda (ip)
     (+ (* (bytevector-u8-ref ip 0) uh)
        (* (bytevector-u8-ref ip 1) ul)
        (* (bytevector-u8-ref ip 2) lh)
        (bytevector-u8-ref ip 3))))
 
 (define ipaddress->string
   (lambda (addr)
     (let ((octs (ipaddress->bytevector-u8 addr))
           (os (open-output-string)))
       (format os "~a.~a.~a.~a"
               (bytevector-u8-ref octs 0)
               (bytevector-u8-ref octs 1)
               (bytevector-u8-ref octs 2)
               (bytevector-u8-ref octs 3))
       (get-output-string os))))

 )

(library
 (rl3 io net nethostdb)

 (export
  ;; host-entry record
  host-entry? host-entry-name host-entry-aliases host-entry-type host-entry-addresses
  get-hostname get-host-by-name get-host-by-address)
 
 (import
  (rnrs base)
  (only (rnrs arithmetic fixnums)
        fx=? fx+ fx-)
  (only (rnrs bytevectors)
        bytevector-length bytevector->u8-list make-bytevector
        bytevector-u8-set! bytevector-u8-ref
        utf8->string)
  (only (rl3 ffi ffi-std)
        %get-int %get-pointer
        %peek-pointer-array %peek-string peek-bytes
        foreign-null-pointer? foreign-procedure)
  (err5rs records syntactic)
  (only (rl3 system unix)
        unix-perror)
  (for (rl3 ffi foreign-ctools)
       run expand)
  (only (rl3 io net ipaddress)
        bytevector-u8->ipaddress))

 #|=============================================================================
 name :: string - host name
 aliases :: (listof string)  - aliase name for the host
 type    :: 
 addresses :: (listof string)- the list of Internet addresses of the given host.
 =============================================================================|#
 
 (define-record-type host-entry #t #t name aliases type addresses)
 
 (define-c-info
   (include<> "netdb.h")
   (sizeof hostentry-size "struct hostent")
   (struct "hostent"
           (hostentry.o_h_name "h_name")
           (hostentry.o_h_aliases "h_aliases")
           (hostentry.o_h_addrtype "h_addrtype")
           (hostentry.o_h_length   "h_length")
           (hostentry.o_haddr_list "h_addr_list")))

 (define hostentry.h_name
   (lambda (he)
     (%peek-string (%get-pointer he hostentry.o_h_name))))
 
 (define hostentry.h_aliases
   (lambda (he)
     (%peek-pointer-array (%get-pointer he hostentry.o_h_aliases) %peek-string)))

 (define hostentry.h_addrtype
   (lambda (he)
     (%get-int he hostentry.o_h_addrtype)))

 ;; reverse as the first entry is the primary entry
 ;; put it first in the list.
 (define hostentry.h_addresses
   (lambda (he)
     (reverse (%peek-pointer-array (%get-pointer he hostentry.o_haddr_list)
                                   (lambda (addr)
                                     ;; AF_INET is 4 bytes FIXME for IP6
                                     (let ((a (make-bytevector 4)))
                                       (peek-bytes addr a 4)
                                       (bytevector-u8->ipaddress a)))))))
 
 #|=============================================================================
 Reads the network host database for a host with the given name.
 If none is found returns #f
 string -> 
 =============================================================================|#
 (define c-gethostbyname (foreign-procedure "gethostbyname" '(string) 'unsigned))
 
 (define get-host-by-name 
   (lambda (hostname)
     (let ((ptr (c-gethostbyname hostname))
           (host-entry-buf (make-bytevector hostentry-size)))
       (if (foreign-null-pointer? ptr)
           (begin
             (unix-perror "gethostbyname")
             #f)
           (begin
             (peek-bytes ptr host-entry-buf (bytevector-length host-entry-buf))
             (let ((name    (hostentry.h_name host-entry-buf))
                   (aliases (hostentry.h_aliases host-entry-buf))
                   (type    (hostentry.h_addrtype host-entry-buf))
                   (addrs   (hostentry.h_addresses host-entry-buf)))
               (make-host-entry name aliases type addrs)))))))

 (define get-host-by-address
   (lambda (addr)
     #f))

 (define c-gethostname (foreign-procedure "gethostname" '(boxed int) 'unsigned))

 (define get-hostname
   (lambda ()
     (define MAX-HOST 256)
     (define null-offset
       (lambda (bytes)
         (let loop ((idx 0))
           (if (fx=? (bytevector-u8-ref bytes idx) 0)
               idx
               (loop (fx+ idx 1))))))
     
     (let ((buff (make-bytevector MAX-HOST 0)))
       (let ((result (c-gethostname buff MAX-HOST)))
         ;; man page says truncation is unspecified 
         (bytevector-u8-set! buff (fx- MAX-HOST 1) 0) 
         (substring (utf8->string buff) 0 (null-offset buff))))))
 )
