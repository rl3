(library
 (rl3 io print)
 
 (export format get-output-string open-output-string pretty-print)

 (import (primitives format open-output-string get-output-string pretty-print))

 ;; format
 ;; Copyright 1998 Lars T Hansen.
 ;; 2002-09-28 / lth
 ;;
 ;; (format <port> <format-string> <arg> ...)
 ;;
 ;; <port> can be: an output port 
 ;;                #t, denoting (current-output-port)
 ;;                #f, denoting an output string (that is returned)
 ;;
 ;; The following formatting characters are valid:
 ;;   ~a   - write as with 'display'
 ;;   ~b   - write bytevector elements (decimal)
 ;;   ~B   - write bytevector elements (hexadecimal)
 ;;   ~c   - write character as with 'write-char'
 ;;   ~f   - write number as with 'display', though see below
 ;;   ~x   - write integer in base 16
 ;;   ~o   - write integer in base 8
 ;;   ~s   - write as with 'write', compatible with Chez Scheme
 ;;   ~w   - write as with 'write'
 ;;   ~?   - next two args are another format string and a list of
 ;;          arguments, respectively;; recursively invoke format on
 ;;          those arguments.
 ;;   ~%   - newline
 ;;   ~~   - write a ~
 ;;
 ;; ~a and ~w admit a field width specifier
 ;;   - an integer n formats the datum left-justified in a field that 
 ;;     is at least n characters wide, inserting spaces on the right 
 ;;     if necessary
 ;;   - an integer n followed by @ formats the datum right-justified,
 ;;     ditto.
 ;;
 ;; ~f admits a formatting specifier
 ;;   - integers w and d, comma separated, that format the datum
 ;;     left justified in a field at least w characters wide rounded
 ;;     to d decimal places.  w is optional in which case no leading
 ;;     spaces are printed;; ,d is optional in which case the number 
 ;;     is formatted as with number->string and an attempt is made to
 ;;     fit it in the field (FIXME: should probably round it to fit,
 ;;     if that's possible.)


 )
