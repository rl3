 #| Port of orginal from Larceny's Experimental/socket.sch |#

(library
 (rl3 io net sockets constants)
 
 (export
  family-protocol->string
  socket-type->string
  AF_UNIX AF_INET AF_INET6
  PF_INET
  IPPROTO_TCP IPPROTO_UDP
  SOCK_STREAM SOCK_DGRAM SOCK_RAW SOCK_RDM SOCK_SEQPACKET
  SOL_SOCKET SO_DEBUG SO_TYPE SO_ERROR SO_BROADCAST
  SO_KEEPALIVE SO_LINGER SO_RCVBUF SO_SNDBUF SO_REUSEADDR
  SHUT_RD SHUT_WR SHUT_RDWR)
 
 (import
  (rnrs base)
  (for (rl3 ffi foreign-ctools)
       run expand))

;;;
 ;; Note for the major protocols AF_XXX = PF_XXX
 ;; therefore AF_INET is for the family AND the protocol
 (define-c-info
   (include<> "sys/socket.h")
   (include<> "netinet/in.h")
   (const AF_UNIX        int "AF_UNIX")
   (const AF_INET        int "AF_INET")
   (const AF_INET6       int "AF_INET6")

   (const PF_INET        int "PF_INET")

   (const IPPROTO_TCP    int "IPPROTO_TCP")
   (const IPPROTO_UDP    int "IPPROTO_UDP")
   
   (const SOCK_STREAM    int "SOCK_STREAM")
   (const SOCK_DGRAM     int "SOCK_DGRAM")
   (const SOCK_RAW       int "SOCK_RAW")
   (const SOCK_RDM       int "SOCK_RDM")
   (const SOCK_SEQPACKET int "SOCK_SEQPACKET")

   (const SOL_SOCKET     int "SOL_SOCKET")   ;; options for socket level
   (const SO_DEBUG       int "SO_DEBUG")     ;; socket debugging
   (const SO_REUSEADDR   int "SO_REUSEADDR") ;; keep connections alive
   (const SO_TYPE        int "SO_TYPE")      
   (const SO_ERROR       int "SO_ERROR")

   (const SO_DONTROUTE   int "SO_DONTROUTE")  ;; just use interface addresses
   (const SO_BROADCAST   int "SO_BROADCAST")  ;; permit sending of broadcast msgs
   (const SO_SNDBUF      int "SO_SNDBUF")     ;; send buffer size
   (const SO_RCVBUF      int "SO_SNDBUF")     ;; receive buffer size
   (const SO_KEEPALIVE   int "SO_KEEPALIVE")  ;; keep connections alive
   (const SO_LINGER      int "SO_LINGER")     ;; linger on close if data present (in seconds)

   (const SHUT_RD        int "SHUT_RD")       ;; Stopped reading data on duplex connection
   (const SHUT_WR        int "SHUT_WR")       ;; Stopped writing data on duplex connection
   (const SHUT_RDWR      int "SHUT_RDWR")     ;; Harder close as closes on all forked fds as well.
   )
 
 (define family-protocol->string
   (lambda (num)
     (case num
       ((1)  "AF_UNIX" )
       ((2)  "AF_INET")
       ((10) "AF_INET6")
       (else "Unknown Address Family"))))
 
 (define socket-type->string 
   (lambda (type)
     (case type
       ((1) "SOCK_STREAM")
       ((2) "SOCK_DGRAM")
       ((3) "SOCK_RAW")
       ((4) "SOCK_RDM")
       ((5) "SOCK_SEQPACKET"))))
 )

(library
 (rl3 io net sockets unix)
 
 (export
  c-accept c-bind c-connect c-listen c-socket c-setsockopt c-shutdown
  make-sockaddr_in SOCKADDR_IN-SIZE
  sockaddr_in.sin_family-set!
  sockaddr_in.sin_addr-set! sockaddr_in.sin_addr
  sockaddr_in.sin_port-set!)
 
 (import
  (rnrs base)
  (only (rnrs bytevectors)
        make-bytevector bytevector-copy!)
  (only (rl3 ffi ffi-std)
        %get-ushort %set-ushort %get-uint %set-uint
        set-hton-16u get-ntoh-16u
        poke-bytes peek-bytes
        foreign-procedure foreign-file)
  (only (rl3 ffi foreign-ctools)
        define-c-info))
 
 (define c-accept (foreign-procedure "accept" '(int boxed boxed) 'int))
 
 (define c-socket (foreign-procedure "socket" '(int int int) 'int))

 (define c-setsockopt (foreign-procedure "setsockopt" '(int int int boxed unsigned) 'int))
 
 (define c-listen (foreign-procedure "listen" '(int int) 'int))
 
 (define c-connect (foreign-procedure "connect" '(int boxed int) 'int))

 (define c-bind (foreign-procedure "bind" '(int boxed int) 'int))

 (define c-shutdown (foreign-procedure "shutdown" '(int int) 'int))
 
 (define-c-info
   (include<> "netinet/in.h")
   (sizeof SOCKADDR_IN-SIZE "struct sockaddr_in")
   (struct "sockaddr_in"
           (sockaddr_in.o_sin_family "sin_family")
           (sockaddr_in.o_sin_addr   "sin_addr")
           (sockaddr_in.o_sin_port   "sin_port")))
 
 (define make-sockaddr_in
   (lambda ()
     (make-bytevector SOCKADDR_IN-SIZE 0)))

 (define sockaddr_in.sin_family
   (lambda (sockaddr)
     (%get-ushort sockaddr sockaddr_in.o_sin_family)))

 (define sockaddr_in.sin_family-set!
   (lambda (sockaddr family)
     (%set-ushort sockaddr sockaddr_in.o_sin_family family)))

 (define sockaddr_in.sin_port
   (lambda (sockaddr) ;; network to host (big endian to little)
     (get-ntoh-16u sockaddr sockaddr_in.sin_port)))
 
 (define sockaddr_in.sin_port-set!
   (lambda (sockaddr port) ;; host to network (little endian to big)
     (set-hton-16u sockaddr sockaddr_in.o_sin_port port)))

 (define sockaddr_in.sin_addr
   (lambda (sockaddr)  ;; in network byte order
     (let ((ip (make-bytevector 4 0))) 
       (bytevector-copy! sockaddr sockaddr_in.o_sin_addr ip 0 4)
       ip)))

 (define sockaddr_in.sin_addr-set!
   (lambda (sockaddr addr)  ;; in network byte order
     (bytevector-copy! addr 0  sockaddr sockaddr_in.o_sin_addr 4)))
 
 ;;(foreign-file "/lib/libsocket.so") 
 )

(library
 (rl3 io net sockets)
 
 (export
  server-socket server-socket-accept
  client-socket
  socket-close socket-open?
  socket-descriptor socket-error
  socket-input-port socket-output-port
  socket-input-port-bytes-read
  socket-input-port-read-limit-set!
  socket-input-port-read-limit-reset!)
 
 (import
  (rnrs base)
  (only (rnrs control)
        do when unless)
  (only (rnrs bytevectors)
        make-bytevector bytevector-length
        bytevector-u8-set! bytevector-u8-ref)
  (only (rnrs arithmetic fixnums)
        fx=? fx<? fx+ fxzero?)
  (only (rnrs lists)
        memq memv)
  (only (rnrs mutable-pairs)
        set-cdr!)
  (only (err5rs records syntactic)
        define-record-type)
  (only (r5rs)
        remainder quotient)
  (only (rl3 io print)
        format)
  (only (rnrs io simple)
        current-output-port
        close-input-port close-output-port
        display newline)
  (only (rnrs io ports)
        make-custom-binary-input-port
        make-custom-binary-output-port
        flush-output-port)
  (only (rl3 io net sockets constants)
        AF_INET PF_INET
        SOCK_STREAM IPPROTO_TCP
        SOL_SOCKET SO_LINGER SO_REUSEADDR SO_KEEPALIVE
        SO_SNDBUF SO_RCVBUF SO_BROADCAST
        SHUT_RD SHUT_WR SHUT_RDWR)
  (only (rl3 system unix)
        get-errno errno->symbol
        unix-read unix-write
        unix-descriptor-set-nonblock
        unix-descriptor-closed?
        unix-perror unix-close)
  (only (rl3 ffi ffi-std)
        sizeof:int 
        %set-uint %set-int %set-ushort)
  (only (rl3 io net nethostdb)
        get-host-by-name host-entry-addresses)
  (only (rl3 io net ipaddress)
        make-ipaddress ipaddress? ipaddress->bytevector-u8)
  (only (rl3 io net sockets unix)
        make-sockaddr_in SOCKADDR_IN-SIZE
        sockaddr_in.sin_port-set!
        sockaddr_in.sin_addr
        sockaddr_in.sin_family-set!
        sockaddr_in.sin_addr-set!
        c-accept c-bind c-connect c-socket c-listen c-setsockopt c-shutdown)
  (only (rl3 concurrency tasks-with-io)
        input-not-ready-handler)
  ;; FIXME RPRP
  (primitives io/make-port vector-like-set! vector-like-ref))

 ;; Opaque socket type (well I need to make it opaque)
 ;;
 ;; error: oneof 'OK, 'EAGAIN, 'EBADF, 'PEER-CLOSED
 
 (define-record-type socket #t #t (descriptor) flags (inp) (outp) (error))

 ;; FIXME BIGTIME RPR, This is from Larceny's iosys.sch
 (define port.iodata 7)
 
 ;; size of socket's iodata vector
 (define iodata.size 2)
 
 ;; iodata vector index of input limit
 ;; the max number of bytes to read from a port
 (define iodata.read-limit 0)
 
 ;; iodata index of total number of bytes read on socket
 (define iodata.bytes-read 1)
 
 ;; total number of bytes read on socket port
 ;; input-port? -> number?
 (define socket-input-port-bytes-read
   (lambda (ip)
     (vector-ref (vector-like-ref ip port.iodata) iodata.bytes-read)))
 
 (define increment-socket-input-port-bytes-read
   (lambda (iodata cnt)
     (vector-set! iodata
                  iodata.bytes-read
                  (fx+ cnt (vector-ref iodata iodata.bytes-read)))))

 (define socket-input-port-read-limit-set!
   (lambda (ip limit)
     (vector-set! (vector-like-ref ip port.iodata)
                  iodata.read-limit limit)))
 
 (define socket-input-port-read-limit-reset!
   (lambda (ip)
     (socket-input-port-read-limit-set! #f)))

 (define make-socket-iodata
   (lambda ()
     (let ((iodata (make-vector iodata.size)))
       (vector-set! iodata iodata.bytes-read 0)
       (vector-set! iodata iodata.read-limit #f)
       iodata)))
 
 (define socket-buffer-bytes-available 0)
 
 (define invalid?
   (lambda (sock-fd)
     (fx=? sock-fd -1)))

 (define accept c-accept)

 (define new-socket c-socket)

 (define listen c-listen)

 (define bind   c-bind)

 (define connect c-connect)

 (define shutdown c-shutdown)
 
 (define report-error
   (lambda (s)
     (unix-perror s)
     #f))
 
 (define open-socket-input-port
   (lambda (socket)
     
     (define id (string-append "socket input port-" (number->string (socket-descriptor socket))))
     
     (define read-proc
       (lambda (iodata buffer)
         (let ((fd (socket-descriptor socket)))
           (if fd
               (begin
                 (let ((bufsz (bytevector-length buffer)))
                   (let ((cnt (unix-read fd buffer bufsz)))
                     (cond
                      ((fx=? cnt -1)
                       (let* ((errno  (get-errno))
                              (errsym (errno->symbol errno)))
                         (socket-error-set! socket errsym)
                         (case errsym
                           ((EAGAIN)
                            (input-not-ready-handler fd)
                            (read-proc iodata buffer))
                           ((EBADF) 'error)    ;; raw socket  was closed, return 0.  Note returning 0  => PORT-EOF? -> #t in Larceny R6RS I/O system.
                           (else
                            (begin
                              (display (string-append "Unhandled Socket Input Port Read! error: "
                                                      (symbol->string errsym)))
                              (newline)
                              'error)))))
                      ((fxzero? cnt)  ;; peer closed socket
                       (display "Socket closed by peer")(newline)
                       (socket-error-set! socket 'PEER-CLOSED)
                       'eof)  ;; 'error ???
                      (else (begin
                              (increment-socket-input-port-bytes-read iodata cnt)
                              cnt))))))
               'eof))))  ;; fd #f => socket was closed
     
     ;; Close input port on socket
     ;; If the output port is closed as well do a full socket close
     ;; else do a shutdown to inform whomever we are talking to that
     ;; I will not be doing anymore reading of data.
     (define (close)
       (socket-inp-set! socket #f)
       (let ((fd (socket-descriptor socket)))
         (cond
          ((and fd
                (socket-outp socket))
           (c-shutdown fd SHUT_RD))
          (fd
           (socket-error-set! socket 'OK)
           (let ((rs (unix-close fd)))
             (socket-descriptor-set! socket #f)
             (if (fx<? rs 0)
                 (begin
                   (socket-error-set! socket (errno->symbol (get-errno)))
                   #f)
                 #t))))))
     
     (define iproc
       (lambda (op)
         (case op
           ((read)
            read-proc)
           ((close) (lambda (iodata)
                      (close)))
           ((name)  (lambda (iodata)
                      id))
           (else 
            (error 'make-socket-input-port "illegal operation" op)))))

     (unix-descriptor-set-nonblock (socket-descriptor socket))
     (let ((inp (io/make-port iproc (make-socket-iodata) 'input 'binary)))
       (socket-inp-set! socket inp)
       inp)))
          
 
 ;;  (define open-socket-input-port
 ;;    (lambda (socket blocked?)
 
 ;;      (define id (string-append "<socket input port-" (number->string (socket-descriptor socket)) ">"))
 
 ;;      ;; The current R6RS/Laceny I/O library will close the custom port IF read! returns 0.
 ;;      ;; Treating a socket as a (potential) indefinite stream of bytes means 0 can only be returned.
 ;;      ;; when the socket is closed and there are not lingering/unread bytes.
 ;;      ;;
 ;;      ;; We note and take advantage the current Larceny R6RS I/O impl always calls with start = 0.
 ;;      ;;
 ;;      ;; buffer? -> fixnum? -> fixnum? -> fixnum?
 
 ;;      (define read!
 ;;        (lambda (buf start count)
 ;;          (let ((fd (socket-descriptor socket)))
 ;;            (if fd
 ;;                (begin
 ;;                  (unless blocked?
 ;;                    (input-not-ready-handler fd)
 ;;                    (when (unix-descriptor-closed? fd)
 ;;                      (display "Socket closed while waiting.")(newline)))
 ;;                  (display "Socket Read: ")
 ;;                  (display count)
 ;;                  (let ((cnt (unix-read fd buf count)))
 ;;                    (display "  Actual: ")
 ;;                    (display cnt)
 ;;                    (newline)
 ;;                    (cond
 ;;                     ((fx=? cnt -1)
 ;;                      (let* ((errno  (get-errno))
 ;;                             (errsym (errno->symbol errno)))
 ;;                        (format #t "Fast Read ERRNO: ~s, ~s~%" errno errsym)
 ;;                        (socket-error-set! socket errsym)
 ;;                        (case errsym
 ;;                          ((EAGAIN) (display "EAGAIN") (newline) (read! buf start count))
 ;;                          ((EBADF)  (display "EBADF") (newline) 0)    ;; raw socket  was closed, return 0.  Note returning 0  => PORT-EOF? -> #t in Larceny R6RS I/O system.
 ;;                          (else
 ;;                           (begin
 ;;                             (display (string-append "Unhandled Socket Input Port Read! error: "
 ;;                                                     (symbol->string errsym)))
 ;;                             (newline)
 ;;                             0)))))
 ;;                     ((fx=? 0 cnt)   ;; peer closed their socket                     
 ;;                      (display "Socket closed by peer.")(newline)
 ;;                      (socket-error-set! socket 'PEER-CLOSED)
 ;;                      0)
 ;;                     (else cnt))))
 ;;                0))))  ;; fd #f => socket has been closed.
 
 ;;      ;; Close input port on socket
 ;;      ;; If the output port is closed as well do a full socket close
 ;;      ;; else do a shutdown to inform whomever we are talking to that
 ;;      ;; I will not be doing anymore reading of data.
 ;;      (define (close)
 ;;        (socket-inp-set! socket #f)
 ;;        (let ((fd (socket-descriptor socket)))
 ;;          (cond
 ;;           ((and fd
 ;;                 (socket-outp socket))
 ;;            (c-shutdown fd SHUT_RD))
 ;;           (fd
 ;;            (socket-error-set! socket 'OK)
 ;;            (let ((rs (unix-close fd)))
 ;;              (socket-descriptor-set! socket #f)
 ;;              (if (fx<? rs 0)
 ;;                  (begin
 ;;                    (socket-error-set! socket (errno->symbol (get-errno)))
 ;;                    #f)
 ;;                  #t))))))
 
 ;;      (let ((fd (socket-descriptor socket)))
 ;;        (unless blocked?
 ;;          (display "Setting Socket to NON-BLOCK.")(newline)
 ;;          (unix-descriptor-set-nonblock fd))
 ;;        (let ((inp (make-custom-binary-input-port id read! #f #f close)))
 ;;          (socket-inp-set! socket inp)
 ;;          inp)))) 
 
 (define open-socket-output-port
   (lambda (socket)
     
     (define id (string-append "<socket output port-" (number->string (socket-descriptor socket)) ">"))
     
     (define (subbytevector bv start-incl end-excl)
       (let ((ret (make-bytevector (- end-excl start-incl))))
         (do ((i start-incl (+ i 1))
              (j 0 (+ j 1)))
             ((= i end-excl) ret)
           (bytevector-u8-set! ret j (bytevector-u8-ref bv i)))))
     
     ;; FIXME wouldn't it be nice to avoid copying here; look into
     ;; revising unix.sch accordingly
     ;; FIXME R6RS says count of 0 should have effect of passing EOF to
     ;; byte sink.  What does that mean in this context?
     ;; He's right.  Need to use pointer offset into the bytevector. RPR
     ;; Since I'm now the fd is in NON-BLOCK mode returning zero, means EOF
     (define (write! buf idx count) 
       (let ((cnt (unix-write (socket-descriptor socket) (subbytevector buf idx (+ idx count)) count)))
         (if (fx=? cnt -1)
             (let* ((errno  (get-errno))
                    (err (errno->symbol errno)))
               (case err
                 ((EAGAIN) 0)
                 (else (unix-perror (string-append "write!(" (symbol->string err) ")"))))
               0)                   
             cnt)))
     
     (define (close)
       (socket-outp-set! socket #f)
       (let ((fd (socket-descriptor socket)))
         (cond
          ((and fd
                (socket-inp socket))
           (c-shutdown fd SHUT_WR))
          (fd
           (socket-error-set! socket 'OK)
           (let ((rs (unix-close fd)))
             (socket-descriptor-set! socket #f)
             (if (fx<? rs 0)
                 (begin
                   (socket-error-set! socket (errno->symbol (get-errno)))
                   #f)
                 #t))))))

     (let ((fd (socket-descriptor socket)))
       (unix-descriptor-set-nonblock fd))
     (let ((outp (make-custom-binary-output-port id write! #f #f close)))
       (socket-outp-set! socket outp)
       outp)))

 ;; create or return existing port on the file descriptor
 ;; for the given socket record type.
 (define socket-input-port
   (lambda (socket)
     (or (socket-inp socket)
         (open-socket-input-port socket))))

 (define socket-output-port
   (lambda (socket)
     (or (socket-outp socket)
         (open-socket-output-port socket))))

 ;; Returns the socket descriptor value
 ;; Returens:  #f if closed, the unix fd number otherwise.
 (define socket-open?
   (lambda (socket)
     (socket-descriptor socket)))

 ;; close a socket
 ;; In orginal Larceny lib code a socket fd could be multiplexed across
 ;; via multiple ports i.e., many port to one socket fd.
 ;; For now, 1 socket has at most, 1 input port, and 1 output port.
 (define socket-close
   (lambda (socket)
     (assert (socket? socket))
     (when (socket-descriptor socket)
       (when (socket-inp socket)
         (close-input-port (socket-inp socket)))
       (let ((op (socket-outp socket)))
         (when op
           (flush-output-port op)
           (close-output-port op)))
       (when (unix-descriptor-closed? (socket-descriptor socket))
         (socket-descriptor-set! socket #f)))))

 (define setsockopt c-setsockopt)

 (define set-socket-option
   (lambda (socket option)
     (let ((one (make-bytevector sizeof:int)))
       (%set-int one 0 1)
       (let ((rs (setsockopt socket SOL_SOCKET option
                             one sizeof:int)))
         (if (invalid? rs)
             (report-error "setsockopt")
             #t)))))

 (define client-socket
   (lambda (host port . flags)
     (let ((ip (cond
                ((ipaddress? host) host)
                ((string? host)
                 (let ((addrs (host-entry-addresses (get-host-by-name host))))
                   (if (pair? addrs)
                       (car addrs)
                       #f)))
                (else #f))))
       (if ip
           (let ((s (new-socket PF_INET SOCK_STREAM IPPROTO_TCP)))
             (if (invalid? s)
                 (report-error "socket")
                 (let ((addr (make-sockaddr_in)))
                   (sockaddr_in.sin_family-set! addr AF_INET)
                   (sockaddr_in.sin_addr-set!   addr ip)
                   (sockaddr_in.sin_port-set!   addr port)
                   (let ((rs (connect s addr SOCKADDR_IN-SIZE)))
                     (if (invalid? rs)
                         (report-error "connect")
                         (make-socket s flags #f #f 'OK))))))
           #f))))

 (define server-socket-accept
   (lambda (socket flags)
     (let ((addr (make-sockaddr_in))
           (addrlen (make-bytevector sizeof:int))
           (nonblocking? (memq 'nonblocking flags)))
       (sockaddr_in.sin_family-set! addr AF_INET)
       (%set-int addrlen 0 SOCKADDR_IN-SIZE)
       ;; for now!, assume task will block on i/o.
       ;; task will unblock iff epoll says its ready
       (input-not-ready-handler (socket-descriptor socket))
       (let ((sdesc (accept (socket-descriptor socket) addr addrlen)))
         (if (invalid? sdesc)
             (report-error "accept")
             (values (make-socket sdesc flags #f #f 'OK)(sockaddr_in.sin_addr addr)))))))

 (define server-socket 
   (lambda (port flags)     
     (let ((s (new-socket PF_INET SOCK_STREAM IPPROTO_TCP)))
       (if (invalid? s)
           (report-error "socket")
           (let ((addr (make-sockaddr_in)))
             (sockaddr_in.sin_family-set! addr AF_INET)
             (sockaddr_in.sin_addr-set! addr (make-ipaddress 127 0 0 1))
             (sockaddr_in.sin_port-set! addr port)
             (set-socket-option s SO_REUSEADDR)
             (if (invalid? (bind s addr (bytevector-length addr)))
                 (report-error "bind")
                 (if (invalid? (listen s 5))
                     (report-error "listen")
                     (make-socket s flags #f #f 'OK))))))))
 )
