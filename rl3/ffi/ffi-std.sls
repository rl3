(library
  (rl3 ffi ffi-std)

  (export
   sizeof:short sizeof:int sizeof:long sizeof:pointer
   %get8 %get16 %get32 %get64
   %set32
   %get-ushort %set-ushort %get-int %set-int %get-uint %set-uint %get-pointer
   set-hton-16u set-hton-32u
   get-ntoh-16u get-ntoh-32u
   %peek8 %peek16 %peek32 %peek64
   %poke32
   %peek-pointer %peek-pointer-array %peek-string peek-bytes
   poke-bytes
   foreign-null-pointer? foreign-procedure foreign-file)

  (import
   (rnrs base)
   
   (only (r5rs)
         remainder quotient)
   
   (primitives
    bytevector-ref bytevector-set!
    
    sizeof:short sizeof:int sizeof:long sizeof:pointer
 
    %get8 %get16 %get32 %get64
    %set32
    
    %get-ushort %set-ushort %get-int %set-int %get-uint %set-uint %get-pointer
    %peek8 %peek16 %peek32 %peek64
    %poke32
    %peek-pointer %peek-pointer-array %peek-string peek-bytes
    poke-bytes
    foreign-null-pointer? foreign-file foreign-procedure))

  (define hu 16777216)
  (define hl 65536)
  (define lu 256)
  ;;(define ll 1)
  
  (define get-ntoh-16u 
    (lambda (x offs)
      (+ (* (bytevector-ref x offs) lu)
         (bytevector-ref x (+ offs 1)))))
  
  (define set-hton-16u 
    (lambda (x offs val)
      (bytevector-set! x offs (quotient val lu))
      (bytevector-set! x (+ offs 1) (remainder val lu))))
  
  (define get-ntoh-32u
    (lambda (x offs)
      (+ (* (bytevector-ref x offs) hu)
         (* (bytevector-ref x (+ offs 1)) hl)
         (* (bytevector-ref x (+ offs 2)) lu)
         (bytevector-ref x (+ offs 3)))))
  
  (define set-hton-32u
    (lambda (x offs val)
      (bytevector-set! x offs (quotient val hu))
      (bytevector-set! x (+ offs 1) 
                       (remainder (quotient val hl) lu))
      (bytevector-set! x (+ offs 2)
                       (remainder (quotient val lu) lu))
      (bytevector-set! x (+ offs 3) (remainder val lu))))
  
  )
