;;; This file provides utilities for interoperating with libraries
;;; written in C.

;;;
;;; For example, the actual offset into a structure for a particular
;;; field is target-dependent, so rather than hard code the offsets
;;; into our code, we calculate it on the fly (at compile time) by
;;; emitting C code to tell us the offsets and then running that
;;; through the system C compiler.

;;; Wish list:
;;; * make code more robust by checking that input strings do not
;;;   inject malicious code

;;; Original: Larceny distribution lib/Standar/foreign-ctools.sch
;;; Modified: port to syntax-case macro - Ray Racine Dec 1, 2007

(library
 (rl3 ffi foreign-ctools)
 
 (export
  define-c-info)
 
 (import
  (for (rnrs base) run expand)
  (for (rnrs syntax-case) run expand)
  (for (only (rnrs lists)
             assq filter ) expand)
  (for (only (rnrs control)
             when) expand)
  (for (only (rnrs io simple)
             call-with-input-file call-with-output-file current-output-port 
	     display newline read) run expand)
  (for (only (rnrs files)
             delete-file) expand)
  (for (only (rl3 env larceny)
             getenv os-type system) expand)
  (for (only (rl3 io print)
	     get-output-string open-output-string format) expand))
 
 (define-syntax define-c-info
   (lambda (stx)
     
     (define filter-clauses
       (lambda (sym clauses)
         (filter (lambda (clause)
                   (eq? sym (car clause)))
                 clauses)))
     
     (define clause-values
       (lambda (sym clauses)
         (map cdr (filter-clauses sym clauses))))
     
     (define clause-values-atom
       (lambda (sym clauses)
	 (map car (clause-values sym clauses))))
     
     (define clause-value
       (lambda (sym clauses)
         (let ((val (clause-values sym clauses)))
           (when (> (length val) 1)
             (syntax-violation 'define-c-info (string-append "'" (symbol->string sym) "' can only be declared once.") stx))
           (if (null? val)
               #f
               (car val)))))

     ;; C includes     
     (define include<>-wrap
       (lambda (incl<>s)
	 (map (lambda (incl)
		(string-append "<" incl ">"))
	      incl<>s)))

     (define include-wrap
       (lambda (incls)
	 (map (lambda (incl)
		(string-append "\"" incl "\""))
	      incls)))

     (define gen-include
       (lambda (incls)
	 (map (lambda (incl)
		(string-append "#include " incl))
	      incls)))

     ;; C Consts
     (define (->c-type-rep type)
       (cond 
	((eq? type 'int)   (values "%d"  "int"))
	((eq? type 'uint)  (values "%u"  "unsigned int"))
	((eq? type 'long)  (values "%ld" "long"))
	((eq? type 'ulong) (values "%lu" "unsigned long"))
	(else (values "%od" "long"))))
         
     ;; '( (EPOLLPRI 'int "EPOLLPRIL") ...) -> '({ printf("%d ",((int)(EPOLLPRI)));} ...)
     (define gen-consts
       (lambda (consts)
	 
	 (define type cadr)
	 (define name caddr)
	 
	 (define (gen-const const)
	   (let ((name (name const)))
	     (call-with-values
		 (lambda () (->c-type-rep (type const)))
	       (lambda (fmt type)
                 (let ((os (open-output-string)))
		 (format os "{ printf (\"~a \" ,((~a)(~a)));}" fmt type name)
                 (get-output-string os))))))
	 
	 (map gen-const consts)))

     ;; SizeOf
     ;; (EPOLL-SIZEOF-STRUCT-EPOLL-EVENT "struct epoll_event") -> { printf("%lu ",((unsigned long)(sizeof(struct epoll_event))));}
     (define gen-sizeofs
       (lambda (sizeofs)

         (define gen-sizeof
           (lambda (sizeofdecl)
             (define struct-name cadr)
             (let ((os (open-output-string)))
               (format os "{ printf(\"%lu \",((unsigned long) (sizeof(~a))));}" (struct-name sizeofdecl))
               (get-output-string os))))
         
         (map gen-sizeof sizeofs)))

     ;; { struct epoll_event s;  printf("%ld ",((long)((char *)&s.data - (char *)&s)));}
     (define gen-structs
       (lambda (structs)
         (define gen-struct
           (lambda (struct)
             (define name   (car struct))
             (define fields (cdr struct))
             (define gen-struct-field
               (lambda (field)
                 (let ((os (open-output-string)))
                   (format os "{ struct ~a s; printf(\"%ld \",((long)((char *)&s.~a - (char *)&s))); }" name (cadr field))
                   (get-output-string os))))
             (map gen-struct-field fields)))
         (map gen-struct structs)))

     (define structs-names
       (lambda (structs)
         (let loop ((ss structs) (names '()))
           (if (null? ss)
               (apply append (reverse names))
               (loop (cdr ss) (cons (map car (cdar ss)) names))))))
     
     (define cc-compile
       (lambda (exe-path c-src-path paths)
         (let ((cc (or (getenv "CC") "cc"))
               (include-paths
                (apply string-append
                       (map (lambda (path)
                              (string-append "-I" path " "))
                            paths))))
           (let ((cc-cmd (string-append
                          cc " "
                          include-paths
                          " -m32 "
                          " -D_XOPEN_SOURCE=500 "
                          " -o " exe-path
                          " "  c-src-path)))
             (zero?
              (system cc-cmd))))))

     ;; FIXME RPR - Broken (Fix like above.
     (define cl-compile
       (lambda (exe-path c-src-path . includes)
         (let ((include-directives
                (apply string-append
                       (map (lambda (path)
                              (string-append "/I" path " "))
                            includes)))
               (obj-path (string-append c-src-path ".obj")))
           (let ((result (zero?
                          (system
                           (string-append
                            "cl /nologo "
                            include-directives
                            " /Fo" obj-path
                            " /Fe" exe-path
                            " "  c-src-path
                            "> nul:")))))
             (if result (delete-file obj-path))
             result))))

     (define temp-c-file "larceny-c-info.c")
     
     (define temp-c-exec (if (eq? os-type 'windows)
                             "larceny-c-info.exe"
                             "./larceny-c-info"))
     
     (define temp-c-outp "larceny-c-info-output")
     
     (define make-c-contents
       (lambda (c-prelude includes sizeofs structs)
         `(,@c-prelude
	   "#include <stdio.h>"
           "int main(int argc, char **argv) {"
           "  printf(\"\\n(\\n\");"
	   ,@(gen-consts includes)
           ,@(gen-sizeofs sizeofs)
           ,@(gen-structs structs)
           "  printf(\"\\n)\\n\");"
           "  return 0;"
           "}")))

     (define generate-c-code
       (lambda (c-contents)
         (call-with-output-file temp-c-file
           (lambda (out)
             ;;(define out (current-output-port))
             (define print
               (lambda (str)
                 (display str out)
                 (newline out)))
             (for-each (lambda (content)
                         (if (list? content)
                             (for-each print content)
                             (print content)))
                       c-contents)))))
     
     (define compile-c-code
       (lambda (c-compile include-paths)
         (c-compile temp-c-exec temp-c-file include-paths)))
     
     (define run-c-program
       (lambda ()
         (system (string-append temp-c-exec
                                " > " temp-c-outp))))
     
     (define read-output
       (lambda ()
         (call-with-input-file temp-c-outp read)))
     
     (define delete-temps
       (lambda ()
         (delete-file temp-c-file)
         (delete-file temp-c-exec)
         (delete-file temp-c-outp)))     
     
     (syntax-case stx ()
       ((define-c-info clause ...)        
        (let ((clauses (syntax->datum (syntax (clause ...)))))
          (let ((compiler   (clause-value  'compiler  clauses))
                (paths      (clause-values 'path      clauses))
                (includes   (include-wrap   (clause-values-atom 'include   clauses)))
                (include<>s (include<>-wrap (clause-values-atom 'include<> clauses)))
		(consts     (clause-values 'const     clauses))
                (sizeofs    (clause-values 'sizeof    clauses))
                (structs    (clause-values 'struct    clauses))
                (c-compile (case os-type
                             ((unix)    cc-compile)
                             ((windows) cl-compile))))
            (let ((c-contents   (make-c-contents (gen-include (append includes include<>s))
                                                 consts
                                                 sizeofs
                                                 structs)))
              		
		(generate-c-code c-contents)
                
		(if (not (compile-c-code c-compile paths))
		    (error 'define-c-value ": compiler error."))

		(run-c-program)
		
		(let ((c-values     (read-output))
		      (scheme-names `(,@(map car consts)
                                      ,@(map car sizeofs)
                                      ,@(structs-names structs))))
                  ;;(delete-temps)
                  (let ((defs (map (lambda (name value)
                                     `(define ,name ,value))
                                   scheme-names c-values)))
                    (with-syntax ((defs-stx (datum->syntax #'define-c-info (cons 'begin defs))))
                                  (syntax defs-stx)))))))))))
     )
