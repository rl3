(module json-writer mzscheme
  
  (provide json-write json-empty-array json-empty-object)
  
  (define json-empty-object 'json-empty-object)
  (define json-empty-array  'json-empty-array)
  
  (define kvsep        ":")
  (define elemsep      ",")  
  (define object-start "{")
  (define object-end   "}")  
  (define array-start  "[")
  (define array-stop   "]")
  (define jtrue        "true")
  (define jfalse       "false")
  (define jnull        "null")
  
  (define (write-value v sop)
    (printf "Writing value: ~s~n" v)
    (cond 
      ((string? v)   (write v sop))
      ((number? v)   (display v sop))
      ((boolean? v)  (if v 
                         (display jtrue sop)
                         (display jfalse sop)))
      ((eq? v (void)) (display jnull sop))
      ((list? v)     (write-json-complex-type v sop))
      ((eq? v json-empty-object) (display "{}" sop))
      ((eq? v json-empty-array)  (display "[]" sop))
      (else
       (error 'write-value "Unable to write unknown type with value ~a~n" v))))
  
  (define (write-pair lst sop)
    (let ((p (car lst)))
      (if (pair? p)
          (let ([key (car p)])
            (cond
              [(string? key)
               (write key sop)]
              [(symbol? key)
               (write (symbol->string key) sop)]
              [else 
               (error 'write-pair "Key is not a string or symbol: ~a." key)])
            (display kvsep sop)
            (write-value (cdr p) sop)
            (if (null? (cdr lst))
                (cdr lst)
                (begin
                  (display elemsep sop)
                  (write-pair (cdr lst) sop)))))))
  
  (define (write-json-object lst sop)
    (display object-start sop)
    (write-pair lst sop)
    (display object-end sop))
  
  (define (write-values lst sop)
    (if (null? lst)
        (void)
        (begin
          (write-value (car lst) sop)
          (when (not (null? (cdr lst)))
            (display elemsep sop))
          (write-values (cdr lst) sop))))
  
  (define (write-json-array lst sop)
    (display array-start sop)
    (write-values lst sop)
    (display array-stop sop))
  
  (define (write-json-complex-type lst sop)
    (if (null?  lst)  ;; ambiguous empty array or object??
        (display "{}" sop)  
        (if (pair? (car lst))
            (write-json-object lst sop)
            (write-json-array lst sop))))
  
  (define (json-write json)
    (cond 
      [(eq? json 'json-empty-object)
       "{}"]
      [(eq? json 'json-empty-array)
       "[]"]
      [(list? json)        
       (let ([sop (open-output-string "json-write")])
         (write-json-complex-type json sop)
         (get-output-string sop))]
      (error 'json-write "Invalid structure to be encoded as json.  Given: ~a~n" json)))
  
  )
