(library
 (rl3 web rpc json test)
  
  (require "json-writer.scm"
           (planet "text-ui.ss" ("schematics" "schemeunit.plt" 2 8))
           (planet "test.ss" ("schematics" "schemeunit.plt" 2 8)))
  
  (define json-object-write
    (test-suite
     "JSON Object Writer"
     (test-equal? "Empty Object" (json-write '()) "{}")
     (test-equal? "Simple types" (json-write `(("a" . 1) ("b" . "c") ("d" . #t) ("e" . #f) ("g" . ,(void))))
                  "{\"a\":1,\"b\":\"c\",\"d\":true,\"e\":false,\"g\":null}")
     (test-equal? "Nested Json" (json-write `(,(cons "a" (list "a" "b" 3)) ,(cons "b" (list (cons "d" "ray")))))
                  "{\"a\":[\"a\",\"b\",3],\"b\":{\"d\":\"ray\"}}")))
  
  (define json-array-write
    (test-suite
     "JSON Array Write"
     (test-equal? "Empty Array" (json-write json-empty-array) "[]")
     (test-equal? "Simple array types" (json-write `("a" 1 #t #f ,(void)))
                  "[\"a\",1,true,false,null]")
     (test-equal? "Nested array" (json-write `("a" ,(list 1 2 3)))
                  "[\"a\",[1,2,3]]")))
  
  (test/text-ui 
   (test-suite "JSON Writer Tests" 
               json-object-write
               json-array-write))
  
  )