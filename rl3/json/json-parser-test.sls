(module json-parser-test mzscheme
  
  (require "json-parser.scm"
           (planet "text-ui.ss" ("schematics" "schemeunit.plt" 2 8))
           (planet "test.ss" ("schematics" "schemeunit.plt" 2 8)))
  
  (define json-object-parse
    (test-suite
     "JSON Parser Object Test"
     (test-equal? "Empty Object" (json-parse "{}") '())
     (test-equal? "Simple Types" (json-parse "{\"a\":123, \"b\":\"c\", \"d\":true,\"e\":false,\"f\":null}") 
                  `(("a" . 123) ("b" . "c") ("d" . #t) ("e" . #f) ("f" . ,(void))))
     (test-equal? "Nested object" (json-parse "{\"a\": {\"b\":123}}")
                  (list (cons "a" (list (cons "b" 123)))))))
  
  (define json-array-parse
    (test-suite
     "JSON Parser Array Test"
     (test-equal? "Empty Array" (json-parse "[]") '())
     (test-equal? "Simple Types" (json-parse "[\"a\",123, true, false, null]")
                  `("a" 123 #t #f ,(void)))))
  
  (define json-nested-parse
    (test-suite
     "JSON Nested Test"
     (test-equal? "Nested Object" (json-parse "{\"a\":{\"b\":{\"d\": 123}}}")
                  (list (list "a" (cons "b" (list (cons "d" 123))))))
     (test-equal? "Nested Array" (json-parse "{\"a\" : [123, \"b\"]}") 
                  (list (cons "a" (list 123 "b"))))))
  
  (test/text-ui 
   (test-suite "JSON Parser Tests" 
               json-object-parse
               json-array-parse
               json-nested-parse))
  
  )