(library
 (rl3 rpc json json-parser)
 
 (export json-parse)

 (import
  (rnrs base))
 
 (define delims    
   '((#\, . comma)
     (#\: . colon)
     (#\{ . object-start)
     (#\} . object-end)
     (#\[ . array-start)
     (#\] . array-end)))
 
 (define (json-delimiter? ch)
   (assq ch delims))
 
 (define (json-string? ch ip)
   (if (eq? #\" ch)
       (read ip)
       #f))
 
 (define (json-number? ch ip)
   (case ch
     ((#\- #\0 #\1 #\2 #\3 #\4 #\5 #\6 #\7 #\8 #\9)
      (read ip))
     (else #f)))
 
 (define (json-true/false/null? ch ip)
   (case ch
     ((#\t) (read ip) 'true)
     ((#\f) (read ip) 'false)
     ((#\n) (read ip) 'null)
     (else #f)))
 
 (define (lex ip)
   (let ((ch (peek-char ip)))
     (cond 
      [(char-whitespace? ch) (begin (read-char ip) (lex ip))]
      ((json-delimiter? ch) => (λ (delim) (read-char ip) (cdr delim)))
      ((json-string? ch ip)) 
      ((json-number? ch ip))
      ((json-true/false/null? ch ip)) 
      ((eof-object? ch) 'eof)
      (else (error 'lex
                   "Failed to lex a valid token.  Char: ~a~n" ch)))))
 
 (define (parse-delimiter delim ip)
   (let ([token (lex ip)])
     (if (eq? token delim)
         #t
         (error 'parse-delimiter 
                "Expected delimeter ~a found ~a." delim token))))
 
 (define (parse-value ip)
   (let ([token (lex ip)])
     (case token
       [(object-start)
        (parse-object ip '())]
       [(array-start)
        (parse-array ip '())]
       ((true) #t)
       ((false) #f)
       ((null) (void))
       (else (if (or (string? token)
                     (number? token))
                 token
                 (error 'parse-value
                        "Expected value, found ~a." token))))))
 
 (define (parse-pair key ip)
   (parse-delimiter 'colon ip)
   (cons key (parse-value ip)))
 
 (define (parse-object ip obj)
   (let ((token (lex ip)))
     (cond
      [(eq? token 'comma) ;; if no objs then bad grammer e.g. {,}
       (if (null? obj)
           (error 'parse-object "Expected a pair but found ','. Maybe {,}?")
           (parse-object ip obj))]
      [(string? token)
       (parse-object ip (cons (parse-pair token ip) obj))]
      [(eq? token 'object-end)
       (reverse! obj)]        ;; ok optimization
      [else (error 'object-start
                   "Expected a string, found ~a.~n" token)])))
 
 (define (parse-array ip array)
   (let ((token (lex ip)))
     (cond 
      [(or (string? token)
           (number? token))
       (parse-array ip (cons token array))]
      [else 
       (case token
         [(true)  (parse-array ip (cons #t array))]
         [(false) (parse-array ip (cons #f array))]
         [(null)  (parse-array ip (cons (void) array))]
         [(comma)
          (if (null? array)
              (error 'parser-array "Expected a value but found just ','.  Maybe [,]?")
              (parse-array ip array))]
         [(object-start)
          (parse-array ip (cons (parse-object ip '()) array))]
         [(array-start)
          (parse-array ip (cons (parse-array ip '()) array))]
         [(array-end)
          (reverse! array)]
         (else (error 'parse-array "Unexpected token ~a~n" token)))])))
 
 ;; str -> (or alist (listof (union string? number? #t #f <void>)))
 (define (json-parse json-str)
   (let ((ip (open-input-string json-str)))
     (let ((token (lex ip)))
       (case token
         [(object-start)
          (parse-object ip '())]
         [(array-start)
          (parse-array ip '())]
         [else "Invalid start of JSON"]))))
 
 )