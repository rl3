;; Initially Reference implementation taken from srfi.schemers.org on
;; 2004-01-04, subsequently lightly adapted to Larceny.

;; Ported to R6RS, Ray Racine 2/10/08

(library
 (rl3 types dates)

 (export
  current-time
  current-time-rfc2822
  local-tz-offset
  date->string
  time->date
  current-julian-day
  get-time-of-day time-utc->date)
 
 (import
  (rnrs base)
  (only (rnrs bytevectors)
        make-bytevector)
  (only (rnrs io simple)
        peek-char
        display newline)
  (only (rnrs io ports)
        put-char get-char
        get-datum get-line eof-object?
        open-file-input-port)
  (only (rnrs lists)
        assoc find member memq)
  (only (rnrs control)
        do when)
  (only (rnrs mutable-strings)
        string-set!)
  (only (rnrs unicode)
        char-numeric?
        char-alphabetic?)
  (err5rs records syntactic)        
  (rl3 ffi ffi-std)
  (only (r5rs)
        quotient remainder modulo
        exact->inexact inexact->exact)
  (rl3 env parameters)
  (primitives open-input-string
              open-output-string
              get-output-string
              memstats
              memstats-system-time
              memstats-user-time
              memstats-gc-total-cpu-time))

 ;; Simple interface to the system clock.
 ;; 2004-01-11 / lth
 ;;
 ;; The epoch (time = 0) is 1 January 1970 00:00:00 UTC.
 ;;
 ;; (current-utc-time) => [seconds microseconds] 
 ;;    Returns the current time, as an offset from the epoch.
 ;;
 ;; (timezone-offset seconds) => seconds'
 ;;    Given a time in seconds relative to the epoch, returns the offset 
 ;;    of local time from UTC at that time expressed in seconds, with
 ;;    positive offsets to the east of Greenwich.
 
 ;; On Linux the epoch is 1/1/1970 00:00:00 UTC, which is right for
 ;; the date package.  I've no idea if the use of UTC is consistent
 ;; across Unix dialects.
 ;;
 ;; local-tz-offset must return positive values east of Greenwich.
 ;; Unix normally reports negative values east of Greenwitch.
 
 ;; date date structure
 (define-record-type date #t #t  (nanosecond) (second) (minute) (hour) (day) (month) (year) (zone-offset))

 (define-record-type time #t #t type nanosecond second)
 
 (define copy-time
   (lambda (time)
     (make-time (time-type time)
                (time-nanosecond time)
                (time-second time))))
 
 (define current-utc-time
   (let ((_gettimeofday 
          (foreign-procedure "gettimeofday" '(boxed boxed) 'int)))
     (lambda ()
       (let ((tv (make-bytevector 8)))
         (_gettimeofday tv #f)
         (values (%get32 tv 0) (%get32 tv 4))))))
 
 (define timezone-offset
   (let ((_localtime 
          (foreign-procedure "localtime" '(boxed) 'uint))
         (_gmtime 
          (foreign-procedure "gmtime"    '(boxed) 'uint))
         (_mktime
          ;; mktime takes a pointer to storage owned by the C runtime, aka a uint.
          ;; (We need to fix our FFI interface to express this directly)
          (foreign-procedure "mktime"    '(uint) 'uint))
         (_difftime
          (foreign-procedure "difftime"  '(uint uint) 'double)))
     (lambda (t)
       (let ((tv (make-bytevector 4)))
         (%set32 tv 0 t)
         ;; A trick from the Apache Portable Runtime: mktime is inverse of localtime,
         ;; so mktime(gmtime(now)) - mktime(localtime(now)) _should_ be offset from utc.
         (let ((gmean-tm (_gmtime tv)))
           ;; set tm->tm_isdst to 0; GMT does not have DST.
           (%poke32 (+ gmean-tm 32) 0)
           (inexact->exact (ceiling (_difftime t (_mktime gmean-tm)))))))))

 (define current-process-milliseconds
   (lambda ()
     (let ((m (memstats)))
       (+ (memstats-system-time m) (memstats-user-time m)))))
 
 (define current-gc-milliseconds
   (lambda ()
     (memstats-gc-total-cpu-time (memstats))))
 
 (define-syntax :optional
   (syntax-rules ()
     ((_ val default-value)
      (if (null? val)
          default-value
          (car val)))))
 
 (define time-thread 'time-thread)
 (define time-process 'time-process)
 (define time-duration 'time-duration)
 
 ;; example of extension (MZScheme specific)
 (define time-gc 'time-gc)

 ;;-- LOCALE dependent constants

 (define locale-number-separator ".")

 (define locale-abbr-weekday-vector
   (vector "Sun" "Mon" "Tue" "Wed"
           "Thu" "Fri" "Sat"))

 (define locale-long-weekday-vector
   (vector "Sunday" "Monday"
           "Tuesday" "Wednesday"
           "Thursday" "Friday"
           "Saturday"))

 ;; note empty string in 0th place. 
 (define locale-abbr-month-vector
   (vector ""
           "Jan" "Feb" "Mar""Apr"
           "May" "Jun" "Jul" "Aug"
           "Sep" "Oct" "Nov" "Dec"))

 (define locale-long-month-vector
   (vector ""
           "January" "February"
           "March" "April" "May"
           "June" "July" "August"
           "September" "October"
           "November" "December"))

 (define locale-pm "PM")
 (define locale-am "AM")

 ;; See date->string
 (define locale-date-time-format "~a ~b ~d ~H:~M:~S~z ~Y")
 (define locale-short-date-format "~m/~d/~y")
 (define locale-time-format "~H:~M:~S")
 (define iso-8601-date-time-format "~Y-~m-~dT~H:~M:~S~z")
 (define rfc2822 #f)
 
 (define legal-formats
   (list 'local 'julian 'iso-8601 'rfc2822))

 (define date-display-format 
   (make-parameter 'display-format 'local
                   (lambda (s)
                     (memq s legal-formats))))


 ;;-- Miscellaneous Constants.
 ;;-- only the tai-epoch-in-jd might need changing if
 ;;   a different epoch is used.

 (define nano (expt 10 9))
 (define sid  86400)                ;; seconds in a day
 (define sihd 43200)                ;; seconds in a half day
 (define tai-epoch-in-jd 4881175/2) ;; julian day number for 'the epoch'


 ;; A Very simple Error system for the time procedures

 (define time-error-types
   '(invalid-clock-type
     unsupported-clock-type
     incompatible-time-types
     not-duration
     dates-are-immutable
     bad-date-format-string
     bad-date-template-string
     invalid-month-specification))

 (define time-error
   (lambda (caller type value)
     (if (member type time-error-types)
         (if value
             (error caller ": TIME-ERROR type " type ": " value)
             (error caller ": TIME-ERROR type " type))
         (error caller ": TIME-ERROR unsupported error type " type))))

 ;; A table of leap seconds
 ;; See ftp://maia.usno.navy.mil/ser7/tai-utc.dat
 ;; and update as necessary.
 ;; this procedures reads the file in the abover
 ;; format and creates the leap second table
 ;; ie (set! leap-second-table (read-tai-utc-date "tai-utc.dat"))

 (define read-tai-utc-data
   (lambda (filename)

     (define convert-jd
       (lambda (jd)
         (* (- (inexact->exact jd) tai-epoch-in-jd) sid)))
     
     (define convert-sec
       (lambda (sec)
         (inexact->exact sec)))
     
     (let ([port (open-file-input-port filename)]
           [table '()])
       (let loop ((line (get-line port)))
         (if (not (eof-object? line))
             (begin
               (let* ( (data (get-datum (open-input-string (string-append "(" line ")")))) 
                       (year (car data))
                       (jd   (cadddr (cdr data)))
                       (secs (cadddr (cdddr data))) )
                 (if (>= year 1972)
                     (set! table (cons (cons (convert-jd jd) (convert-sec secs)) table)))
                 (loop (get-line port))))))
       table)))

 ;; each entry is ( utc seconds since epoch . # seconds to add for tai )
 ;; note they go higher to lower, and end in 1972.
 (define leap-second-table
   '((915148800 . 32)
     (867715200 . 31)
     (820454400 . 30)
     (773020800 . 29)
     (741484800 . 28)
     (709948800 . 27)
     (662688000 . 26)
     (631152000 . 25)
     (567993600 . 24)
     (489024000 . 23)
     (425865600 . 22)
     (394329600 . 21)
     (362793600 . 20)
     (315532800 . 19)
     (283996800 . 18)
     (252460800 . 17)
     (220924800 . 16)
     (189302400 . 15)
     (157766400 . 14)
     (126230400 . 13)
     (94694400  . 12)
     (78796800  . 11)
     (63072000  . 10)))

 (define read-leap-second-table
   (lambda (filename)
     (set! leap-second-table (read-tai-utc-data filename))
     (values)))

 (define leap-second-delta
   (lambda (utc-seconds)
     (letrec ( (lsd (lambda (table) 
                      (cond
                       ((>= utc-seconds (caar table))
                        (cdar table))
                       (else (lsd (cdr table)))))) )
       (if (< utc-seconds  (* (- 1972 1970) 365 sid)) 0
           (lsd leap-second-table)))))

 ;; going from tai seconds to utc seconds ... 
 (define leap-second-neg-delta
   (lambda (tai-seconds)
     (letrec ((lsd (lambda (table)
                     (cond ((null? table) 0)
                           ((<= (cdar table) (- tai-seconds (caar table)))
                            (cdar table))
                           (else (lsd (cdr table)))))) )
       (if (< tai-seconds  (* (- 1972 1970) 365 sid)) 0
           (lsd leap-second-table)))))
 
 (define current-time-thread
   (lambda ()
     (current-time-ms-time time-process current-process-milliseconds)))
 
 (define current-time-process
   (lambda ()
     (current-time-ms-time time-process current-process-milliseconds)))
 
 (define current-time-gc
   (lambda ()
     (current-time-ms-time time-gc current-gc-milliseconds))) 
 
 ;; define it to be the same as tai.
 ;; a different implemation of current-time-montonic
 ;; will require rewriting all of the time-monotonic converters,
 ;;  of course.
 (define current-time-monotonic
   (lambda ()
     (let-values (((seconds ms) (get-time-of-day)))
       (make-time 'MONOTONIC
                  (* ms 10000)
                  (+ seconds (leap-second-delta seconds))))))

 (define current-time-utc
   (lambda ()
     (let-values (((seconds ms) (get-time-of-day)))
       (make-time  'UTC (* ms 10000) seconds ))))

 (define get-time-of-day
   (lambda ()
     (let-values (((secs usecs) (current-utc-time)))
       (values secs (quotient usecs 1000)))))
 
 (define current-time-tai
   (lambda ()
     (let-values (((seconds ms) (get-time-of-day)))
       (make-time 'TAI
                  (* ms 10000)
                  (+ seconds (leap-second-delta seconds))))))

 (define current-time-ms-time
   (lambda (time-type proc)
     (let ((current-ms (proc)))
       (make-time time-type 
                  (* (remainder current-ms 1000) 10000)
                  (quotient current-ms 10000)))))
 
 (define current-time
   (lambda clock-type
     (let ((clock-type (:optional clock-type 'UTC)))
       (cond
        ((eq? clock-type 'TAI) (current-time-tai))
        ((eq? clock-type 'UTC) (current-time-utc))
        ((eq? clock-type 'MONOTONIC) (current-time-monotonic))
        ((eq? clock-type time-thread) (current-time-thread))
        ((eq? clock-type time-process) (current-time-process))
        ((eq? clock-type time-gc) (current-time-gc))
        (else (time-error 'current-time 'invalid-clock-type clock-type))))))
 
 (define local-tz-offset
   (lambda ()
     (let-values (((secs usecs) (current-utc-time)))
       (timezone-offset secs))))

 ;; -- time resolution
 ;; this is the resolution of the clock in nanoseconds.
 ;; this will be implementation specific.
 
 (define time-resolution
   (lambda (clock-type)
     (let ((clock-type (:optional clock-type 'UTC)))
       (cond
        ((eq? clock-type 'TAI) 10000)
        ((eq? clock-type 'UTC) 10000)
        ((eq? clock-type 'MONOTONIC) 10000)
        ((eq? clock-type time-thread) 10000)
        ((eq? clock-type time-process) 10000)
        ((eq? clock-type time-gc) 10000)
        (else (time-error 'time-resolution 'invalid-clock-type clock-type))))))

;;;
;;; Time Comparisons
;;;
 
 (define time-compare-check
   (lambda (time1 time2 caller)
     (if (or (not (and (time? time1) (time? time2)))
             (not (eq? (time-type time1) (time-type time2))))
         (time-error caller 'incompatible-time-types #f)
         #t)))
 
 (define time=?
   (lambda (time1 time2)
     (time-compare-check time1 time2 'time=?)
     (and (= (time-second time1) (time-second time2))
          (= (time-nanosecond time1) (time-nanosecond time2)))))

 (define time>?
   (lambda (time1 time2)
     (time-compare-check time1 time2 'time>?)
     (or (> (time-second time1) (time-second time2))
         (and (= (time-second time1) (time-second time2))
              (> (time-nanosecond time1) (time-nanosecond time2))))))
 
 (define time<?
   (lambda (time1 time2)
     (time-compare-check time1 time2 'time<?)
     (or (< (time-second time1) (time-second time2))
         (and (= (time-second time1) (time-second time2))
              (< (time-nanosecond time1) (time-nanosecond time2))))))
 
 (define time>=?
   (lambda (time1 time2)
     (time-compare-check time1 time2 'time>=?)
     (or (>= (time-second time1) (time-second time2))
         (and (= (time-second time1) (time-second time2))
              (>= (time-nanosecond time1) (time-nanosecond time2))))))
 
 (define time<=?
   (lambda (time1 time2)
     (time-compare-check time1 time2 'time<=?)
     (or (<= (time-second time1) (time-second time2))
         (and (= (time-second time1) (time-second time2))
              (<= (time-nanosecond time1) (time-nanosecond time2))))))

;;; 
;;; Time Arithmetic
;;;
 
 (define time->nanoseconds
   (lambda (time)     
     (define (sign1 n)
       (if (negative? n) -1 1))     
     (+ (* (time-second time) nano)
        (time-nanosecond time))))
 
 (define nanoseconds->time
   (lambda (time-type nanoseconds)
     (make-time time-type
                (remainder nanoseconds nano)
                (quotient nanoseconds nano))))

 (define nanoseconds->values
   (lambda (nanoseconds)
     (values (abs (remainder nanoseconds nano))
             (quotient nanoseconds nano))))
 
 (define time-difference
   (lambda (time1 time2)
     (if (or (not (and (time? time1)
                       (time? time2)))
             (not (eq? (time-type time1)
                       (time-type time2))))
         (time-error 'time-difference 'incompatible-time-types #f))
     (if (time=? time1 time2)
         (make-time time-duration 0 0)
         (let-values (((nanos secs)
                       (nanoseconds->values (- (time->nanoseconds time1)
                                               (time->nanoseconds time2)))))
           (make-time time-duration nanos secs)))))

 (define time-difference!
   (lambda (time1 time2)
     (time-difference time1 time2 time1)))

 (define add-duration
   (lambda (time duration)
     (when (not (and (time? time)
                     (time? duration)))
       (time-error 'add-duration 'incompatible-time-types #f))
     (when (not (eq? (time-type duration) time-duration))
       (time-error 'add-duration 'not-duration duration))
     (let ((sec-plus (+ (time-second time) (time-second duration)))
           (nsec-plus (+ (time-nanosecond time) (time-nanosecond duration))))
       (let ((r (remainder nsec-plus nano))
             (q (quotient nsec-plus nano)))
         (if (negative? r)
             (make-time (time-duration time) (+ nano r)(+ sec-plus q -1))
             (make-time (time-duration time) r (+ sec-plus q)))))))
 
 (define add-duration!
   (lambda (time1 duration)
     (add-duration time1 duration time1)))

 (define subtract-duration
   (lambda (time duration)
     (when (not (and (time? time)
                     (time? duration)))
       (time-error 'add-duration 'incompatible-time-types #f))
     (when (not (eq? (time-type duration)
                     time-duration))
       (time-error 'subtract-duration 'not-duration duration))
     (let ((sec-minus  (- (time-second time)
                          (time-second duration)))
           (nsec-minus (- (time-nanosecond time)
                          (time-nanosecond duration))))
       (let ((r (remainder nsec-minus nano))
             (q (quotient nsec-minus nano)))
         (if (negative? r)
             (make-time (time-type time) (+ nano r)(- sec-minus q 1))                 
             (make-time (time-type time) r (- sec-minus q)))))))

;;;
;;;  Converters Between Date And Time Types.
;;;

 (define time-monotonic?
   (lambda (time)
     (eq? (time-type time) 'MONOTONIC)))
 
 (define time-utc?
   (lambda (time)
     (eq? (time-type time) 'UTC)))
 
 (define time-tai?
   (lambda (time)
     (eq? (time-type time) 'TAI)))
 
 (define time-tai->time-utc
   (lambda (time)
     (assert (time-tai? time))
     (make-time 'UTC
                (time-nanosecond time)
                (let ((secs (time-second time)))
                  (- secs (leap-second-neg-delta secs))))))
 
 (define time-utc->time-tai
   (lambda (time time-out)
     (assert (time-utc? time))
     (make-time 'TAI
                (time-nanosecond time)
                (let ((secs (time-second time)))
                  (+ secs (leap-second-delta secs))))))
 
                                        ; Assumes time-monotonic has the same definition as time-tai
 (define time-monotonic->time-utc
   (lambda (time)
     (assert (time-monotonic? time))
     (time-tai->time-utc (make-time 'TAI (time-nanosecond time)(time-second time)))))
 
 (define time-monotonic->time-tai
   (lambda (time)
     (assert (time-monotonic? time))
     (make-time 'TAI (time-nanosecond time)(time-second time))))
 
 (define time-utc->time-monotonic
   (lambda (time)    
     (assert (time-utc? time))
     (let ((tai (time-utc->time-tai time)))
       (make-time 'MONOTONIC (time-nanosecond tai)(time-second tai)))))

 (define time-tai->time-monotonic
   (lambda (time)
     (assert (time-tai? time))
     (make-time 'MONOTONIC (time-nanosecond time)(time-second time))))

 ;; gives the julian day which starts at noon.
 (define encode-julian-day-number
   (lambda (day month year)
     (let* ((a (quotient (- 14 month) 12))
            (y (- (- (+ year 4800) a) (if (negative? year) -1 0)))
            (m (- (+ month (* 12 a)) 3)))
       (+ day
          (quotient (+ (* 153 m) 2) 5)
          (* 365 y)
          (quotient y 4)
          (- (quotient y 100))
          (quotient y 400)
          -32045))))

 (define char-pos
   (lambda (char str index len)
     (cond
      ((>= index len) #f)
      ((char=? (string-ref str index) char)
       index)
      (else
       (char-pos char str (+ index 1) len)))))

 (define fractional-part
   (lambda (r)
     (if (integer? r) "0"
         (let ((str (number->string (exact->inexact r))))
           (let ((ppos (char-pos #\. str 0 (string-length str))))
             (substring str  (+ ppos 1) (string-length str)))))))

 ;; gives the seconds/date/month/year 
 (define decode-julian-day-number
   (lambda (jdn)
     (let* ((days (truncate jdn))
            (a (+ days 32044))
            (b (quotient (+ (* 4 a) 3) 146097))
            (c (- a (quotient (* 146097 b) 4)))
            (d (quotient (+ (* 4 c) 3) 1461))
            (e (- c (quotient (* 1461 d) 4)))
            (m (quotient (+ (* 5 e) 2) 153))
            (y (+ (* 100 b) d -4800 (quotient m 10))))
       (values ; seconds date month year
        (* (- jdn days) sid)
        (+ e (- (quotient (+ (* 153 m) 2) 5)) 1)
        (+ m 3 (* -12 (quotient m 10)))
        (if (>= 0 y) (- y 1) y)))))
 
 ;; special thing -- ignores nanos
 (define time->julian-day-number
   (lambda (seconds tz-offset)
     (+ (/ (+ seconds tz-offset sihd) sid)
        tai-epoch-in-jd)))
 
 (define tai-before-leap-second?
   (lambda (second)
     (find (lambda (x)
             (= second (- (+ (car x) (cdr x)) 1)))
           leap-second-table)))
 
 (define time->date
   (lambda (time tz-offset ttype)
     (if (not (eq? (time-type time) ttype))
         (time-error 'time->date 'incompatible-time-types  time))
     (let* ( (offset (:optional tz-offset (local-tz-offset))) )
       (let-values (((secs date month year)
                     (decode-julian-day-number (time->julian-day-number (time-second time) offset))))
         (let* ((hours    (quotient secs (* 60 60)))
                (rem      (remainder secs (* 60 60)))
                (minutes  (quotient rem 60))
                (seconds  (remainder rem 60)) )
           (make-date (time-nanosecond time)
                      seconds
                      minutes
                      hours
                      date
                      month
                      year
                      offset))))))
 
 (define time-tai->date
   (lambda (time . tz-offset)
     (if (tai-before-leap-second? (time-second time))
         ;; if it's *right* before the leap, we need to pretend to subtract a second ...
         (let ((d (time->date (subtract-duration (time-tai->time-utc time) (make-time time-duration 0 1)) tz-offset 'UTC)))
           (make-date (date-nanosecond d)
                      60
                      (date-minute d)
                      (date-hour d)
                      (date-day d)
                      (date-month d)
                      (date-year d)
                      (date-zone-offset d)))
         (time->date (time-tai->time-utc time) tz-offset 'UTC))))
 
 (define time-utc->date
   (lambda (time . tz-offset)
     (time->date time tz-offset 'UTC)))
 
 ;; again, time-monotonic is the same as time tai
 (define time-monotonic->date
   (lambda (time . tz-offset)
     (time->date time tz-offset 'MONOTONIC)))

 (define date->time-utc
   (lambda (date)
     (let ( (nanosecond (date-nanosecond date))
            (second     (date-second date))
            (minute     (date-minute date))
            (hour        (date-hour date))
            (day         (date-day date))
            (month       (date-month date))
            (year        (date-year date))
            (offset      (date-zone-offset date)) )
       (let ((jdays (- (encode-julian-day-number day month year)
                       tai-epoch-in-jd)))
         (make-time 
          'UTC
          nanosecond
          (+ (* (- jdays 1/2) 24 60 60)
             (* hour 60 60)
             (* minute 60)
             second
             (- offset)))))))

 (define date->time-tai
   (lambda (d)
     (if (= (date-second d) 60)
         (subtract-duration (time-utc->time-tai (date->time-utc d))
                            (make-time 'DURATION 0 1))
         (time-utc->time-tai (date->time-utc d)))))
 
 (define date->time-monotonic
   (lambda (date)
     (time-utc->time-monotonic (date->time-utc date))))
 
 (define leap-year?
   (lambda (year)
     (or (= (modulo year 400) 0)
         (and (= (modulo year 4) 0) (not (= (modulo year 100) 0))))))
 
 (define date-leap-year?
   (lambda (date)
     (leap-year? (date-year date))))

 ;; year-day fixed: adding wrong number of days.
 (define  month-assoc '((0 . 0) (1 . 31)  (2 . 59)   (3 . 90)   (4 . 120) 
                        (5 . 151) (6 . 181)  (7 . 212)  (8 . 243)
                        (9 . 273) (10 . 304) (11 . 334)))
 
 (define year-day
   (lambda (day month year)
     (let ((days-pr (assoc (- month 1) month-assoc)))
       (if (not days-pr)
           (time-error 'date-year-day 'invalid-month-specification month))
       (if (and (leap-year? year) (> month 2))
           (+ day (cdr days-pr) 1)
           (+ day (cdr days-pr))))))
 
 (define date-year-day
   (lambda (date)
     (year-day (date-day date) (date-month date) (date-year date))))
 
 ;; from calendar faq 
 (define week-day
   (lambda (day month year)
     (let* ((a (quotient (- 14 month) 12))
            (y (- year a))
            (m (+ month (* 12 a) -2)))
       (modulo (+ day y (quotient y 4) (- (quotient y 100))
                  (quotient y 400) (quotient (* 31 m) 12))
               7))))

 (define date-week-day
   (lambda (date)
     (week-day (date-day date) (date-month date) (date-year date))))

 (define days-before-first-week
   (lambda (date day-of-week-starting-week)
     (let* ( (first-day (make-date 0 0 0 0 1 1
                                   (date-year date)
                                   #f))
             (fdweek-day (date-week-day first-day))  )
       (modulo (- day-of-week-starting-week fdweek-day) 7))))
 
 (define date-week-number
   (lambda (date day-of-week-starting-week)
     (quotient (- (date-year-day date)
                  (days-before-first-week  date day-of-week-starting-week))
               7)))

 (define current-date
   (lambda tz-offset 
     (time-utc->date (current-time 'UTC)
                     (:optional tz-offset (local-tz-offset)))))

 ;; given a 'two digit' number, find the year within 50 years +/-
 (define natural-year
   (lambda (n)
     (let* ( (current-year (date-year (current-date)))
             (current-century (* (quotient current-year 100) 100)) )
       (cond
        ((>= n 100) n)
        ((<  n 0) n)
        ((<=  (- (+ current-century n) current-year) 50)
         (+ current-century n))
        (else
         (+ (- current-century 100) n))))))

 (define date->julian-day
   (lambda (date)
     (let ( (nanosecond (date-nanosecond date))
            (second (date-second date))
            (minute (date-minute date))
            (hour (date-hour date))
            (day (date-day date))
            (month (date-month date))
            (year (date-year date))
            (offset (date-zone-offset date)) )
       (+ (encode-julian-day-number day month year)
          (- 1/2)
          (+ (/ (/ (+ (* hour 60 60)
                      (* minute 60) second (/ nanosecond nano)) sid)
                (- offset)))))))

 (define date->modified-julian-day
   (lambda (date)
     (- (date->julian-day date)
        4800001/2)))
 
 (define time-utc->julian-day
   (lambda (time)
     (if (not (eq? (time-type time) 'UTC))
         (time-error 'time-utc->julian-day 'incompatible-time-types  time))
     (+ (/ (+ (time-second time) (/ (time-nanosecond time) nano))
           sid)
        tai-epoch-in-jd)))

 (define time-utc->modified-julian-day
   (lambda (time)
     (- (time-utc->julian-day time)
        4800001/2)))
 
 (define time-tai->julian-day
   (lambda (time)
     (if (not (eq? (time-type time) 'TAI))
         (time-error 'time-tai->julian-day 'incompatible-time-types  time))
     (+ (/ (+ (- (time-second time) 
                 (leap-second-delta (time-second time)))
              (/ (time-nanosecond time) nano))
           sid)
        tai-epoch-in-jd)))

 (define time-tai->modified-julian-day
   (lambda (time)
     (- (time-tai->julian-day time)
        4800001/2)))

 ;; this is the same as time-tai->julian-day
 (define time-monotonic->julian-day
   (lambda (time)
     (if (not (eq? (time-type time) 'MONOTONIC))
         (time-error 'time-monotonic->julian-day 'incompatible-time-types  time))
     (+ (/ (+ (- (time-second time) 
                 (leap-second-delta (time-second time)))
              (/ (time-nanosecond time) nano))
           sid)
        tai-epoch-in-jd)))

 (define time-monotonic->modified-julian-day
   (lambda (time)
     (- (time-monotonic->julian-day time)
        4800001/2)))
 
 (define julian-day->time-utc
   (lambda (jdn)
     (let ( (nanosecs (* nano sid (- jdn tai-epoch-in-jd))) )
       (make-time 'UTC
                  (remainder nanosecs nano)
                  (floor (/ nanosecs nano))))))

 (define julian-day->time-tai
   (lambda (jdn)
     (time-utc->time-tai (julian-day->time-utc jdn))))

 (define julian-day->time-monotonic
   (lambda (jdn)
     (time-utc->time-monotonic (julian-day->time-utc jdn))))

 (define julian-day->date
   (lambda (jdn . tz-offset)
     (let ((offset (:optional tz-offset (local-tz-offset))))
       (time-utc->date (julian-day->time-utc jdn) offset))))

 (define modified-julian-day->date
   (lambda (jdn . tz-offset)
     (let ((offset (:optional tz-offset (local-tz-offset))))
       (julian-day->date (+ jdn 4800001/2) offset))))

 (define modified-julian-day->time-utc
   (lambda (jdn)
     (julian-day->time-utc (+ jdn 4800001/2))))

 (define modified-julian-day->time-tai
   (lambda (jdn)
     (julian-day->time-tai (+ jdn 4800001/2))))

 (define modified-julian-day->time-monotonic
   (lambda (jdn)
     (julian-day->time-monotonic (+ jdn 4800001/2))))

 (define current-julian-day
   (lambda ()
     (time-utc->julian-day (current-time 'UTC))))

 (define current-modified-julian-day
   (lambda ()
     (time-utc->modified-julian-day (current-time 'UTC))))

 ;; returns a string rep. of number N, of minimum LENGTH,
 ;; padded with character PAD-WITH. If PAD-WITH if #f, 
 ;; no padding is done, and it's as if number->string was used.
 ;; if string is longer than LENGTH, it's as if number->string was used.

 (define padding
   (lambda (n pad-with length)
     (let* ( (str (number->string n))
             (str-len (string-length str)) )
       (if (or (> str-len length)
               (not pad-with))
           str
           (let* ( (new-str (make-string length pad-with))
                   (new-str-offset (- (string-length new-str)
                                      str-len)) )
             (do ((i 0 (+ i 1)))
                 ((>= i (string-length str)))
               (string-set! new-str (+ new-str-offset i) 
                            (string-ref str i)))
             new-str)))))

 (define last-n-digits
   (lambda (i n)
     (abs (remainder i (expt 10 n)))))

 (define locale-abbr-weekday
   (lambda (n) 
     (vector-ref locale-abbr-weekday-vector n)))

 (define locale-long-weekday
   (lambda (n)
     (vector-ref locale-long-weekday-vector n)))

 (define locale-abbr-month
   (lambda (n)
     (vector-ref locale-abbr-month-vector n)))

 (define locale-long-month
   (lambda (n)
     (vector-ref locale-long-month-vector n)))

 (define vector-find
   (lambda (needle haystack comparator)
     (let ((len (vector-length haystack)))
       (define (vector-find-int index)
         (cond
          ((>= index len) #f)
          ((comparator needle (vector-ref haystack index)) index)
          (else (vector-find-int (+ index 1)))))
       (vector-find-int 0))))

 (define locale-abbr-weekday->index
   (lambda (string)
     (vector-find string locale-abbr-weekday-vector string=?)))

 (define locale-long-weekday->index
   (lambda (string)
     (vector-find string locale-long-weekday-vector string=?)))

 (define locale-abbr-month->index
   (lambda (string)
     (vector-find string locale-abbr-month-vector string=?)))

 (define locale-long-month->index
   (lambda (string)
     (vector-find string locale-long-month-vector string=?)))
 
 ;; do nothing. 
 ;; Your implementation might want to do something...
 (define locale-print-time-zone
   (lambda (date port)
     (values)))
 
 ;; Again, locale specific.
 (define locale-am/pm
   (lambda (hr)
     (if (> hr 11) locale-pm locale-am)))
 
 (define tz-printer
   (lambda (offset port)
     (cond
      ((= offset 0) (display "Z" port))
      ((negative? offset) (display "-" port))
      (else (display "+" port)))
     (if (not (= offset 0))
         (let ( (hours   (abs (quotient offset (* 60 60))))
                (minutes (abs (quotient (remainder offset (* 60 60)) 60))) )
           (display (padding hours #\0 2) port)
           (display (padding minutes #\0 2) port)))))

 (define get-formatter
   (lambda (char)
     (let ((associated (assoc char directives)))
       (if associated (cdr associated) #f))))
 
 (define date->string
   (lambda (date .  format-string)
     (let ((str-port (open-output-string))
           (fmt-str (:optional format-string "~c")) )
       (date-printer date 0 fmt-str (string-length fmt-str) str-port)
       (get-output-string str-port))))

 (define date-printer
   (lambda (date index format-string str-len port)
     (if (>= index str-len)
         (values)
         (let ( (current-char (string-ref format-string index)) )
           (if (not (char=? current-char #\~))
               (begin
                 (display current-char port)
                 (date-printer date (+ index 1) format-string str-len port))

               (if (= (+ index 1) str-len) ; bad format string.
                   (time-error 'date-printer 'bad-date-format-string 
                               format-string)
                   (let ( (pad-char? (string-ref format-string (+ index 1))) )
                     (cond
                      ((char=? pad-char? #\-)
                       (if (= (+ index 2) str-len) ; bad format string.
                           (time-error 'date-printer 'bad-date-format-string 
                                       format-string)
                           (let ( (formatter (get-formatter 
                                              (string-ref format-string
                                                          (+ index 2)))) )
                             (if (not formatter)
                                 (time-error 'date-printer 'bad-date-format-string 
                                             format-string)
                                 (begin
                                   (formatter date #f port)
                                   (date-printer date (+ index 3)
                                                 format-string str-len port))))))
                      
                      ((char=? pad-char? #\_)
                       (if (= (+ index 2) str-len) ; bad format string.
                           (time-error 'date-printer 'bad-date-format-string 
                                       format-string)
                           (let ( (formatter (get-formatter 
                                              (string-ref format-string
                                                          (+ index 2)))) )
                             (if (not formatter)
                                 (time-error 'date-printer 'bad-date-format-string 
                                             format-string)
                                 (begin
                                   (formatter date #\space port)
                                   (date-printer date (+ index 3)
                                                 format-string str-len port))))))
                      (else
                       (let ( (formatter (get-formatter 
                                          (string-ref format-string
                                                      (+ index 1)))) )
                         (if (not formatter)
                             (time-error 'date-printer 'bad-date-format-string 
                                         format-string)
                             (begin
                               (formatter date #\0 port)
                               (date-printer date (+ index 2)
                                             format-string str-len port)))))))))))))
 
 ;; A table of output formatting directives.
 ;; the first time is the format char.
 ;; the second is a procedure that takes the date, a padding character
 ;; (which might be #f), and the output port.

 (define directives 
   (list
    (cons #\~ (lambda (date pad-with port) (display #\~ port)))
    
    (cons #\a (lambda (date pad-with port)
                (display (locale-abbr-weekday (date-week-day date))
                         port)))
    (cons #\A (lambda (date pad-with port)
                (display (locale-long-weekday (date-week-day date))
                         port)))
    (cons #\b (lambda (date pad-with port)
                (display (locale-abbr-month (date-month date))
                         port)))
    (cons #\B (lambda (date pad-with port)
                (display (locale-long-month (date-month date))
                         port)))
    (cons #\c (lambda (date pad-with port)
                (display (date->string date locale-date-time-format) port)))
    (cons #\d (lambda (date pad-with port)
                (display (padding (date-day date)
                                  #\0 2)
                         port)))
    (cons #\D (lambda (date pad-with port)
                (display (date->string date "~m/~d/~y") port)))
    (cons #\e (lambda (date pad-with port)
                (display (padding (date-day date)
                                  #\space 2)
                         port)))
    (cons #\f (lambda (date pad-with port)
                (if (> (date-nanosecond date)
                       nano)
                    (display (padding (+ (date-second date) 1)
                                      pad-with 2)
                             port)
                    (display (padding (date-second date)
                                      pad-with 2)
                             port))
                (let* ((ns (fractional-part (/ 
                                             (date-nanosecond date)
                                             nano 1.0)))
                       (le (string-length ns)))
                  (if (> le 2)
                      (begin
                        (display locale-number-separator port)
                        (display (substring ns 2 le) port))))))
    (cons #\h (lambda (date pad-with port)
                (display (date->string date "~b") port)))
    (cons #\H (lambda (date pad-with port)
                (display (padding (date-hour date)
                                  pad-with 2)
                         port)))
    (cons #\I (lambda (date pad-with port)
                (let ((hr (date-hour date)))
                  (if (> hr 12)
                      (display (padding (- hr 12)
                                        pad-with 2)
                               port)
                      (display (padding hr
                                        pad-with 2)
                               port)))))
    (cons #\j (lambda (date pad-with port)
                (display (padding (date-year-day date)
                                  pad-with 3)
                         port)))
    (cons #\k (lambda (date pad-with port)
                (display (padding (date-hour date)
                                  #\0 2)
                         port)))
    (cons #\l (lambda (date pad-with port)
                (let ((hr (if (> (date-hour date) 12)
                              (- (date-hour date) 12) (date-hour date))))
                  (display (padding hr  #\space 2)
                           port))))
    (cons #\m (lambda (date pad-with port)
                (display (padding (date-month date)
                                  pad-with 2)
                         port)))
    (cons #\M (lambda (date pad-with port)
                (display (padding (date-minute date)
                                  pad-with 2)
                         port)))
    (cons #\n (lambda (date pad-with port)
                (newline port)))
    (cons #\N (lambda (date pad-with port)
                (display (padding (date-nanosecond date)
                                  pad-with 7)
                         port)))
    (cons #\p (lambda (date pad-with port)
                (display (locale-am/pm (date-hour date)) port)))
    (cons #\r (lambda (date pad-with port)
                (display (date->string date "~I:~M:~S ~p") port)))
    (cons #\s (lambda (date pad-with port)
                (display (time-second (date->time-utc date)) port)))
    (cons #\S (lambda (date pad-with port)
                (if (> (date-nanosecond date)
                       nano)
                    (display (padding (+ (date-second date) 1)
                                      pad-with 2)
                             port)
                    (display (padding (date-second date)
                                      pad-with 2)
                             port))))
    (cons #\t (lambda (date pad-with port)
                (display (integer->char 9) port)))
    (cons #\T (lambda (date pad-with port)
                (display (date->string date "~H:~M:~S") port)))
    (cons #\U (lambda (date pad-with port)
                (if (> (days-before-first-week date 0) 0)
                    (display (padding (+ (date-week-number date 0) 1)
                                      #\0 2) port)
                    (display (padding (date-week-number date 0)
                                      #\0 2) port))))
    (cons #\V (lambda (date pad-with port)
                (display (padding (date-week-number date 1)
                                  #\0 2) port)))
    (cons #\w (lambda (date pad-with port)
                (display (date-week-day date) port)))
    (cons #\x (lambda (date pad-with port)
                (display (date->string date locale-short-date-format) port)))
    (cons #\X (lambda (date pad-with port)
                (display (date->string date locale-time-format) port)))
    (cons #\W (lambda (date pad-with port)
                (if (> (days-before-first-week date 1) 0)
                    (display (padding (+ (date-week-number date 1) 1)
                                      #\0 2) port)
                    (display (padding (date-week-number date 1)
                                      #\0 2) port))))
    (cons #\y (lambda (date pad-with port)
                (display (padding (last-n-digits 
                                   (date-year date) 2)
                                  pad-with
                                  2)
                         port)))
    (cons #\Y (lambda (date pad-with port)
                (display (date-year date) port)))
    (cons #\z (lambda (date pad-with port)
                (tz-printer (date-zone-offset date) port)))
    (cons #\Z (lambda (date pad-with port)
                (locale-print-time-zone date port)))
    (cons #\1 (lambda (date pad-with port)
                (display (date->string date "~Y-~m-~d") port)))
    (cons #\2 (lambda (date pad-with port)
                (display (date->string date "~k:~M:~S~z") port)))
    (cons #\3 (lambda (date pad-with port)
                (display (date->string date "~k:~M:~S") port)))
    (cons #\4 (lambda (date pad-with port)
                (display (date->string date "~Y-~m-~dT~k:~M:~S~z") port)))
    (cons #\5 (lambda (date pad-with port)
                (display (date->string date "~Y-~m-~dT~k:~M:~S") port)))))

 (define current-time-rfc2822
   (lambda ()
     (date->string (time->date (current-time) '() 'UTC) "~a, ~d ~b ~Y ~T ~z")))

 (define char->int
   (lambda (ch)
     (cond
      ((char=? ch #\0) 0)
      ((char=? ch #\1) 1)
      ((char=? ch #\2) 2)
      ((char=? ch #\3) 3)
      ((char=? ch #\4) 4)
      ((char=? ch #\5) 5)
      ((char=? ch #\6) 6)
      ((char=? ch #\7) 7)
      ((char=? ch #\8) 8)
      ((char=? ch #\9) 9)
      (else (time-error 'string->date 'bad-date-template-string
                        (list "Non-integer character" ch ))))))

 ;; read an integer upto n characters long on port; upto -> #f if any length
 (define integer-reader
   (lambda (upto port)
     (define (accum-int port accum nchars)
       (let ((ch (peek-char port)))
         (if (or (eof-object? ch)
                 (not (char-numeric? ch))
                 (and upto (>= nchars  upto )))
             accum (accum-int port (+ (* accum 10)
                                      (char->int (get-char
                                                  port)))
                              (+ nchars 1)))))
     (accum-int port 0 0)))

 (define make-integer-reader
   (lambda (upto)
     (lambda (port)
       (integer-reader upto port))))

 ;; read *exactly* n characters and convert to integer; could be padded
 (define integer-reader-exact
   (lambda (n port)
     (let ( (padding-ok #t) )
       (define (accum-int port accum nchars)
         (let ((ch (peek-char port)))
           (cond
            ((>= nchars n) accum)
            ((eof-object? ch) 
             (time-error 'string->date 'bad-date-template-string 
                         "Premature ending to integer read."))
            ((char-numeric? ch)
             (set! padding-ok #f)
             (accum-int port (+ (* accum 10)
                                (char->int (get-char port)))
                        (+ nchars 1)))
            (padding-ok
             (get-char port) ; consume padding
             (accum-int port accum (+ nchars 1)))
            (else ; padding where it shouldn't be
             (time-error 'string->date 'bad-date-template-string 
                         "Non-numeric characters in integer read.")))))
       (accum-int port 0 0))))

 (define make-integer-exact-reader
   (lambda (n)
     (lambda (port)
       (integer-reader-exact n port))))

 (define zone-reader
   (lambda (port) 
     (let ( (offset 0) 
            (positive? #f) )
       (let ( (ch (get-char port)) )
         (if (eof-object? ch)
             (time-error 'string->date 'bad-date-template-string
                         (list "Invalid time zone +/-" ch)))
         (if (or (char=? ch #\Z) (char=? ch #\z))
             0
             (begin
               (cond
                ((char=? ch #\+) (set! positive? #t))
                ((char=? ch #\-) (set! positive? #f))
                (else
                 (time-error 'string->date 'bad-date-template-string
                             (list "Invalid time zone +/-" ch))))
               (let ((ch (get-char port)))
                 (if (eof-object? ch)
                     (time-error 'string->date 'bad-date-template-string
                                 (list "Invalid time zone number" ch)))
                 (set! offset (* (char->int ch)
                                 10 60 60)))
               (let ((ch (get-char port)))
                 (if (eof-object? ch)
                     (time-error 'string->date 'bad-date-template-string
                                 (list "Invalid time zone number" ch)))
                 (set! offset (+ offset (* (char->int ch)
                                           60 60))))
               (let ((ch (get-char port)))
                 (if (eof-object? ch)
                     (time-error 'string->date 'bad-date-template-string
                                 (list "Invalid time zone number" ch)))
                 (set! offset (+ offset (* (char->int ch)
                                           10 60))))
               (let ((ch (get-char port)))
                 (if (eof-object? ch)
                     (time-error 'string->date 'bad-date-template-string
                                 (list "Invalid time zone number" ch)))
                 (set! offset (+ offset (* (char->int ch)
                                           60))))
               (if positive? offset (- offset))))))))

 ;; looking at a char, read the char string, run thru indexer, return index
 (define locale-reader
   (lambda (port indexer)
     (let ( (string-port (open-output-string)) )
       (define (read-char-string)
         (let ((ch (peek-char port)))
           (if (char-alphabetic? ch)
               (begin (put-char (get-char port) string-port) 
                      (read-char-string))
               (get-output-string string-port))))
       (let* ( (str (read-char-string)) 
               (index (indexer str)) )
         (if index index (time-error 'string->date
                                     'bad-date-template-string
                                     (list "Invalid string for " indexer)))))))

 (define make-locale-reader
   (lambda (indexer)
     (lambda (port)
       (locale-reader port indexer))))

 (define make-char-id-reader
   (lambda (char)
     (lambda (port)
       (if (char=? char (get-char port))
           char
           (time-error 'string->date
                       'bad-date-template-string
                       "Invalid character match.")))))

 ;; A List of formatted read directives.
 ;; Each entry is a list.
 ;; 1. the character directive; 
 ;; a procedure, which takes a character as input & returns
 ;; 2. #t as soon as a character on the input port is acceptable
 ;; for input,
 ;; 3. a port reader procedure that knows how to read the current port
 ;; for a value. Its one parameter is the port.
 ;; 4. a action procedure, that takes the value (from 3.) and some
 ;; object (here, always the date) and (probably) side-effects it.
 ;; In some cases (e.g., ~A) the action is to do nothing

 (define read-directives 
   (let ( (ireader4 (make-integer-reader 4))
          (ireader2 (make-integer-reader 2))
          (ireaderf (make-integer-reader #f))
          (eireader2 (make-integer-exact-reader 2))
          (eireader4 (make-integer-exact-reader 4))
          (locale-reader-abbr-weekday (make-locale-reader
                                       locale-abbr-weekday->index))
          (locale-reader-long-weekday (make-locale-reader
                                       locale-long-weekday->index))
          (locale-reader-abbr-month   (make-locale-reader
                                       locale-abbr-month->index))
          (locale-reader-long-month   (make-locale-reader
                                       locale-long-month->index))
          (char-fail (lambda (ch) #t))
          (do-nothing (lambda (val object) (values)))
          )
     
     (list
      (list #\~ char-fail (make-char-id-reader #\~) do-nothing)
      (list #\a char-alphabetic? locale-reader-abbr-weekday do-nothing)
      (list #\A char-alphabetic? locale-reader-long-weekday do-nothing)
      (list #\b char-alphabetic? locale-reader-abbr-month
            (lambda (val object)
              (date-month-set! object val)))
      (list #\B char-alphabetic? locale-reader-long-month
            (lambda (val object)
              (date-month-set! object val)))
      (list #\d char-numeric? ireader2 (lambda (val object)
                                         (date-day-set!
                                          object val)))
      (list #\e char-fail eireader2 (lambda (val object)
                                      (date-day-set! object val)))
      (list #\h char-alphabetic? locale-reader-abbr-month
            (lambda (val object)
              (date-month-set! object val)))
      (list #\H char-numeric? ireader2 (lambda (val object)
                                         (date-hour-set! object val)))
      (list #\k char-fail eireader2 (lambda (val object)
                                      (date-hour-set! object val)))
      (list #\m char-numeric? ireader2 (lambda (val object)
                                         (date-month-set! object val)))
      (list #\M char-numeric? ireader2 (lambda (val object)
                                         (date-minute-set!
                                          object val)))
      (list #\S char-numeric? ireader2 (lambda (val object)
                                         (date-second-set! object val)))
      (list #\y char-fail eireader2 
            (lambda (val object)
              (date-year-set! object (natural-year val))))
      (list #\Y char-numeric? ireader4 (lambda (val object)
                                         (date-year-set! object val)))
      (list #\z (lambda (c)
                  (or (char=? c #\Z)
                      (char=? c #\z)
                      (char=? c #\+)
                      (char=? c #\-)))
            zone-reader (lambda (val object)
                          (date-zone-offset-set! object val))))))

 (define string->date-from-port
   (lambda (date index format-string str-len port template-string)
     (define (skip-until port skipper)
       (let ((ch (peek-char port)))
         (if (eof-object? ch)
             (time-error 'string->date 'bad-date-format-string template-string)
             (if (not (skipper ch))
                 (begin (get-char port) (skip-until port skipper))))))
     (if (>= index str-len)
         (begin 
           (values))
         (let ( (current-char (string-ref format-string index)) )
           (if (not (char=? current-char #\~))
               (let ((port-char (get-char port)))
                 (if (or (eof-object? port-char)
                         (not (char=? current-char port-char)))
                     (time-error 'string->date 'bad-date-format-string template-string))
                 (string->date date (+ index 1) format-string str-len port template-string))
               ;; otherwise, it's an escape, we hope
               (if (> (+ index 1) str-len)
                   (time-error 'string->date 'bad-date-format-string template-string)
                   (let* ( (format-char (string-ref format-string (+ index 1)))
                           (format-info (assoc format-char read-directives)) )
                     (if (not format-info)
                         (time-error 'string->date 'bad-date-format-string template-string)
                         (begin
                           (let ((skipper (cadr format-info))
                                 (reader  (caddr format-info))
                                 (actor   (cadddr format-info)))
                             (skip-until port skipper)
                             (let ((val (reader port)))
                               (if (eof-object? val)
                                   (time-error 'string->date 'bad-date-format-string template-string)
                                   (actor val date)))
                             (string->date date (+ index 2) format-string  str-len port template-string)))))))))))
 
 (define string->date
   (lambda (input-string template-string)
     (define (date-ok? date)
       (and (date-nanosecond date)
            (date-second date)
            (date-minute date)
            (date-hour date)
            (date-day date)
            (date-month date)
            (date-year date)
            (date-zone-offset date)))
     (let ( (newdate (make-date 0 0 0 0 #f #f #f (local-tz-offset))) )
       (string->date-from-port newdate
                               0
                               template-string
                               (string-length template-string)
                               (open-input-string input-string)
                               template-string)
       (if (date-ok? newdate)
           newdate
           (time-error 'string->date 'bad-date-format-string (list "Incomplete date read. " newdate template-string))))))
 )

