(library
 (rl3 types lists)

 (export
  last-pair make-list every? any? weave)
  
 (import
  (rnrs base)
  (only (rnrs arithmetic fixnums)
        fxzero?)
  (only (rl3 env prelude)
        fx1-)
  (primitives every? any?))
 
 ;; Returns the last pair in list.
 (define last-pair
   (lambda (x)
     (if (pair? (cdr x))
         (last-pair (cdr x))
         x)))

 ;; weave an element between a list of elements
(define weave
  (lambda (e lst)
    (if (null? lst)
        lst
        (if (null?  (cdr lst))
            lst
            (cons (car lst) (cons e (weave e (cdr lst))))))))
 
(define make-list
  (lambda (len val)
    (let loop ((len len) (lst '()))
      (if (fxzero? len)
          lst
          (loop (fx1- len) (cons val lst))))))

)
