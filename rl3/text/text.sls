;; general string / text manipulation

(library
 (rl3 text text)

 (export separate)
 
 (import
  (rnrs base)
  (only (rl3 types lists)
        weave))
 
 ;; weave a separator string strictly between every pair of strings in a list
 ;; (weave '("1" "2" "3") ",") -> "1,2,3"
 (define (separate sep lst)
   (apply string-append (weave sep lst)))

)
