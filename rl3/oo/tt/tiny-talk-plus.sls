#!r6rs
;;; FILE      "tiny-talk-plus.sls"
;;; IMPLEMENTS Adds object support for Scheme values to tiny-talk.
;;; AUTHOR    Ken [dot] Dickey [at] Whidbey [dot] Com

;;;COPYRIGHT (c) 2008 by Kenneth A Dickey. All rights reserved.
;;;
;;;Permission is hereby granted, free of charge, to any person
;;;obtaining a copy of this software and associated documentation
;;;files (the "Software"), to deal in the Software without
;;;restriction, including without limitation the rights to use,
;;;copy, modify, merge, publish, distribute, sublicense, and/or
;;;sell copies of the Software, and to permit persons to whom
;;;the Software is furnished to do so, subject to the following
;;;conditions:
;;;
;;;The above copyright notice and this permission notice shall
;;;be included in all copies or substantial portions of the Software.
;;;
;;;THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
;;;OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
;;;NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
;;;HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
;;;WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;OTHER DEALINGS IN THE SOFTWARE.


;;;tiny-talk=plus
(library (rl3 oo tt tiny-talk-plus)
 (export  $ object define-predicate ->string object? ;; tiny-talk also exported
          start-tiny-talk add-deputy! deputy-object  ;; plus
 )
 (import (rnrs)
         (rl3 oo tt tiny-talk)
         (rnrs mutable-pairs (6))
 )

;; EXPORTED INTERFACE
;;
;; (start-tiny-talk) -- inits this library!
;; (add-deputy! predicate object) -- add object behaviors to Scheme values
;; (object-deputy value) -> object or #f

;; A Deputy Object is an object deputized to represent a Scheme
;; value type.  This allows for methods on Scheme values.

;; A deputy is keyed on a predicate.
 
;; EXAMPLES:
;; (import (tiny-talk-plus))
;; (start-tiny-talk) ;==> OK
;; (add-deputy! list?   (object () [(length self) (length self)] ))
;; (add-deputy! string? (object () [(length self) (string-length self)] ))
;; (add-deputy! vector? (object () [(length self) (vector-length self)] ))
;; [$ length (list 1 2 3)  ] ;==> 3
;; [$ length "abc"         ] ;==> 3
;; [$ length (vector 1 2 3)] ;==> 3

;;; CAUTION: Predicates should be disjoint! [else order dependent]


;; The function deputy-object takes a Scheme value and
;; returns its deputy object or #f.  You can use this to
;; add behaviors.

;; [$ add-method! (deputy-object "a string")
;;      'reverse
;;      (lambda (self) (list->string (reverse (string->list self))))]
;; [$ reverse "abc"] ;==> "cba"

;;; CAUTION: shallow-clone and deep-clone only copy a value
;;;  there is no cloning of a deputy object.

 
(define deputy-alist '()) ;; A globle table, sigh.

(define (add-deputy! predicate object)
  (unless (and (procedure? predicate)
               (object? object))
    (error 'add-deputy!
           "requires a predicate and an object"
           predicate object))
  (cond
   [(assq predicate deputy-alist)
    =>
    (lambda (bucket) (set-cdr! bucket object))
    ]
   [else (set! deputy-alist
               (cons (cons predicate object) deputy-alist))]
) )
                             
(define (deputy-object thing)
  (let loop ( [deps deputy-alist] )
    (cond
     [(null? deps) #f]
     [((caar deps) thing) (cdar deps)]
     [else (loop (cdr deps))]
) ) )

(define (deputy-method-finder sym obj)
  ;; Answer a method or #f
  (cond
   [(deputy-object obj)
    => ;; object representing a Scheme native type
    (lambda (deputy) ([$ lookup deputy] sym))] ; NB: NO delegation
   [else #f])
)


(define (start-tiny-talk) ;; ensures this lib is "initialized"
  ;; Hook into tiny-talk -> (method lookup) function.
  (custom-method-finder deputy-method-finder)
  'OK)


;; =============================================================
;; Helpers

(define (slice indexed-coll start end repackage)
  ;; start-inclusive end-exclusive
  ;; repackage is list-><whatever>
  (let loop ( [elts '()] [index (- end 1)] )
    (if (< index start)
        (repackage elts)
        (loop (cons [$ iref indexed-coll index] elts) (- index 1))))
)

(define (id x) x)
;; =============================================================
;; "SIMPLE" (non-collection) OBJECTS

;; name =? shallow-clone deep-clone

(add-deputy! boolean?
   (object ()
     [(name self) 'boolean]
     [(=? self other)
      (or (and self other)
          (and (not self) (not other)))
      ]
     [(shallow-clone self) (if self #t #f)]
     [(deep-clone    self) (if self #t #f)]
     ;; ...
) )

(add-deputy! symbol?
   (object ()
     [(name self) 'symbol]
     [(=? self other) (eq? self other)]
     [(shallow-clone self) self]
     [(deep-clone    self) self]
     ;; ...
) )

(add-deputy! char?
   (object ()
     [(name self) 'character]
     [(=? self other) (and (char? other)(char=? self other))]
     [(shallow-clone self) self]
     [(deep-clone    self) self]
     ;; ...
) )

(add-deputy! procedure?
   (object ()
     [(name self) 'procedure]
     [(=? self other) (eq? self other)]
     [(shallow-clone self) self]
     [(deep-clone    self) self]
     ;; ...
) )

(add-deputy! number?
   (object ()
     [(name self) 'number]
     [(join self other) (+ self other)] ;; test
     [(negate self) (- self)]
     [(=? self other) (= self other)]
     [(shallow-clone self) self]
     [(deep-clone    self) self]

     ;; ...
) )


;; =============================================================
;; COLLECTIONS

;; length map for-each every? any? collect reject join
;; iref slice [for indexed collections only]

(add-deputy! string?
   (object ()
     [(name self) 'string]
     [(join self other) (string-append self other)]
     [(length self) (string-length self)]
     [(iref self index) (string-ref self index)]
     [(slice self start end)
      ;;(slice self start end list->string)
      (substring self start end)]
     [(=? self other) (and (string? other) (string=? self other))]
     [(for-each self proc) ;; for-each-elt
      (let ( [limit (string-length self)] )
        (let loop ( [index 0] )
          (if (>= index limit)
              'OK
              (begin (proc (string-ref self index))
                     (loop (+ index 1))))))
      ]
     [(map self proc) ;; NB: returns a string
      (let ( [limit (string-length self)] )
        (let loop ( [index 0] [results '()] )
          (if (>= index limit)
              (list->string (reverse results))
              (loop (+ index 1)
                    (cons (proc (string-ref self index))
                          results)))
       ) )
      ]
     [(every?   self proc) [$ every? (string->list self) proc]]
     [(any?     self proc) [$ any?   (string->list self) proc]]
     [(shallow-clone self) (string-copy self)]
     [(deep-clone    self) (string-copy self)]
     [(collect self proc)
      (let-values ( [(yes no) (partition proc (string->list self))] )
        (list->string yes))]
     [(reject self proc)
      (let-values ( [(yes no) (partition proc (string->list self))] )
        (list->string no))]
     ;; ...
) )


(add-deputy! vector?
   (object ()
     [(name self) 'vector]
     [(join self other) ;; (vector-append self other)
      (unless (vector? other)
        (error 'vector:join
               "requires two vectors to append together"
               self other))
      (list->vector
       (append (vector->list self)
               (vector->list other)))
      ]
     [(=? self other)
      (and (vector? other)
           (or (eq? self other)
               (and (= (vector-length self) (vector-length other))
                    (call-with-current-continuation
                     (lambda (return)
                       (vector-for-each
                        (lambda (a b)
                          (unless [$ =? a b] (return #f)))
                        self other)
                       (return #t))))))
      ]
     [(length self) (vector-length self)]
     [(iref self index) (vector-ref self index)]
     [(slice self start end)
      (slice self start end list->vector)]
     [(for-each self proc) (vector-for-each proc self)]
     [(map      self proc) (vector-map      proc self)]
     [(every?   self proc) [$ every? (vector->list self) proc]]
     [(any?     self proc) [$ any?   (vector->list self) proc]]
     [(shallow-clone self) (vector-map id self)]
     [(deep-clone    self) (vector-map id self)]
     [(collect self proc)
      (let-values ( [(yes no) (partition proc (vector->list self))] )
        (list->vector yes))]
     [(reject self proc)
      (let-values ( [(yes no) (partition proc (vector->list self))] )
        (list->vector no))]
     ;; ...
) )

(add-deputy! list?
   (object ()
     [(name self) 'list]
     [(length self) (length self)]
     [(join self other) (append self other)]
     [(=? self other) (equal? self other)
#|      (or (eq? self other)
          (and (list? other)
               (let loop ( [left self] [right other] )
                  (cond 
                   [(null? left ) (null? right)]
                   [(null? right) #f]
                   [[$ =? (car left) (car right)]
                    (loop (cdr left) (cdr right))]
                   [else #f])))) |#
      ]
     [(iref  self index) (list-ref self index)]
     [(slice self start end)
      ;; start-inclusive end-exclusive
      (let start-loop ( [list self] [index 0] )
        (if (< index start)
            (start-loop (cdr list) (+ 1 index))
            ;; ASSERT: (= index start)
            (let collect-loop ( [elts '()] [list list] [index index] )
              (if (< index end)
                  (collect-loop (cons (car list) elts)
                                (cdr list)
                                (+ index 1))
                  (reverse elts)))))
      ]
     [(every?   self proc) (for-all proc self)]
     [(any?     self proc) (exists  proc self)]
     [(shallow-clone self) (map id self)]
     [(deep-clone    self) (map id self)] ;; bogus
     [(for-each self proc) (for-all proc self)]
     [(map      self proc) (map     proc self)]
     [(collect self proc)
      (let-values ( [(yes no) (partition proc self)] )
        yes)]
     [(reject self proc)
      (let-values ( [(yes no) (partition proc self)] )
        no)]
     ;; ...
) )




)


;;		---   E O F   ---		;;
