ReadMePlease.txt

FILES:
- tiny-talk.sls      -- This is the ONLY required file. Read it.
- tiny-talk-plus.sls -- This library adds support for Scheme value types.

- tiny-code.sls      -- Test Code which uses tiny-talk.
- tiny-tests.ss      -- Simple regression test suite for tiny-talk & tt-plus.
- simple-regression-testing.sls -- Regression testing support/implementation.
- testing-tests.ss   -- Quick check test for simple-regression-testing.sls.

- ReadMePlease.txt   -- You are looking at me.
- Scribblings.txt    -- Some of my ramblings, er, commentary.
- yasos.sls          -- Historical: purely functional, non-reflective [ignorable]

Sample Test Invocations:
 cd /usr/local/src/TinyTalk
 larceny -r6rs --path /usr/local/src/TinyTalk -program "tiny-tests.ss"
 ikarus --r6rs-script tiny-tests.ss


Enjoy!
-KenD		Ken [dot] Dickey [at] Whidbey [dot] Com