#!r6rs
;;; FILE		"tiny-code.sls"
;;; IMPLEMENTS	Code to test TinyTalk Object System
;;;                based losely on Sun's Lively Kernel
;;;                http://research.sun.com/projects/lively
;;; AUTHOR	Kenneth Dickey
;;; DATE	2008 February 1

;;; COPYRIGHT (c) 2008 by Kenneth A Dickey.  All rights reserved.


(library (tiny-code)
  (export ;; point
          new-point point?
          polar->point list-of-points->rectangle 
          ;; rectangle
          new-rectangle rectangle?
          ;; color
          new-color color? color-named ;; random-color
          rgb->color hsb->color 
          ;; events
          make-event event? event-handlers

   )
  (import (rnrs)
          (rnrs mutable-pairs (6))
;;        (srfi random) 
          (format) ;; (srfi format)
          (tiny-talk-plus)
          )

;==============================================================;

  
;; Helpers
(define (->integer n)
  ;; return integer closest to n
  (exact (round n)))

(define (every? pred? . rest)
    (for-all pred? rest))

;==============================================================;
;;; POINT x y

(define-predicate point?)

;; use new-* for objects (vs make-* for records)
(define (new-point x y)
  ;; 2d, exact-integer values for x,y
  ;; Intent is for pixel position graphics
  (unless (and (integer? x)
               (integer? y))
    (error 'new-point "x and y must be integers" x y))
  (object ([x x] [y y])
     ;;methods
     [(point? self) #t] ;; Yes, this is a point!
     [(name self) 'point]
     [(->string self) (format "(new-point ~a ~a)" [$ x self] [$ y self])
      ]
     [(add self other)
      (cond
       ((point?  other) (new-point (+ [$ x self] [$ x other])
                                   (+ [$ y self] [$ y other])))
       ((number? other) (new-point (+ [$ x self] other)
                                   (+ [$ y self] other)))
       (else (error 'point:add "Can't add self to other" self other)))
      ]
     [(point-between self other)
      (unless (point? other)
        (error 'point:between
               "Don't know how to be between point and non-point"
               self other))
      (new-point (->integer (/ (+ [$ x self] [$ x other]) 2))
                 (->integer (/ (+ [$ y self] [$ y other]) 2)))
      ]
     [(sub self other)
      (cond
       [(point? other)  (new-point (- [$ x self] [$ x other])
                                   (- [$ y self] [$ y other]))]
       [(number? other) (new-point (- [$ x self] other)
                                   (- [$ y self] other))]
       [else (error 'point:sub "Can't add self to other" self other)])
      ]
     [(negate self)
      (new-point (- [$ x self]) (- [$ y self]))
      ]
     [(scale-by self scale)
      (unless (number? scale)
        (error 'point:scale-by "Don't know scale point with other" self scale))
      (new-point (->integer (* scale [$ x self]))
                 (->integer (* scale [$ y self])))
      ]
     [(=? self other)
      (unless (point? other)
        (error 'point:=?
               "Don't know how compare point to non-point"
               self other))
      (and (= [$ x self] [$ x other]) (= [$ y self] [$ y other]))
      ]
     [(<? self other)
      (unless (point? other)
        (error 'point:<?
               "Don't know how compare point to non-point"
               self other))
      (and (< [$ x self] [$ x other]) (< [$ y self] [$ y other]))
      ]
     [(>? self other)
      (unless (point? other)
        (error 'point:>?
               "Don't know how compare point to non-point"
               self other))
      (and (> [$ x self] [$ x other]) (> [$ y self] [$ y other]))
      ]
     [(<=? self other)
      (unless (point? other)
        (error 'point:<=?
               "Don't know how compare point to non-point"
               self other))
      (and (<= [$ x self] [$ x other]) (<= [$ y self] [$ y other]))
      ]
     [(>=? self other)
      (unless (point? other)
        (error 'point:>=?
               "Don't know how compare point to non-point"
               self other))
      (and (>= [$ x self] [$ x other]) (>= [$ y self] [$ y other]))
      ]
     [(<>? self other)
      (unless (point? other)
        (error 'point:<>?
               "Don't know how compare point to non-point"
               self other))
      (or (not (= [$ x self] [$ x other]))
          (not (= [$ y self] [$ y other])))
      ]
     [(min-point self other)
      (unless (point? other)
        (error 'point:min-point
               "Requires two points"
               self other))
      (new-point (min [$ x self] [$ x other])
                 (min [$ y self] [$ y other]))
      ]
     [(max-point self other)
      (unless (point? other)
        (error 'point:max-point
               "Requires two points"
               self other))
      (new-point (max [$ x self] [$ x other])
                 (max [$ y self] [$ y other]))
      ]
     [(nearest-point-on-line-between self start end)
      (unless (and (point? start) (point? end))
        (error 'point:nearest-point-on-line-between
               "Needs a point and two line end-points"
               self start end))
      (cond
       [(= [$ x start] [$ x end])
        (new-point [$ x start] [$ y self])
        ]
       [(= [$ y start] [$ y end])
        (new-point [$ x self] [$ y start])
       ]
       [else
        (let* ( (x1  [$ x start])
                (y1  [$ y start])
                (x21 (- [$ x end] x1))
                (y21 (- [$ y end] y1))
                (tao (/ (+ (/ (- [$ y self] y1) x21)
                           (/ (- [$ x self] x1) y21))
                        (+ (/ x21 y21)
                           (/ y21 x21))))
                )
          (new-point (->integer (+ x1 (* tao x21)))
                     (->integer (+ y1 (* tao y21)))))       
        ])
      ]
     [(distance-between self other)
      (unless (point? other)
        (error 'point:distance-between
               "Needs two points"
               self other))
      (let ( [dx (- [$ x self] [$ x other])]
             [dy (- [$ y self] [$ y other])]
           )
        (sqrt (+ (* dx dx) (* dy dy))))
      ]

     [(as-rectangle self)
      (new-rectangle [$ x self] [$ y self] 0 0) ; x y width height
      ]
     [(rect-from-extent self extent) ; extent = (width,height)
      (unless (point? extent)
        (error 'point:extent
               "Needs two points"
               self extent))
      (new-rectangle [$ x self] [$ y self] [$ x extent] [$ y extent])
      ]
     [(rect-from-any-2-points self other)
      (unless (point? other)
        (error 'point:rect-from-any-2-points
               "Needs two points"
               self other))
      [$ rect-from-2-points [$ min-point self other]
                            [$ max-point self other]]
      ]
     [(rect-from-2-points self other)
      (new-rectangle [$ x self] [$ y self]
                     (abs (- [$ x other] [$ x self]))
                     (abs (- [$ y other] [$ y self])))
      ]
     [(polar-r self)
      [$ distance-between self (new-point 0 0)]
      ]
     [(polar-theta self)
      (atan (+ 0.0 (/ [$ y self] [$ x self])))
      ]
) )


(define (polar->point r theta)
  (new-point (->integer (* r (sin theta)))
             (->integer (* r (cos theta))))
)


(define (list-of-points->rectangle list-of-points)
  (if (null? list-of-points)
      (new-rectangle 0 0 0 0)
      (let loop ( [min-pt (car list-of-points)]
                  [max-pt (car list-of-points)]
                  [points (cdr list-of-points)]
                )
        (if (null? points)
            [$ rect-from-2-points min-pt max-pt]
            (let ( (pt (car points)) )
              (loop [$ min-point min-pt pt]
                    [$ max-point max-pt pt]
                    (cdr points)))))
) )


  
;==============================================================;
;;; RECTANGLE x y width height
;;
;; Nota Bene: Screen (0,0) is upper left corner
;;              ..with Y increasing downward.
;;  o-->X
;;  |
;;  v   (x1,y1) => upper-left corner point
;;  Y     *----------*
;;        |          |\
;;        |          | } height
;;        |          |/
;;        *----------* (x2,y2) = lower-right corner
;;           width    extent = (width, height)

(define-predicate rectangle?)


(define (new-rectangle x y width height)
  (unless (every? (lambda (n) (and (integer? n) (>= n 0)))
                  x y width height)
    (error 'new-rectangle
           "parameters must be non-negative integers"
           x y width height))
  (let ( [ul-point (new-point x y)] )
         
  (object ( [width width] [height height] )

   [(rectangle? self) #t]
   
   [(delegate self) ul-point]
   
   [(name self) 'rectangle]
   
   [(->string self)
    (format "(new-rectangle ~a ~a ~a ~a)"
            [$ x self]     [$ y self]
            [$ width self] [$ height self])
    ]
   [(=? self other)
    (if (rectangle? other)
        (and (= [$ x self] [$ x other])
             (= [$ y self] [$ y other])
             (= [$ width  self] [$ width  other])
             (= [$ height self] [$ height other]))
        (error 'rectangle:=?
               "Can only compare two rectangles"
               self other))
    ]
   [(extent self) (new-point [$ width self] [$ height self])
    ]
   [(max-x self) (+ [$ x self] [$ width  self])
    ]
   [(max-y self) (+ [$ y self] [$ height self])
    ]
   [(with-width self width)
    (new-rectangle [$ x self] [$ y self] [->integer width] [$ height self])
    ]
   [(with-height self height)
    (new-rectangle [$ x self] [$ y self] [$ width self] (->integer height))
    ]
   [(with-x self x)
    (new-rectangle (->integer x) [$ y self] [$ width self] [$ height self])
    ]
   [(with-y self y)
    (new-rectangle [$ x self] (->integer y) [$ width self] [$ height self])
    ]
   [(with-extent self extent-pt)
    (new-rectangle [$ x self] [$ y self] [$ x extent-pt] [$ y extent-pt])
    ]
   ;; Control Handle Support
   [(center-point self)
    (new-point (->integer (+ [$ x self] (/ [$ width  self] 2)))
               (->integer (+ [$ y self] (/ [$ height self] 2))))
    ]
   [(top-left-point self)
    [$ delegate self] ;; @@?Copy to avoid side effects?@@
    ]
   [(top-right-point self)
    (new-point (+ [$ x self] [$ width self]) [$ y self])
    ]
   [(bottom-left-point self)
    (new-point [$ x self] (+ [$ y self] [$ height self]))
    ]
   [(bottom-right-point self)
    (new-point (+ [$ x self] [$ width  self])
               (+ [$ y self] [$ height self]))
    ]
   [(left-center-point self)
    (new-point [$ x self]
               (->integer (+ [$ y self] (/ [$ height self] 2))))
    ]
   [(right-center-point self)
    (new-point (+ [$ x self] [$ width self])
               (->integer (+ [$ y self] (/ [$ height self] 2))))
    ]
   [(top-center-point self)
    (new-point (->integer (+ [$ x self] (/ [$ width self] 2)))
               [$ y self])
    ]
   [(bottom-center-point self)
    (new-point (->integer (+ [$ x self] (/ [$ width self] 2)))
               (+ [$ y self] [$ height self]))
    ]
   [(with-top-left self point)
    [$ rect-from-any-2-points point [$ bottom-right-point self]]
    ]
   [(with-top-right self point)
    [$ rect-from-any-2-points point [$ bottom-left-point self]]
    ]
   [(with-bottom-left self point)
    [$ rect-from-any-2-points point [$ top-right-point self]]
    ]
   [(with-bottom-right self point)
    [$ rect-from-any-2-points point [$ top-left-point self]]
    ]
   [(with-left-center self point)
    (new-rectangle [$ x point]
                   [$ y self]
                   (+ [$ width self] (- [$ x self] [$ x point]))
                   [$ height self])
    ]
   [(with-right-center self point)
    (new-rectangle [$ x point]
                   [$ y self]
                   (- [$ x point] [$ x self])
                   [$ height self])
    ]
   [(with-bottom-center self point)
    (new-rectangle [$ x self]
                   [$ y self]
                   [$ width self]
                   (- [$ y point] [$ y self]))
    ]
   [(with-top-center self point)
    (new-rectangle [$ x self]
                   [$ y point]
                   [$ width self]
                   (+ [$ height self]
                      (- [$ y self] [$ y point])))
    ]
   ;; Containment
   [(contains-point? self point)
    (and (<= [$ x self] [$ x point] (+ [$ x self] [$ width  self]))
         (<= [$ y self] [$ y point] (+ [$ y self] [$ height self])))
    ]
   [(contains-rect? self rectangle) ;; STRICT [full] containment
    (and (<= [$ x self] [$ x rectangle])
         (<= [$ y self] [$ y rectangle])
         (<= [$ max-x rectangle] [$ max-x self])
         (<= [$ max-y rectangle] [$ max-y self]))
    ]
   ;; Point must remain within rect
   [(constrain-point self point) 
    (new-point (min [$ max-x self] (max [$ x self] [$ x point]))
               (min [$ max-y self] (max [$ y self] [$ y point])))
    ]
   [(intersection self other)
    (unless (rectangle? other)
      (error 'rectangle:intersection
             "Interscetion requires two rectangles"
             self other))
    [$ rect-from-2-points [$ max-point [$ top-left-point self]
                                       [$ top-left-point other]]
                          [$ min-point [$ bottom-right-point self]
                                       [$ bottom-right-point other]]]
    ]
   [(union self other)
    (unless (rectangle? other)
      (error 'rectangle:union
             "Union requires two rectangles"
             self other))
    [$ rect-from-2-points [$ min-point [$ top-left-point self]
                                       [$ top-left-point other]]
                          [$ max-point [$ bottom-right-point self]
                                       [$ bottom-right-point other]]]
    ]
   [(empty? self)
    (and (zero? [$ width self]) (zero? [$ height self]))
    ]
   [(distance-between self other)
    (cond
     [(rectangle? other)
      (if (not [$ empty? [$ intersection self other]])
          0
          (let*
              ( [p1 [$ closest-point-to self [$ center-point other]]]
                [p2 [$ closest-point-to other p1]]
              )
            [$ distance-between p1 p2]))
      ]
     [(point? other)
      (if [$ contains-point? self other]
          0
          [$ closest-point-to self other])
      ]
     [else
      (error 'rectangle:distance-between
             "Requires a rectangle and another rectangle or a point"
             self other)])
    ]
    [(closest-point-to self point)
     (unless (point? point)
       (error 'rectangle:closest-point-to
              "requires a rectangle and a point"
              self point))
     ;; return a perimeter point
     (new-point (min (max [$ x self] [$ x point]) [$ max-x self])
                (min (max [$ y self] [$ y point]) [$ max-y self]))
     ]
    [(translate-by self delta)
     (unless (point? delta)
       (error 'rectangle:translate-by
              "requires a rectangle and a point"
              self delta))
     (new-rectangle (+ [$ x delta] [$ x self])
                    (+ [$ y delta] [$ y self])
                    [$ width  self]
                    [$ height self])
     ]
    [(scale-by self scale)
     (cond
      [(number? scale)
       (new-rectangle [$ x self] [$ y self]
                      (->integer (* scale [$ width  self]))
                      (->integer (* scale [$ height self])))
       ]
      [(rectangle? scale)
       ;; relative rectangle, e.g. pane spec in a window
       (new-rectangle (+ [$ x self] (* [$ x scale] [$ width  self]))
                      (+ [$ y self] (* [$ y scale] [$ height self]))
                      (* [$ width  self] [$ width  scale])
                      (* [$ height self] [$ height scale]))
       ]
      ;; NB: a rectangle is also a point, so point comes 2nd
      [(point? scale)
       (new-rectangle [$ x self] [$ y self]
                      (* [$ x scale] [$ width  self])
                      (* [$ y scale] [$ height self]))
       ]
      [else
       (error 'rectangle:scale
              "Don't know how to scale a rectangle with scale.."
              self scale)]
      )
     ]
    [(expand-by self expansion)
     [$ inset-by self
        (if (object? expansion) [$ negate expansion] (- expansion))]
     ]
    [(inset-by self inset)
     (cond
      [(number? inset)
       (new-rectangle (->integer (+ inset [$ x self]))
                      (->integer (+ inset [$ y self]))
                      (->integer (- [$ width  self] (* inset 2)))
                      (->integer (- [$ height self] (* inset 2))))
       ]
      [(point? inset)
       (new-rectangle (+ [$ x self] [$ x inset])
                      (+ [$ y self] [$ y inset])
                      (- [$ width  self] (* [$ x inset] 2))
                      (- [$ height self] (* [$ y inset] 2)))
       ]
      [else
       (error 'rectangle:inset-by
              "Don't know how to inset rectangle by inset.."
              self inset)]
      )
     ]
   
) ) )


;==============================================================;
;;; COLORs  red green blue

(define-predicate color?)

(define (new-color red green blue)
;; @@FIXME@@ rgb values range from 0 to 1, use 0..255
  (unless
      (every? (lambda (c) (and (number? c) (<= 0 c 1)))
              red green blue)
    (error 'new-color
           "red, green, and blue values must be in range 0..1"
            red green blue))
  (object ( [red red] [green green] [blue blue] )
   [(color? self) #t]
   [(->string self)
    (format "(new-color ~a ~a ~a)" [$ red self] [$ green self] [$ blue self])
    ]
   [(=? self other)
    (unless (color? other)
      (error 'color:=?
             "comparison requires two colors"
             self other))
    (let ( [epsilon 0.0001] ) ;; color values range 0..1
      (and (< (- [$ red   self] [$ red   other]) epsilon)
           (< (- [$ green self] [$ green other]) epsilon)
           (< (- [$ blue  self] [$ blue  other]) epsilon)))
    ]
   [(mixed-with self other proportion)
    (unless (and (color? other)
                 (number? proportion)
                 (<= 0 proportion 1))
      (error 'color:mixed-with
             "requires two colors and a proportion"
             self other proportion))
    (let ( [left  proportion]
           [right (- 1 proportion)]
         )
      (new-color (+ (* [$ red   self] left) (* [$ red   other] right))
                 (+ (* [$ green self] left) (* [$ green other] right))
                 (+ (* [$ blue  self] left) (* [$ blue  other] right))))
    ]
   [(darker self shades)
    (unless (and (integer? shades)
                 (>= shades 0))
      (error 'color:darker
             "shades must a positive integer"
             shades))
    (let ( [black (color-named 'black)] )
      (let loop ( [shades shades] [color [$ mixed-with self black 0.5]] )
        (if (zero? shades)
            color
            (loop (- shades 1) [$ mixed-with color black 0.5]))))
    ]
   [(lighter self shades)
    (unless (and (integer? shades)
                 (>= shades 0))
      (error 'color:lighter
             "shades must a positive integer"
             shades))
    (let ( [white (color-named 'white)] )
      (let loop ( [shades shades] [color [$ mixed-with self white 0.5]] )
        (if (zero? shades)
            color
            (loop (- shades 1) [$ mixed-with color white 0.5]))))
    ])
)

(define (rgb->color r g b) ;; range of r g b is 0..#xFF=255
  (unless (and (integer? r) (integer? g) (integer? b)
               (<= 0 r 255)
               (<= 0 g 255)
               (<= 0 b 255))
    (error 'rgb->color
           "rgb values must be integers in range 0..255"
           r g b)
    )
  (new-color (/ r 255) (/ g 255) (/ b 255))
)

;; HSB a.k.a. HSV
;; http://en.wikipedia.org/wiki/HSL_color_space#Conversion_from_HSV_to_RGB
(define (hsb->color hue saturation brightness)
  (unless (and (integer? hue)
               (number?  saturation)
               (number?  brightness)
               (<= 0 hue        360)
               (<= 0 saturation   1)
               (<= 0 brightness   1))
    (error 'hsb->color
           "hue range 0..360 [degrees]; saturation & brightness range 0..1"
           hue saturation brightness)
    )
  (if (zero? saturation)
      ;; zero saturation yields gray with given brightness
      (rgb->color brightness brightness brightness)
      (let*
          ( (hue/60 (/ (mod hue 360) 60))
            (hue-index (floor hue/60)) ;; integer part
            (f (- hue/60 hue-index))   ;; fractional part
            (p (* brightness (- 1 saturation)))
            (q (* brightness (- 1 (* f saturation))))
            (t (* brightness (- 1 (* (- 1 f) saturation))))
          )
;         (display  ;;@@DEBUG@@;;
;          (format "i=~a f=~a p=~a q=~a t=~a b=~a ~%"
;                  hue-index f p q t brightness))
        (case hue-index
          ((0) (new-color brightness t p)
           )
          ((1) (new-color q brightness p)
           )
          ((2) (new-color p brightness t)
           )
          ((3) (new-color p q brightness)
           )
          ((4) (new-color t p brightness)
           )
          ((5) (new-color brightness p q)
           )
          (else
           (new-color 0 0 0))
          )
) ) )

#|
(define (random-color)
  (rgb->color (+ .0 (random-integer 255))
              (+ .0 (random-integer 255))
              (+ .0 (random-integer 255)))
)
|#

(define color-named
  (let ( (name-db
          ;;          ...@@CHECK THESE@@
          (list
             (cons 'black      (new-color  0  0  0))
             (cons 'white      (new-color  1  1  1))
             (cons 'gray       (new-color .8 .8 .8))
             (cons 'red        (new-color .8  0  0))
             (cons 'green      (new-color  0 .8  0))
             (cons 'yellow     (new-color  0  0 .8))
             (cons 'blue       (new-color  0  0 .8))
             (cons 'purple     (rgb->color 131   0 201))
             (cons 'magenta    (rgb->color 204   0 255))
             (cons 'turquoise  (rgb->color   0 240 255))
             (cons 'brown      (rgb->color 182  67   0))
             (cons 'orange     (rgb->color 255 153   0))
             (cons 'lime-green (rgb->color  51 255   0))
             (cons 'cyan       (rgb->color   0 255 255))
             (cons 'pink       (rgb->color 255  30 153))
             (cons 'light-gray (rgb->color 189 190 192))
             (cons 'dark-gray  (new-color   .4  .4  .4))
           )
          )
       )
    (lambda (name)
      (let ( (name-sym (if (string? name) (string->symbol name) name)) )
        (cond
         ((assq name-sym name-db) => cdr)
         (else
          (error 'color-named
                 "unknown color name"
                 name)))))
) )
  
;==============================================================;

;==============================================================;
;;; EVENTS

(define-predicate event?)

(define (make-event name . args)
  (object ([name name] [args args])
   [(event? self) #t]
) )

(define default-handler
  (lambda (evt)
    (error 'event-handler
           "unknown event"
           evt)))

(define event-handlers
  (let ( [handler-alist '()] )
    (object ()
     [(name self) 'event-handlers]
     [(->string self)
      (format "#<EVENT-HANDLER ~a>" (map car handler-alist))
      ]
     [(add-handler! self event-name new-handler)
      (cond
       [(assq event-name handler-alist)
        => (lambda (bucket)
             (let ( (old-handler (cdr bucket)) )
               (set-cdr! bucket new-handler)
               old-handler))
        ]
       [else
        (set! handler-alist
              (cons (cons event-name new-handler) handler-alist))
        default-handler
        ])
      ]
     [(handle-event self event)
      (cond
       [(and (event? event)
             (assq [$ name event] handler-alist))
        => (lambda (bucket) ((cdr bucket) event))]
       [else (default-handler event)])
     ])
) )


) ;; end-library

;;			--- End tiny-code ---
