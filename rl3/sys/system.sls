;;;
;; Drop Larceny system information procs into R6RS library.
;; Useful for importing for expansion in FFI
;;;

(library
 (rl3 sys system)
 
 (export
  break getenv
  call-without-interrupts disable-interrupts enable-interrupts error-handler timer-interrupt-handler
  os-type perror reset-handler system standard-timeslice system-features)
 
 (import
  (rnrs base)
  (only (rnrs lists)
        assq member)
  (primitives call-without-interrupts larceny-break getenv system system-features
              enable-interrupts error-handler disable-interrupts
              reset-handler standard-timeslice timer-interrupt-handler)
  (ffi ffi-std))
 
 (define break larceny-break)

