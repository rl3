(library
 
 (rl3 concurrency green-threads mutexes)
 
 (export)
 
 (import
  (primitives record-updater)
  (rnrs base)
  (only (rnrs io simple)
        display newline)
  (only (rnrs control)
        case-lambda when)
  (only (rnrs arithmetic fixnums)
        fxand fxzero?)
  (err5rs records procedural)
  (only (err5rs records inspection)
        record-rtd)
  (only (env prelude)
        unspecified)
  (only (rl3 concurrency green-threads threads)
        current-thread thread-alive? thread-block thread-unblock without-interrupts)
  (only (rl3 concurrency green-threads condition-variables)
        condition-variable-put!))
 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; SRFI-18 implemntation
;;; Mutex
;;; by Ray Racine
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
 
 ;; Borrows heavily from the orginal
 ;; Larceny lib/Standard/mutex.sch
 
 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
 ;;                     MUTEX States
 ;;
 ;; I don't know where the SRFI-18 author gets his concept naming from ...
 ;; LOCKED-OWNED  = 0 0xb00 => Locked and owned by a thread T
 ;; NOT-OWNED     = 2 0xb10 => Locked without an owner.
 ;; NOT-ABANDONED = 1 0xb01 => Not locked, not abandoned.
 ;; ABANDONED     = 3 0xb11 => Not locked, abandoned.
 ;; First bit determines locked / unlocked status.
 ;; Second bit determines owend / abandoned status.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
 
 (define locked/owned           0)
 (define locked/not-owned       2)
 (define unlocked/not-abandoned 1)
 (define unlocked/abandoned     3)
 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
 ;; Manually define the mutex record type
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
 
 (define mutex-rtd
   (make-rtd 'mutex '#(state         ;; fixnum, LOCKED NOT-OWNED ABANDONED NOT-ABANDONED
                       owner         ;; Owning Thread
                       blocked       ;; blocked threads
                       name          ;; string, useful for debugging
                       specific)))   ;; user specific slot
 
 (define make-mutex
   (let* ((ctor (rtd-constructor mutex-rtd))
          (make (lambda (name)
                  (ctor unlocked/not-abandoned
                        #f '() name (unspecified)))))
     (case-lambda
       (() (make "anon"))
       ((name) (make name)))))
 
 (define mutex?
   (rtd-predicate mutex-rtd))
 
 (define mutex-name
   (rtd-accessor mutex-rtd 'name))
 
 (define mutex-name-set!
    (rtd-mutator mutex-rtd 'name))

  (define mutex-specific
    (rtd-accessor mutex-rtd 'specific))

  (define mutex-specific-set!
    (rtd-mutator mutex-rtd 'specific))

  (define state-bits
    (rtd-accessor mutex-rtd 'state))

  (define state-bits-set!
    (rtd-mutator mutex-rtd 'state))

  (define blocked
    (rtd-accessor mutex-rtd 'blocked))

  (define blocked-set!
    (rtd-mutator mutex-rtd 'blocked))

  (define owner
    (rtd-accessor mutex-rtd 'owner))

  (define owner-set!
    (rtd-mutator mutex-rtd 'owner))

  (define locked?
    (let ((lock-mask #b01))
      (lambda (mutex)
        (fxzero? (fxand (state-bits mutex) lock-mask)))))
  
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;; mutex-state
  ;;
  ;;Returns information about the state of the mutex. The possible results are:
  ;;  * thread T: the mutex is in the locked/owned state and
  ;;    thread T is the owner of the mutex
  ;;  * symbol NOT-OWNED: the mutex is in the locked/not-owned state
  ;;  * symbol ABANDONED: the mutex is in the unlocked/abandoned state
  ;;  * symbol NOT-ABANDONED: the mutex is in the unlocked/not-abandoned state 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  
  (define mutex-state
    (lambda (mutex)
      (if (locked? mutex)
          (owner mutex)
          (case (state-bits mutex)
            ((1) 'NOT-ABANDONED)
            ((2) 'NOT-OWNED)
            ((3) 'ABANDONED)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;; (mutex-lock! mutex [timeout [thread]])
  ;;
  ;; If the mutex is currently locked, the current thread waits until the mutex is unlocked,
  ;; or until the timeout is reached if timeout is supplied.
  ;;
  ;; If the timeout is reached, mutex-lock! returns #f.
  ;; Otherwise, the state of the mutex is changed as follows:
  ;;
  ;;  * if thread is #f the mutex becomes locked/not-owned,
  ;;  * otherwise, let T be thread (or the current thread if thread is not supplied),
  ;;      o if T is terminated the mutex becomes unlocked/abandoned,
  ;;      o otherwise mutex becomes locked/owned with T as the owner. 
  ;;
  ;; After changing the state of the mutex, an "abandoned mutex exception" is raised if the
  ;; mutex was unlocked/abandoned before the state change, otherwise mutex-lock! returns #t.
  ;;
  ;; It is not an error if the mutex is owned by the current thread
  ;; (but the current thread will have to wait).
  ;;
  ;; Notes: 1) Timeouts not implmented, yet, and are ignored.
  ;;        2) abandoned mutex exception is not thrown.
  ;;        3) Original Larceny impl was reentrant for the same thread with a count.
  ;;           SRFI-18 says a reentrant thread will block on a lock it already owns.
  ;;           Why?  If no reason reveals itself, lets put back reentrant locks.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

  (define mutex-lock!
    (let ((lock
           (lambda (mutex timeout thread)  ;; timeout ignored
             (without-interrupts     ;; how can we be looping without interrupts PRR ???  prob.  double lock pattern here.      
              (let loop ()
                (newline)
                (if (locked? mutex)
                    (begin           ;; wait
                      (blocked-set! mutex (cons thread (blocked mutex)))
                      (thread-block thread)  ;; block -> removes from scheduler, and yields (switched).
                      (loop))
                    (if (not thread) ;; lock
                        (state-bits-set! mutex locked/not-owned)
                        (if (thread-alive? thread)
                            (begin
                              (state-bits-set! mutex locked/owned)
                              (owner-set! mutex thread))
                            (state-bits-set! mutex locked/not-owned)))) ;; dead thread, srfi-18 requires exception here
                #t)))))
      
      (case-lambda    
        ((mutex)
         (lock mutex -1 (current-thread)))
        ((mutex timeout)         ;; timeout ignored
         (lock mutex -1 (current-thread)))
        ((mutex timeout thread)
         (lock mutex timeout thread)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;; (mutex-unlock! mutex [condition-variable [timeout]])  ;procedure
  ;;
  ;; Unlocks the mutex by making it unlocked/not-abandoned.
  ;; It is not an error to unlock an unlocked mutex and a mutex that is owned by any thread.
  ;;
  ;; If condition-variable is supplied, the current thread is blocked and added to the condition-variable
  ;; before unlocking mutex; the thread can unblock at any time but no later than when an appropriate call
  ;; to condition-variable-signal! or condition-variable-broadcast! is performed (see below), and no later
  ;; than the timeout (if timeout is supplied).
  ;;
  ;; If there are threads waiting to lock this mutex, the scheduler selects a thread, 
  ;; the mutex becomes locked/owned or locked/not-owned, and the thread is unblocked.
  ;; mutex-unlock! returns #f when the timeout is reached, otherwise it returns #t.
  ;;
  ;; Notes: 1) Timeout is currently ignored and assumes a value of infinite, ie, wait forever.
  ;;        2) The above SRFI-18 condition-variable logic demands a strict sequence, which we do not do.
  ;;           However, all steps are atomic so we meet the semantics.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

  (define mutex-unlock!
    (let ((unlock
           (lambda (mutex condition-variable timeout)
             (without-interrupts
              (state-bits-set! mutex unlocked/not-abandoned)                
              (owner-set! mutex #f)
              (let ((blocked (blocked mutex)))
                (blocked-set! mutex '())
                (for-each thread-unblock blocked))
              (when condition-variable
                (let ((thread (current-thread)))
                  (condition-variable-put! condition-variable thread)
                  (thread-block thread)))      ;; block -> removes from scheduling, yields (thread:switch)
              #t))))
      
      (case-lambda
        ((mutex)
         (unlock mutex #f -1))
        ((mutex condition-variable)
         (unlock mutex condition-variable -1))
        ((mutex condition-variable timeout)
         (unlock mutex condition-variable timeout)))))

  
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;; define a printer routine for when a mutex
  ;; is displayed
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;   ((record-updater (record-rtd mutex-rtd) 'printer)
;;    mutex-rtd
;;    (lambda (mutex out) 
;;      (display "<mutex \"" out)
;;      (display (string-append (mutex-name mutex) ":"
;;                              (symbol->string (mutex-state mutex)) ":"
;;                              (number->string (state-bits mutex))) out)
;;      (display "\">" out)))
  
)