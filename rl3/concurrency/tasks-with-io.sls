;; Tasking with non-blocking IO
;; Ray Racine 

;; Original work from Larceny's tasking-with-io.sch
                                        ;
;; Copyright 1999 Lars T Hansen
;;
;; $Id: tasking-with-io.sch 3578 2006-10-03 20:31:33Z pnkfelix $
;;
;; Tasking system extensions supporting nonblocking I/O and a
;; nonblocking console.  This code is not platform-specific.
;;
;; It isn't necessary to use the nonblocking console here if your
;; program does not require interactive user input from the console,
;; and does not invoke the repl.  Just comment out the REQUIRE and the
;;  parameterization of the factories in WITH-TASKING.


;; Primordial task the process must always make progress when possible.
;; Primoridial task blocks if there are no RUNNABLE tasks and there exists at 
;; least one blocked task on IO.
;; If there there are no RUNNABLE or BLOCKED tasks then the primoridial task
;; exists the tasking system.

(library
 
 (rl3 concurrency tasks-with-io)
 
  (export
   with-tasking-io
   input-not-ready-handler output-not-ready-handler)
  
  (import
   (primitives reverse-assq)
   (rnrs base)
   (only (rnrs control)
         do)
   (only (rnrs io simple)
         current-output-port display newline)
   (only (rnrs lists)
         assq remq)
   (rl3 env parameters)
   (only (rl3 io print)
         format)
   (only (rl3 concurrency tasks)
         current-task end-tasking task-state task-handlers-set!
         task-unblock task-block with-tasking without-interrupts)
   (only (rl3 io epoll constants)
         EPOLL-CTL-ADD EPOLL-CTL-DEL
         EPOLLOUT EPOLLIN EPOLLERR)
   (only (rl3 io epoll)
         ioevent-fd 
         epoll-close epoll-create epoll-ctl epoll-wait))
  
  ;; Invariant: Only BLOCKED tasks are ever in the blocked lists
  ;; Enforced by the state-changed-handler
  
  (define *blocked-on-input*  '())	; ((fd . task) ...)
  (define *blocked-on-output* '())	; ((fd . task) ...)
  
;;; TODO ideally parameterized values should be task local
  
  (define with-tasking-io
    (lambda (thunk)
      ;;    (parameterize ((console-input-port-factory 
      ;;                    nonblocking-console-input-port)
      ;;                   (console-output-port-factory 
      ;;                    nonblocking-console-output-port))
      ;;      (parameterize ((current-input-port ((console-input-port-factory)))
      ;;                     (current-output-port ((console-output-port-factory))))
      (task-handlers-set! idle-handler poll-handler)  ;; FIXME parameterize?? RPR
      (with-tasking thunk)))

;;;
  (define input-not-ready-handler
    (lambda (desc)
      (without-interrupts
       (set! *blocked-on-input* 
             (cons (cons desc (current-task)) *blocked-on-input*))
       (task-block (current-task)))))

;;;

  (define output-not-ready-handler
    (lambda (desc)
      (without-interrupts
       (set! *blocked-on-output* 
             (cons (cons desc (current-task)) *blocked-on-output*))
       (task-block (current-task)))))

;;;
  ;; If the scheduler has RUNNABLE tasks then check for blocked tasks
  ;; with ready io.
  ;; Invoke while in critical section

  (define idle-handler
    (let ((task-end-notification
           (lambda ()
             (newline)
             (display "; The run queue is empty and no tasks are blocked on I/O")
             (newline)
             (display "; Ending tasking")
             (newline))))    
      (lambda ()
;;         (format (current-output-port)
;;                 "Idle Handler Input: ~s Output: ~s~%"
;;                 (length *blocked-on-input*)
;;                 (length *blocked-on-output*))
        (if (and (null? *blocked-on-input*)
                 (null? *blocked-on-output*))
            (begin
              (task-end-notification)
              (end-tasking))
            (poll-for-io #t)))))

;;;
  ;; Overrideable handler periodically invoked by the scheduler.
  ;; Checks if any tasks are ready.
  ;; Invoked while in critical section

  (define poll-handler
    (lambda ()
;;         (format (current-output-port)
;;                 "Poll Input: ~s Output: ~s~%"
;;                 (length *blocked-on-input*)
;;                 (length *blocked-on-output*))
      (poll-for-io #f)))  ;; #f do not block system

;;;
  ;; Overrideable handler invoked by scheduler whenever a task changes state.
  ;; Critical section procedure

  ;; FIXME: This is a pretty inefficient enforcing the invariant that non BLOCKED
  ;; tasks are never in the blocked-on-input/output queues.
  ;; Currently this is an O(N) operation for every task that changes state.
  ;; With *lots* of tasks, e.g., web-server, this is onerous and unnecessary.
  ;; i.e., The scheduler knows that task was BLOCKED before it changed the task state.
  ;; Maybe the scheduler should explicitely call thead:remove-from-ioblock only if the
  ;; task's previous state was blocked.  With a flag for input or output blocked checking
  ;; both lists is avoided.

  (define task-state-changed-handler
    (lambda (task)
      (case (task-state task)
        (( RUNNABLE RUNNING TERMINATED )
         (remove-from-ioblock-if-blocked task)))))

;;;
  ;; Poll 
  ;; 
  ;; Notes: Current Larceny vanilla loops and individually polls each file descriptor for readiness.
  ;; Here I'm explicitly switching to epoll for Linux.
  ;; epoll checks a set of fds for readiness in a single call.
  ;;
  ;; Critical section procedure

  (define inpoll-flgs  (list EPOLLIN EPOLLERR))
  (define outpoll-flgs (list EPOLLOUT EPOLLERR))
  
  (define clear-blocked
    (lambda (epoll-fd ioevents blocked-id)
      ;; clear all fds from the given ctrl
;;       (format (current-output-port)
;;               "Clearing ~s ~s ~s~%" epoll-fd ioevents blocked-id)
      (for-each (lambda (ioe)
                  (epoll-ctl epoll-fd
                             EPOLL-CTL-DEL
                             (ioevent-fd ioe)
                             '()))
                ioevents)
      ;; unblock tasks
      (case blocked-id
        ((IN)
         (let loop ((ioevents ioevents))
             (if (pair? ioevents)
                 (begin
                   ;;(format (current-output-port)
                   ;;        "Unblocking ~s  ~s ~s from ~s.~%" (ioevent-fd (car ioevents)) ioevents *blocked-on-input*)
                   (cond
                    ((assq (ioevent-fd (car ioevents)) *blocked-on-input*) =>
                     (lambda (probe)
                       (set! *blocked-on-input*
                             (remq probe *blocked-on-input*))
                       (task-unblock (cdr probe))))
                    (else "I/O signalled i/o IN ready on unblocked task"))
                   (loop (cdr ioevents)))
                 #t)))
        ((OUT)
         (let loop ((ioevents ioevents))
           (if (pair? ioevents)
               (begin
                 (cond ((assq (ioevent-fd (car ioevents)) *blocked-on-output*)
                        => (lambda (probe)
                             (set! *blocked-on-output*
                                   (remq probe *blocked-on-output*))
                             (task-unblock (cdr probe))))
                       (else "I/O signalled i/o OUT ready on unblocked task"))
                 (loop (cdr ioevents)))
               #t)))
        (else
         (error "clear-blocked given unknown block-id")))))

  (define build-epoll-fd
    (lambda (epoll-fd fds epoll-flgs)
      (for-each (lambda (fd)
                  (epoll-ctl epoll-fd
                             EPOLL-CTL-ADD
                             fd
                             epoll-flgs))
                fds)
      epoll-fd))

  (define POLL-NOWAIT  0)
  (define POLL-WAIT   -1)
  
  (define poll-for-io
    (lambda (block-system)
;;       (format (current-output-port) "Polling for I/O ~s~%" (current-task))
;;       (format (current-output-port) "Blocked input: ~s~%" *blocked-on-input*)
      (let ((epoll-in-fd  (epoll-create))
            (epoll-out-fd (epoll-create)))
        (let ((in-ready (epoll-wait
                         (build-epoll-fd epoll-in-fd
                                         (map car *blocked-on-input*) (list EPOLLIN)) POLL-NOWAIT))
              (out-ready (epoll-wait
                          (build-epoll-fd epoll-out-fd
                                          (map car *blocked-on-output*) (list EPOLLOUT)) POLL-NOWAIT)))
;;           (format (current-output-port)
;;                   "Ready In: fd=~s ~s  Out: fd= ~s ~s System Block: ~s ~%" epoll-in-fd in-ready epoll-out-fd out-ready block-system)
          (if (and (null? in-ready)
                   (null? out-ready)
                   block-system)
              ;; idle handler set block-system -> #t,
              ;; that means no runnable tasks BUT blocked tasks exists
              ;; (otherwise idle handler exits from tasking)
              ;; in other words, there is absolutely nothing to do
              ;; so we wait until some I/O IN or OUT happens.
              ;; if something happens we invoke poll-for-io again
              ;; (THIS LAST PART IS KLUDGE.  No need to recurse here.)
              ;; And it WON'T WORK at all when I take epoll to edge detect mode.
              ;; FIXME RPR
              (let ((epoll-io-fd (epoll-create)))
;;                (format #t "No ready I/O.  Blocking system: ~s.~%" (current-task))
                (let ((io-ready (epoll-wait
                                 (build-epoll-fd epoll-io-fd
                                                 (list epoll-in-fd epoll-out-fd)
                                                 (list EPOLLIN EPOLLOUT))
                                 POLL-WAIT)))
;;                  (format #t "EPoll Ready I/O: ~s.~%" io-ready)
                  (epoll-close epoll-in-fd)
                  (epoll-close epoll-out-fd)
                  (epoll-close epoll-io-fd)
                  (poll-for-io #f)))
              (begin
;;                 (format (current-output-port)
;;                         "Clearing EPoll desc IN ~s OUT ~s ~%"
;;                         epoll-in-fd epoll-out-fd)
                (clear-blocked epoll-in-fd in-ready 'IN)
                (epoll-close epoll-in-fd)
                (clear-blocked epoll-out-fd out-ready 'OUT)
                (epoll-close epoll-out-fd)))))))
  
  ;;    (define poll-for-io
  ;;      (lambda (block-system)
  ;;        (let ((ready (poll-descriptors (map car *blocked-on-input*)
  ;;                                       (map car *blocked-on-output*)
  ;;                                       block-system)))
  ;;          (do ((ready ready (cdr ready)))
  ;;              ((null? ready))
  ;;            (cond ((assq (car ready) *blocked-on-input*)
  ;;                   => (lambda (probe)
  ;;                        (set! *blocked-on-input*
  ;;                              (remq! probe *blocked-on-input*))
  ;;                        (unblock (cdr probe))))
  ;;                  ((assq (assq (car ready) *blocked-on-output*))
  ;;                   => (lambda (probe)
  ;;                        (set! *blocked-on-output*
  ;;                              (remq! probe *blocked-on-output*))
  ;;                        (unblock (cdr probe))))
  ;;                  (else
  ;;                   (error "I/O signalled ready on unblocked task")))))))))

  ;; Critical section procedure

  (define (remove-from-ioblock-if-blocked task)
    (cond ((reverse-assq task *blocked-on-input*)
           => (lambda (x)
                (set! *blocked-on-input* (remq x *blocked-on-input*))))
          ((reverse-assq task *blocked-on-output*)
           (lambda (x)
             (set! *blocked-on-output* (remq x *blocked-on-output*))))))

                                        ; eof
  )