(library
 (rl3 concurrency tasks primitives)
 
 (export
  call-without-interrupts disable-interrupts enable-interrupts error-handler
  timer-interrupt-handler standard-timeslice system-features)

 (import
  (primitives
   call-without-interrupts disable-interrupts enable-interrupts error-handler
   timer-interrupt-handler standard-timeslice system-features))
 )

(library
 
 (rl3 concurrency tasks)
 
 (export
  current-task end-tasking task? task-state
  make-task
  task-alive? task-block task-join! task-start!
  task-unblock task-yield task-handlers-set!
  with-tasking without-interrupts)
 
 (import
  (primitives
   record-updater)
  (rnrs base)
  (only (rnrs control)
        case-lambda when unless)
  (only (rnrs lists)
        remq)
  (only (rnrs io simple)
        newline current-output-port
        display)
  (only (rnrs io ports)
        flush-output-port)
  (err5rs records procedural)
  (only (err5rs records inspection)
        record-rtd)
  (only (larceny records printer)
        rtd-printer-set!)
  (only (rl3 env larceny)
        reset-handler)
  (only (rl3 concurrency tasks primitives)
        call-without-interrupts enable-interrupts disable-interrupts
        standard-timeslice timer-interrupt-handler)
  ;;   (only (sys condition-variables)
  ;;         make-condition-variable)
  (only (rl3 io print)
        format)
  (only (rl3 env prelude)
        id unspecified)
  (rl3 env parameters)
  (rl3 adt queue))
 
 ;;#####################################
;;; SRFI-18 implemntation
 ;;  Based on Larceny's tasking.sch
 ;;  Using EPoll
 ;; Ray Racine
 ;;#####################################

 ;; The core code pulls heavily from Larceny's tasking.sch
 ;; In fact, you could say that its basically a renaming to SRFI-18 API.
 ;;
 ;; Copyright 1999 Lars T Hansen.
 ;; Simple multitasking for Larceny.

 ;;  (require 'assert)
 ;;  (require 'queue)
 ;;  (require 'define-record)

 (define *task-enabled* #f)		    ; #t in dynamic scope of with-tasking
 (define *task-exit-k*)		            ; To leave tasking
 (define *task-schedule-k*)    	            ; To enter scheduler
 (define *task-run-queue*)                  ; Queue of scheduled tasks
 (define *task-wait-queue*)                 ; Queue of waiting tasks (non-i/o)
 (define *task-current-task*)               ; Current task
 (define *task-all-tasks*)	            ; List of all non-dead tasks
 (define *task-saved-interrupt-handler*)    ; To restore state 
 (define *task-saved-timeslice*)	    ;   when done tasking
 (define *task-poll-counter*)	            ; Countdown for polling

 (define *task-timeslice* 50000)            ; Perhaps low on modern HW.
 (define *task-poll-frequency* 10)          ; Poll every this many switches


;;;
 (define-syntax without-interrupts         ; Critical section
   (syntax-rules ()
     ((without-interrupts E0 E1 ...)
      (call-without-interrupts
       (lambda () E0 E1 ...)))))

;;;
 ;; System-level critical section does not use dynamic-wind.

 (define-syntax task-without-interrupts
   (syntax-rules ()
     ((task-without-interrupts E0 E1 ...)
      (let ((x (disable-interrupts)))
        (let ((r (begin E0 E1 ...)))
          (if x (enable-interrupts x))
          r)))))

;;;
 ;; Entry procedure into the tasking system.
 ;; Invoke with a primordial thunk or main task.

 (define (with-tasking thunk)
   (assert (not *task-enabled*))
   (call-with-current-continuation
    (lambda (k)
      (let ((valid #t))
        (set! *task-exit-k* k)
        (dynamic-wind
            (lambda () 
              (if (not valid)
                  (error "Attempting to throw into a tasking continuation")))
            (lambda () 
              (task-start-tasking thunk))
            (lambda () 
              (set! valid #f)
              (task-stop-tasking)))))))

;;;
;;; SRFI-18 Task API
;;; Public core api for tasking.
;;;

 (define (current-task) 
   (assert *task-enabled*)
   *task-current-task*)

 (define task-start!
   (lambda (task)
     (assert *task-enabled*)
     (set! *task-all-tasks* (cons task *task-all-tasks*))
     (when (eq? (task-state task) 'NEW)
       (task-without-interrupts
        (task-schedule task)))))

 (define task-yield
   (lambda ()
     (assert *task-enabled*)
     (let ((critical (task-in-critical-section?)))
;;        (format (current-output-port)
;;                "Yielding ~s is critical ~s~%" (current-task) critical)
       (task-without-interrupts
        (task-switch #t critical)))))

;;;
 ;; task-sleep!
 ;;
 ;; The current task waits until the timeout is reached.
 ;; This blocks the task only if timeout represents a point in the future.
 ;; It is an error for timeout to be #f. task-sleep! returns an unspecified value.

 (define task-sleep!
   (lambda (n)
     (error "task-sleep not implemented")))

;;;
 ;; task-terminate
 ;;
 ;; Causes an abnormal termination of the task.
 ;;
 ;; If the task is not already terminated,
 ;; all mutexes owned by the task become unlocked/abandoned
 ;; and a "terminated task exception" object is stored in the
 ;; task's end-exception field.
 ;;
 ;; If task is the current task, task-terminate! does not return.
 ;;
 ;; Otherwise task-terminate! returns an unspecified value;
 ;; the termination of the task will occur
 ;; before task-terminate! returns.

 (define task-terminate
   (lambda (task)
     (assert *task-enabled*)
     (assert (task? task))
     (task-without-interrupts
      (task-end task))))

;;;
 ;; task-join!
 ;;
 ;; The current task waits until the task terminates (normally or not)
 ;; or until the timeout is reached if timeout is supplied.
 ;;
 ;; If the timeout is reached, task-join! returns timeout-val if it is supplied,
 ;; otherwise a "join timeout exception" is raised.
 ;;
 ;; If the task terminated normally, the content of the end-result field is returned,
 ;; otherwise the content of the end-exception field is raised.
 ;;

 ;; Kludge hack for now via a yielding, poll loop

 (define task-join!
   ;;  (case-lambda
   (lambda (task)
     (let ((terminated-condvar (task-terminated-condvar task)))
       (condition-variable-put! terminated-condvar task)
       (task-block (current-task)))))
 ;;      ((task timeout) #f)
 ;;      ((task timeout timeout-val) #f)))

;;;
 ;; Fully end the tasking system, reset the system and exit the scheduler.

 (define (end-tasking . rest)
   (assert *task-enabled*)
   (*task-exit-k* (if (not (null? rest))
                      (car rest))))

;;;
 ;; Copy of all non 'TERMINATED tasks.

 (define (all-tasks)
   (assert *task-enabled*)
   (task-without-interrupts
    *task-all-tasks*)) ;;immutable lists!!
 ;; (list-copy *task-all-tasks*)))

 ;; (define (spawn thunk . rest)
 ;;   (let ((name (if (null? rest) #f (car rest))))
 ;;     (assert *task-enabled*)
 ;;     (tasks/without-interrupts
 ;;      (tasks/schedule (tasks/create-task thunk name)))))

;;;
 ;; task record
 ;;
 ;; Invariants:
 ;;  * state is one of
 ;;      NEW         -- not yet started
 ;;      RUNNABLE    -- on run queue
 ;;      RUNNING     -- only the current task (note: not SRFI-18 state)
 ;;      BLOCKED     -- victim of a block
 ;;      TERMINATED  -- exited or was killed
 ;;  * critical is #t if task must be rescheduled with interrupts disabled
 ;;  * the current task is never on the run queue.
 ;;  * dead tasks are never on *all-tasks*

 (define task-rtd
   (let ((task-rtd (make-rtd 'task '#(id k state critical
                                         ;; srfi-18
                                         name specific
                                         end-result end-exception
                                         mutexes
                                         ;; internal use
                                         terminated-condvar))))
     ;; Define a printer routine for when a task is displayed.
     (rtd-printer-set! task-rtd
                       (lambda (task out)
                         (display "#<task \"" out)
                         (display (string-append (task-name task) "-"
                                                 (number->string (task-id task))) out)
                         (display "\">" out)))
     task-rtd))

;;;
 ;;
 
 (define make-task
   (let ((make-task-record (rtd-constructor task-rtd))
         (exit-thunk (lambda () 
                       (task-without-interrupts
                        (task-end *task-current-task*))))
         (id -1))
     (let ((make-task-thunk (lambda (thunk)
                              (lambda ()
                                (parameterize ((reset-handler exit-thunk))                        
                                  (thunk)
                                  (exit-thunk))))))      
       (let ((ctor-task
              (lambda (thunk name)
                (set! id (+ id 1))  ;; fixnum? with wrap around to 0. ???
                (make-task-record id
                                  (make-task-thunk thunk)
                                  'NEW #f name
                                  #f #f #f
                                  '() #f)))) ;; mutexes terminated-condvar
         (case-lambda
           ((thunk)
            (assert (procedure? thunk))
            (ctor-task thunk "anon"))
           ((thunk name)
            (assert (procedure? thunk))
            (ctor-task thunk name)))))))

 
 (define task-id
   (rtd-accessor task-rtd 'id))
 
 (define task-name
   (rtd-accessor task-rtd 'name))
 
 (define task?
   (rtd-predicate task-rtd))

 (define task-state
   (let ((task-state-record (rtd-accessor task-rtd 'state)))
     (lambda (task)
       (let ((state (task-state-record task)))
         (case state
           ;;(( RUNNING RUNNABLE ) 'RUNNABLE)
           (else state))))))
 
 (define task-state-set!
   (let ((task-state-set!-record (rtd-mutator task-rtd 'state)))
     (lambda (task state)
       (task-state-set!-record task state)
       (task-state-changed-handler task))))
 
 (define task-terminated-condvar-set!
   (rtd-mutator task-rtd 'terminated-condvar))

 ;; tasks are created more then they join so lazy create terminated-condvar as necesary
 (define task-terminated-condvar
   (let ((task-terminated-condvar-record (rtd-accessor task-rtd 'terminated-condvar)))
     (lambda (task)
       (cond
        ((task-terminated-condvar-record task) => id)
        (else
         (let ((t-cv (make-condition-variable)))
           (task-terminated-condvar-set! task t-cv)
           t-cv))))))
 
;;;
 ;; THE Scheduler.  Call only with interrupts turned off.
 ;; It's vital that errors are never signalled here without interrupts
 ;; being turned on first.

 (define (task-start-tasking primordial-thunk)
   
   (define task-k
     (rtd-accessor task-rtd 'k))
   
   (define task-critical
     (rtd-accessor task-rtd 'critical))

   (define task-critical-set!
     (rtd-mutator task-rtd 'critical))
   
   (define interval-poll-blocked-tasks
     (lambda ()
       (set! *task-poll-counter* (- *task-poll-counter* 1))      
       (if (zero? *task-poll-counter*)
           (begin (set! *task-poll-counter* *task-poll-frequency*)
                  (task-poll-handler)))))
   
   (disable-interrupts)
   (set! *task-enabled* #t)
   (set! *task-saved-interrupt-handler* (timer-interrupt-handler))
   (set! *task-saved-timeslice* (standard-timeslice))
   (standard-timeslice *task-timeslice*)
   
   ;; this is so damn elegant, EPIPHANY !!!
   (timer-interrupt-handler
    (lambda ()
      (task-switch #t #f)))
   
   (set! *task-current-task* #f)
   (set! *task-run-queue* (make-queue))
   (set! *task-all-tasks* '())
   (set! *task-poll-counter* *task-poll-frequency*)

   ;;(display "Starting main task\n")
   ;;(flush-output-port)
   
   (task-start! (make-task primordial-thunk "main"))
   
   ;;(display "Starting Scheduler\n")
   ;;(flush-output-port)
   
   (enable-interrupts (standard-timeslice))
   
   ;; Scheduler
   
   (call-with-current-continuation
    (lambda (k)
      (set! *task-schedule-k* k)))

   (interval-poll-blocked-tasks)

   (let loop ()
     (when (queue-empty? *task-run-queue*)
       (task-idle-handler)
       (loop)))
   
   (let ((task (queue-get! *task-run-queue*)))
     (set! *task-current-task* task)
     (task-state-set! task 'RUNNING)
     (if (task-critical task)
         (begin (task-critical-set! task #f)
                (enable-interrupts (standard-timeslice))
                (disable-interrupts))
         (enable-interrupts (standard-timeslice)))
     ((task-k task))))

;;;

 (define (task-stop-tasking)
   (disable-interrupts)
   (set! *task-enabled* #f)
   (timer-interrupt-handler *task-saved-interrupt-handler*)
   (standard-timeslice *task-saved-timeslice*)
   (enable-interrupts (standard-timeslice)))

;;;
 ;; called without interrupts => atomic
 (define task-end
   (lambda (task)
     (let ((state (task-state task)))
       (task-state-set! task 'TERMINATED)
       (set! *task-all-tasks* (remq task *task-all-tasks*))
       (condition-variable-broadcast! (task-terminated-condvar task))
       (case state
         (( RUNNABLE )
          (queue-remove! *task-run-queue* task))
         (( RUNNING ) 
          (task-switch #f #f))))))

;;;

 (define task-schedule
   (lambda (task)
     (assert (not (eq? 'TERMINATED (task-state task))))
     (task-state-set! task 'RUNNABLE)
     (queue-put! *task-run-queue* task)))

;;;

 (define task-switch
   (let ((task-k-set!        (rtd-mutator task-rtd 'k))
         (task-critical-set! (rtd-mutator task-rtd 'critical)))
     (lambda (reschedule-current in-critical-section)
       (call-with-current-continuation
        (lambda (k)
          (let ((task *task-current-task*))
            (task-k-set! task (lambda () (k #f)))
            (task-critical-set! task in-critical-section)
            (if reschedule-current
                (task-schedule task))
            (*task-schedule-k* #t)))))))

;;;

 (define task-alive?
   (lambda (task)
     (not (eq? (task-state task) 'TERMINATED))))

;;;

 (define task-in-critical-section?
   (lambda ()
     (let ((flg (disable-interrupts)))
       (if flg (enable-interrupts flg))
       (not flg))))

;;;

 (define task-block
   (lambda (task)
     (assert *task-enabled*)
     (assert (task? task))
     (let ((critical (task-in-critical-section?)))
       (task-without-interrupts
        (let ((state (task-state task)))
          (task-state-set! task 'BLOCKED)
          (case state
            (( RUNNABLE ) (queue-remove! *task-run-queue* task))
            (( RUNNING )  (task-switch #f critical))))))))

;;;
 ;; Makes ready and schedules a task for slicing.
 ;; Task must be in state 'BLOCKED

 (define task-unblock
   (lambda (task)
     (assert *task-enabled*)
     (assert (task? task))
     (task-without-interrupts
      (if (eq? (task-state task) 'BLOCKED)
          (task-schedule task)))))

;;;
 ;; The following are all call-backs from the scheduler
 ;; Override for specific behavior.
;;;

;;;
 ;; IDLE-HANDLER can eg block on a SELECT so that we avoid
 ;; busy-waiting for I/O.  The default is to kill the tasking system,
 ;; since there is no platform-independent way of getting out of an
 ;; idling state.
 
 (define task-idle-handler
   (lambda ()
     (newline)
     (display "; The run queue is empty.  Ending tasking.")
     (newline)
     (end-tasking)))

;;;
 ;; POLL-HANDLER can eg poll for I/O and unblock tasks that have pending I/O.
 ;; Invoked by the scheduler at *task-poll-frequency* intervals

 (define task-poll-handler
   (lambda ()
     (display "****** Null Op Poll Handler ******")
     (newline)
     (flush-output-port)
     #t))


;;; Override by setting the default handlers.
 
 (define task-handlers-set!
   (lambda (idle-thunk poll-thunk)
     (set! task-idle-handler idle-thunk)
     (set! task-poll-handler poll-thunk)))

;;;
                                        ; Overridable.  TASK-STATE-CHANGED-HANDLER is called after the task
                                        ; has changed state and can take action for the task, eg, if task has
                                        ; been killed it can remove the task from data structures.

 (define task-state-changed-handler
   (lambda (t)
;;      (display t)
;;      (display " :: ")
;;      (display (task-state t))
;;      (newline)
     (flush-output-port)
     #t))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; SRFI-18 implementation
;;; Condition Variable
;;; by Ray Racine
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

 ;; NOTE This used to be in its own library, however R6RS libraries are not recursive.
 ;; So condition variables were moved here.
 ;; THE answer IMHO is to getting Chez module syntax (a la Ikarus) into Larceny.
 ;; Then a Library can be composed of modules.

 ;; Another thought. Consider put the task termination cond-var somewhere else besides the task.
 ;; e.g., hashmap (task_id, end-task-convar)
 
 ;; Use Larceny's standard fifo queue as an implementation
 ;;(require 'assert)
 ;;(require 'queue)

 (define condition-variable-rtd
   (make-rtd 'condition-variable '#(name queue specific)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
 ;; (make-condition-variable [name])  ;procedure
 ;;
 ;; Returns a new empty condition variable.
 ;; The optional name is an arbitrary Scheme object which identifies the condition variable
 ;; (useful for debugging); it defaults to an unspecified value.
 ;; The condition variable's specific field is set to an unspecified value
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
 
 (define make-condition-variable
   (let* ((ctor (rtd-constructor condition-variable-rtd))
	  (make (lambda (name)
		  (ctor name (make-queue) (unspecified)))))
     (case-lambda
       (()
        (make "anon"))
       ((name)
        (make name)))))
 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
 ;; (condition-variable? obj)                             
 ;; Returns #t if obj is a condition variable, otherwise returns #f.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
 (define condition-variable?
   (rtd-predicate condition-variable-rtd))
 
 (define condition-variable-name
   (rtd-accessor condition-variable-rtd 'name))
 
 (define condition-variable-specific
   (rtd-accessor condition-variable-rtd 'specific))
 
 (define condition-variable-specific-set!
   (rtd-mutator condition-variable-rtd 'specific))
 
 (define queue
   (rtd-accessor condition-variable-rtd 'queue))
 
 (define condition-variable-put!
   (lambda (cvar task)
     (assert (task? task))
     (queue-put! (queue cvar) task)))
 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
 ;; (condition-variable-signal! condition-variable)      
 ;;
 ;; If there are tasks blocked on the condition-variable, the scheduler
 ;; selects a task and unblocks it.
 ;; condition-variable-signal! returns an unspecified value. 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
 
 (define condition-variable-signal!
   (lambda (cvar)
     (without-interrupts
      (let ((q (queue cvar)))
	(unless (queue-empty? q)
          (task-unblock 
           (queue-get! q)))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
 ;;(condition-variable-broadcast! condition-variable)    
 ;;
 ;; Unblocks all the tasks blocked on the condition-variable.
 ;; condition-variable-broadcast! returns an unspecified value.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
 (define condition-variable-broadcast!
   (lambda (cvar)
     (without-interrupts
      (let ((q (queue cvar)))
	(let loop ()
	  (unless (queue-empty? q)
            (task-unblock (queue-get! q))
            (loop)))))))

 )
